#!/bin/sh

echo ""
echo "Script para criação de imagem de produção"

if [ "$#" -ne 2 ]; then
    echo "" >&2
    echo "Execute o script com o nome do instituto e host" >&2
    echo "Ex: $ $0 fau localhost" >&2
    exit 2
fi

export INSTITUTE=$1
HOST_ADDRESS=$2

DB_USER=${DB_NAME:-user}
DB_PASSWORD=${DB_PASSWORD:-password}
DB_NAME="${INSTITUTE}_db"

cd ./src/client
echo "REACT_APP_BACKEND_URL=http://$HOST_ADDRESS:8000" > .env

cd ../server
echo -e "EMAIL_HOST=localhost\n\
EMAIL_PORT=25\n\
EMAIL_HOST_USER=example@example.com\n\
EMAIL_HOST_PASSWORD=mypassword\n\
EMAIL_USE_TLS=False\n\
\n\
DB_USER=${DB_USER}\n\
DB_PASSWORD=${DB_PASSWORD}\n\
DB_HOST=database\n\
DB_PORT=5432\n\
\n\
AGGREGATE_DB_HOST=mongo\n\
AGGREGATE_DB_PORT=27017\n\
\n\
DJANGO_ALLOWED_HOSTS=$HOST_ADDRESS\n\
DJANGO_DEBUG=False\n\
DJANGO_LOGGING_LEVEL=info\n\
DJANGO_SECRET_KEY=secretkey\n\

EMAIL_CPQS_ABERTAS=cpqs-abertas@usp.br\n" > .env

cd ..

echo ""
echo "Montando imagens..."
docker compose --profile prod build client server --no-cache

cd ..

echo ""
echo "Salvando imagens..."
docker image save cpqs_client:latest cpqs_server:latest | pigz --fast > /tmp/cpqs_images.tar.gz
wait
