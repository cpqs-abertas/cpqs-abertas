#!/bin/bash

###
# Este script deve ser executado apenas uma vez ao criar um host novo para instalar o docker compose
###

echo ""
echo "Script para setup do host CPqs Abertas"

if [ "$#" -ne 1 ]; then
    echo "" >&2
    echo "Execute o script com o nome do instituto" >&2
    echo "Ex: $ $0 fau" >&2
    exit 2
fi

INSTITUTE=$1

echo "export INSTITUTE=$INSTITUTE" >> ~/.bashrc
source ~/.bashrc

sudo apt update
sudo apt install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io docker-compose-plugin

sudo service docker start
