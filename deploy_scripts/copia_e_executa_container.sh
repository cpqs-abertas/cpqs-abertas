#!/bin/sh

echo ""
echo "Script para envio de imagem e subida de container"

if [ "$#" -ne 3 ]; then
    echo "" >&2
    echo "Execute o script com o nome do instituto, do host e o usuário" >&2
    echo "Ex: $ $0 fau localhost ubuntu" >&2
    exit 2
fi

INSTITUTE=$1
HOST_ADDRESS=$2
USER=$3

scp /tmp/cpqs_images.tar.gz $USER@$HOST_ADDRESS:/tmp/cpqs_images.tar.gz

ssh $USER@$HOST_ADDRESS bash -c "'

echo Running command: docker load /tmp/cpqs_images.tar.gz
docker load < /tmp/cpqs_images.tar.gz

echo Running command: rm /tmp/cpqs_images.tar.gz
rm /tmp/cpqs_images.tar.gz

echo Running command: git pull master
cd ~/cpqs-abertas
git checkout master
git pull

cd ./src

echo Running command: docker compose --profile prod down
INSTITUTE=$INSTITUTE docker compose --profile prod down

echo Running command: docker image prune -f
INSTITUTE=$INSTITUTE docker image prune -f

echo Running command: docker compose --profile prod up -d
INSTITUTE=$INSTITUTE docker compose --profile prod up -d
'"
