#!/usr/bin/env bash

DEV_STRING="dev"
PROD_STRING="prod"

if [ "$#" -ne 2 ]; then
    echo "" >&2
    echo "Execute o script com o nome do instituto a ser configurado e o modo de execução (\"$DEV_STRING\"->desenvolvimento, \"$PROD_STRING\"->produção) como argumentos" >&2
    echo "Ex: $ $0 fau $DEV_STRING" >&2
    exit 1
fi

mode=$2

case $mode in
    $DEV_STRING)
        server_service="server-dev"
        client_service="client-dev"
    ;;
    $PROD_STRING)
        server_service="server"
        client_service="client"
    ;;
    *)
        echo "Modo de desenvolvimento '$mode' desconhecido. Modos disponíveis: [$DEV_STRING, $PROD_STRING]" >&2
        exit 1
    ;;
esac

export INSTITUTE=$1

cd ./src/

echo ""
echo "Montando imagens..."
docker compose --profile $mode build

echo ""
echo "Inicializando containers..."
docker compose --profile $mode up -d

sleep 60

echo ""
echo "Rodando migrations..."
docker compose --profile $mode exec $server_service python manage.py migrate

echo ""
echo "Aplicação está operante. Populando banco de dados..."
docker compose --profile $mode exec $server_service python util/populate/code/main.py -v
docker compose --profile $mode exec $server_service python util/populate/code/mongo_aggregate.py -v

echo ""
echo "Finalizado"

if [ "$mode" = "$DEV_STRING" ]; then
    echo ""
    echo "Capturando logs dos conteineres..."
    docker compose logs --follow
fi
