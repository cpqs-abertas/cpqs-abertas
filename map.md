## Pastas

### Documentation
Pasta de documentações gerais, no momento, com apenas o modelo do banco.

---

### src
É a pasta que tem o docker-compose, e as pastas para as imagens que o docker compose cria.

Para mais informações: [Docker README](https://gitlab.com/lcfpadilha/fau-aberta/blob/master/server/README_DOCKER.md)

#### django
Dockerfile para a imagem e os arquivos do django.

#### postgres
Dockerfile para a imagem e os arquivos do postgres.

#### react
Dockerfile para a imagem e os arquivos do react.

##### volume_[django/postgres/react]
Pasta que é montada dentro do container. Tem o resto dos arquivos do projeto.

##### docker_files
Arquivos da imagem do docker.

---

### volume_django

#### models
Contém arquivos model, e a pasta de migrations.

#### cpqsabertas
Configuração do app global
  - urls (rotas).
  - settings.

#### pages
Cada pasta tem rotas.

#### util
Scripts úteis.

---
### React

#### volume_public
Arquivos públicos do react.

### volume_src
Arquivos fonte do react compõe os códigos base da aplicação. A API tem as funções de chamada ao nosso backend (django) e as tiles definem os estilos dos nosso componentes.

#### assets
Files que não são código.
  - Imagens.

#### charts
Arquivos base para os gráficos.

#### components
Componentes que são reutilizados no código base.

#### pages
Código fonte para cada uma das nossas páginas.
