import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useParams } from "react-router-dom";
import { Conteudo } from "../styles";
import {
  getCountOriPessoa,
  getCountOriPessoaPorAno,
  getPessoa,
  getTiposOriPessoa,
} from "../../common/API";
import ProfessorActivityProfile from "../components/profile/ProfessorActivityProfile";
import { setFilters } from "../helpers";

function PerfilOrientacoes(props) {
  const { menuFilter, setMenuFilter } = props;
  const { id } = useParams();
  const [professorData, setProfessorData] = useState(null);

  useEffect(() => {
    getPessoa({ id }).then((responsePayload) => {
      setProfessorData(responsePayload);

      const department = responsePayload.departamento;

      setFilters(
        setMenuFilter,
        {
          docentes: true,
          perfil: true,
          orientacoes: true,
          [department]: true,
        },
        (filter, newState) => {
          newState[filter] = false;
        }
      );
    });
  }, []);

  if (!professorData) {
    return (
      <Conteudo
        style={{
          alignSelf: "center",
          textAlign: "center",
        }}
      >
        carregando
      </Conteudo>
    );
  }

  const detailsToDisplay = [
    { description: "Título:", makeText: (production) => production.titulo },
    { description: "Aluno:", makeText: (production) => production.aluno },
    { description: "Curso:", makeText: (production) => production.curso },
    {
      description: "Tipo:",
      makeText: (production) => production.tipo_pesquisa,
    },
    {
      description: "Instituição:",
      makeText: (production) => production.instituicao,
    },
    { description: "Idioma:", makeText: (production) => production.idioma },
    { description: "País:", makeText: (production) => production.pais },
  ];

  if (!professorData.orientacoes) {
    return (
      <Conteudo style={{ justifyContent: "start" }}>
        Não foi possível encontrar essa pessoa
      </Conteudo>
    );
  }

  return (
    <ProfessorActivityProfile
      detailsToDisplay={detailsToDisplay}
      getCountPessoa={getCountOriPessoa}
      getCountPessoaPorAno={getCountOriPessoaPorAno}
      getTiposPessoa={getTiposOriPessoa}
      menuFilter={menuFilter}
      productionType="Orientação"
      professorData={professorData}
      title="Orientações"
      productionDataKey="orientacoes"
    />
  );
}

PerfilOrientacoes.propTypes = {
  menuFilter: PropTypes.object.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
};

export default PerfilOrientacoes;
