import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import {
  getDashboardAggregatedCount,
  getDashboardAggregated,
  getDashboardAggregatedMap,
  getDashboardAggregatedNationalMap,
} from "../../common/API";
import {
  Conteudo,
  DivCard,
  DivGraph,
  DivInfoText,
  DivTitle,
  DivInfoProducao,
  DivTotal,
  barColors,
  departmentColors,
  instituteColor,
  instituteArrow,
  BarChartWrapper,
  LineChartWrapper,
  NationalMapWrapper,
  WorldMapWrapper,
} from "../styles";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import BarChartContainer from "../../common/charts/BarChartContainer";
import LineChartContainer from "../../common/charts/LineChartContainer";
import NationalMapContainer from "../../common/maps/NationalMapContainer";
import WorldMapContainer from "../../common/maps/WorldMapContainer";
import FaderWrapper from "../components/wrappers/FaderWrapper";
import { setFilters } from "../helpers";
import CarouselWrapper from "../../fearp/components/wrappers/CarouselWrapper";

function Home(props) {
  const { setMenuFilter } = props;
  const [countByProductionType, setCountByProductionType] = useState(null);

  useEffect(() => {
    getDashboardAggregated().then((responsePayload) => {
      setCountByProductionType(responsePayload);
    });

    setFilters(setMenuFilter, { home: true }, (filter, newState) => {
      newState[filter] = false;
    });
  }, []);

  if (!countByProductionType) {
    return (
      <Conteudo
        style={{
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <FaderWrapper />
      </Conteudo>
    );
  }

  return <HomeData countByProductionType={countByProductionType} />;
}

function HomeData(props) {
  const { countByProductionType } = props;
  const total = Object.values(countByProductionType).reduce(
    (accumulator, value) => accumulator + value,
    0
  );

  return (
    <Conteudo style={{ justifyContent: "start" }}>
      <DivCard style={{ border: "none" }}>
        <DivInfoProducao>
          <DivTitle>
            <span style={{ fontSize: 32 }}>Total IME-USP</span>
          </DivTitle>
          <DivTotal>
            <span style={{ fontSize: 100 }}>
              {Intl.NumberFormat().format(total)}
            </span>
            <span>resultados</span>
          </DivTotal>
        </DivInfoProducao>
        <DivGraph>
          <CarouselWrapper arrowImage={instituteArrow}>
            <LineChartWrapper>
              <LineChartContainer
                labels={["IME"]}
                color={{ IME: departmentColors.IME }}
                fill={{ IME: "origin" }}
                fetchData={() =>
                  getDashboardAggregatedCount({ ano_inicio: 1978 })
                }
                Loading={FaderWrapper}
              />
            </LineChartWrapper>
            <BarChartWrapper>
              <BarChartContainer
                labels={["IME"]}
                type="single"
                colors={barColors.IME}
                fetchData={async () => ({
                  countByYearByCategoryByLabel: { IME: countByProductionType },
                  categories: [
                    "Produção Artística",
                    "Produção Técnica",
                    "Produção Bibliográfica",
                    "Orientação",
                    "Bancas",
                    "Prêmios e Títulos",
                  ],
                })}
                Loading={FaderWrapper}
              />
            </BarChartWrapper>
            <WorldMapWrapper>
              <WorldMapContainer
                color={instituteColor}
                fetchData={() => getDashboardAggregatedMap()}
                Loading={FaderWrapper}
              />
            </WorldMapWrapper>
            <NationalMapWrapper>
              <NationalMapContainer
                color={instituteColor}
                fetchData={() => getDashboardAggregatedNationalMap()}
                Loading={FaderWrapper}
              />
            </NationalMapWrapper>
          </CarouselWrapper>
        </DivGraph>
        <DivInfoText style={{ padding: "0px 0px 0px 5px" }}>
          <span style={{ fontSize: 13.7 }}>
            O <b>IME Aberto</b> é uma iniciativa da{" "}
            <b>
              Comissão de Pesquisa do IMEUSP (CPq-IMEUSP) em parceria com o
              Projeto CPqs Abertas - PRPI-USP
            </b>{" "}
            com o intuito de dar visibilidade à produção intelectual do
            instituto, difundindo sua especificidade e diversidade através de
            dados extraídos do currículo Lattes dos docentes. Como se sabe, é
            compromisso das universidades públicas ampliar o acesso às suas
            pesquisas acadêmicas e aos seus resultados (produções
            bibliográficas, técnicas e artísticas), permitindo quantificá-los e
            qualificá-los em termos de impacto social, impacto econômico,
            inovação tecnológica e desdobramentos em políticas públicas e de
            sustentabilidade. O projeto envolveu a participação de docentes da{" "}
            <b>FAU</b> e do <b>IME</b>, além de discentes de graduação,
            bibliotecários e a Superintendência da Tecnologia e Informação da
            USP (STI-USP). Esta terceira etapa do projeto, desenvolvida em 2021
            e implementada em 2022, expande a iniciativa que teve início com a
            FAUaberta. Atualmente, esse projeto em andamento disponibiliza
            apenas a produção intelectual dos docentes do IMEUSP cadastrada até
            setembro de 2022, quando os dados foram coletados. Para a próxima
            etapa de desenvolvimento e automatização do sistema, seguiremos
            contando com o apoio do IME-USP, do STI-USP, STI-IME e da Diretoria
            do IMEUSP.
          </span>
          <span style={{ marginBottom: 5, marginTop: 0 }}>
            <b>coordenação do projeto CPqs Abertas:</b> Alfredo Goldman vel
            Lejbman
          </span>
          <span style={{ marginBottom: 0 }}>
            <b>pesquisadores colaboradores:</b> Artur Rozestraten, Beatriz
            Bueno, Leandro Velloso, Amarílis Corrêa, Harley Macedo e Deidson
            Rafael Trindade
            <br />
            <b>etapa III, 2021 | desenvolvimento:</b> João Daniel, João Gabriel
            Lembo, Leonardo Pereira, Victor Lima | <b>design:</b> Gustavo
            Machado. <b>coordenação:</b> Luís Felipe Abbud
            <br />
            <b>etapa IV, 2022 | desenvolvimento:</b> João Daniel, Rafael
            Rodrigues, Mohamad Rkein | <b>design:</b> Gustavo Machado.{" "}
            <b>coordenação:</b> Luís Felipe Abbud
          </span>
        </DivInfoText>
      </DivCard>
    </Conteudo>
  );
}

Home.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

HomeData.propTypes = {
  countByProductionType: PropTypes.object.isRequired,
};

export default Home;
