import React from "react";
import PropTypes from "prop-types";
import Department from "../components/department/Department";

function DCC(props) {
  const { setMenuFilter } = props;

  return (
    <Department
      setMenuFilter={setMenuFilter}
      dep="DCC"
      depDescription="Departamento de Ciência da Computação"
      chefe="Prof. Dr. Marcelo Gomes de Queiroz"
      viceChefe="Prof. Dr. Roberto Hirata Junior"
    >
      <span style={{ fontSize: 25, fontWeight: "bold" }}>Histórico</span>
      <span>
        O Departamento de Ciência da Computação do IME-USP foi formalmente
        criado em 1987. Mas a atividade em Ciência da Computação no IME-USP
        antecede este evento. Pode-se dizer que o núcleo do corpo docente do DCC
        teve origem em 1961 em torno do computador IBM-1620, que acabava de ser
        instalado no Centro de Cálculo Numérico (hoje CeTI-SP – Centro de
        Tecnologia da Informação de São Paulo) da USP. Em 1970, com a criação do
        IME, vários integrantes daquele grupo e do Departamento de Matemática da
        Escola Politécnica tornaram-se docentes do MAP, onde permaneceram até a
        criação do DCC.
      </span>
      <span>
        O DCC é responsável pelo curso de Bacharelado em Ciência da Computação
        (BCC), que oferece 50 vagas por ano via vestibular Fuvest e formou 905
        bacharéis até 2002. A primeira turma de alunos concluiu o curso em 1974.
        No ano de 1994 o BCC comemorou seus 20 anos; em 1999, comemorou seus 25
        anos. O BCC é bastante procurado no vestibular (30 a 60 candidatos por
        vaga). O DCC também é responsável pelo ensino de uma disciplina básica
        de Introdução à Computação que faz parte do currículo de quase todos
        cursos das áreas de ciências exatas e tecnologia (cerca de 1700 alunos
        por ano). Mais recentemente, uma variante desta disciplina passou a ser
        oferecida a vários cursos da área de ciência humanas; a demanda por esta
        variante da disciplina continua maior que a capacidade de atendimento do
        DCC. Para enfrentar esta demanda foi criado, em 1992, o CEC – Centro de
        Ensino da Computação, sob a coordenação do DCC.
      </span>
      <span>
        Em relação à pós-graduação, o DCC é responsável pelos cursos de Mestrado
        e Doutorado em Ciência da Computação, que existem desde 1972. O programa
        formou 174 mestres de 1972 a 2002. Vários destes mestres continuaram
        seus estudos no exterior, onde obtiveram o grau de doutor. O programa de
        doutorado em Ciência de Computação no IME-USP está se consolidando, já
        tendo formado 19 doutores até o ano 2003. O programa tem hoje entre 25 e
        30 alunos. As dissertações de nossos alunos têm recebido reconhecimento
        no meio acadêmico, tendo recebido vários prêmios.
      </span>
      <span>
        Em 1979 o IME-USP sediou a primeira Escola de Computação, organizada por
        docentes que hoje estão no DCC. Hoje a Escola de Computação é um evento
        tradicional, de âmbito nacional, já na sua décima segunda edição, que
        também será organizada pelo DCC.
      </span>
      <span>
        Em 1992 o DCC coordenou e sediou o simpósio internacional LATIN’92
        (Latin American Theoretical INformatics), que contou com o apoio de
        várias associações internacionais (ACM/SIGACT, EATCS, IFIP, IEEE) e a
        presença de uma centena de pesquisadores (destes, cerca de 54 do
        exterior). Os trabalhos apresentados no simpósio foram publicados,
        antecipadamente, na série Lecture Notes in Computer Science, da
        Springer-Verlag.
      </span>
      <span>
        A qualificação do corpo docente de pós-graduação do DCC aumentou
        consideravelmente nos últimos anos. Nossos docentes tem formação
        diversificada, com formação obtida em em vários centros do Brasil e do
        exterior. O DCC conta atualmente com 36 professores, sendo 35
        professores doutores.
      </span>
    </Department>
  );
}

DCC.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default DCC;
