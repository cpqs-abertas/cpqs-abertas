import React from "react";
import PropTypes from "prop-types";
import Department from "../components/department/Department";

function MAT(props) {
  const { setMenuFilter } = props;

  return (
    <Department
      setMenuFilter={setMenuFilter}
      dep="MAT"
      depDescription="Departamento de Matemática"
      chefe="Profa. Dra. Lucia Renato Junqueira"
      viceChefe="Prof. Dr. Alexandre Lymberopoulos"
    >
      <span style={{ fontSize: 25, fontWeight: "bold" }}>Histórico</span>
      <span>
        O Departamento de Matemática (MAT) foi constituído em 1970 como
        consequência da implantação na USP da reforma universitária de 1968. Os
        departamentos de Matemática existentes em várias unidades foram então
        reunidos em um único departamento no recém-criado Instituto de
        Matemática e Estatística. O mais antigo desses departamentos, o da
        Escola Politécnica, já existia antes da fundação da universidade. O da
        Faculdade de Filosofia, Ciências e Letras havia sido criado em 1934,
        junto com a própria universidade.
      </span>
      <span>
        O MAT é responsável pelos cursos de Bacharelado em Matemática (diurno) e
        Licenciatura em Matemática (diurno e noturno), e pelo Centro de
        Aperfeiçoamento do Ensino da Matemática (CAEM). É também responsável
        pelo oferecimento de disciplinas de Matemática às seguintes unidades da
        USP: Escola Politécnica; os institutos de Astronomia, Geofísica e
        Ciências Atmosféricas; Física; Geociências; Química; Oceanografia; e as
        faculdades de Arquitetura e Urbanismo; Ciências Farmacêuticas; Economia,
        Administração e Contabilidade.
      </span>
      <span>
        A maioria dos docentes do MAT colabora com pelo menos um dos dois
        programas de pós-graduação: o programa de pós-graduação em Matemática do
        IME, criado também em 1970, cujos cursos de Mestrado e Doutorado estão
        entre os mais antigos e bem-reconhecidos do país e o programa de
        Mestrado Profissional em Ensino de Matemática, criado em 2012. Muitos
        dos ex-alunos desses cursos ocupam hoje posições de liderança em
        diversas universidades brasileiras e do exterior.
      </span>
      <span>
        O MAT conta com cerca de 76 professores, todos doutores, atuantes em
        todas as grandes áreas de pesquisa em Matemática e em Educação
        Matemática. Mantém intenso intercâmbio com pesquisadores de todas as
        regiões do país e de grandes centros estrangeiros.
      </span>
    </Department>
  );
}

MAT.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default MAT;
