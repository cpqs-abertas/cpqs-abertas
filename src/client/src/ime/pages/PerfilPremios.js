import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useParams } from "react-router-dom";
import { Conteudo } from "../styles";
import { getPessoa, getCountPremioPessoaPorAno } from "../../common/API";
import ProfessorActivityProfile from "../components/profile/ProfessorActivityProfile";
import { setFilters } from "../helpers";

function PerfilPremios(props) {
  const { menuFilter, setMenuFilter } = props;
  const { id } = useParams();
  const [professorData, setProfessorData] = useState(null);

  useEffect(() => {
    getPessoa({ id }).then((responsePayload) => {
      setProfessorData(responsePayload);

      const department = responsePayload.departamento;

      setFilters(
        setMenuFilter,
        {
          docentes: true,
          perfil: true,
          premios: true,
          [department]: true,
        },
        (filter, newState) => {
          newState[filter] = false;
        }
      );
    });
  }, []);

  if (!professorData) {
    return (
      <Conteudo
        style={{
          alignSelf: "center",
          textAlign: "center",
        }}
      >
        carregando
      </Conteudo>
    );
  }

  if (!professorData.premios_titulos) {
    return (
      <Conteudo style={{ justifyContent: "start" }}>
        Não foi possível encontrar essa pessoa
      </Conteudo>
    );
  }

  const detailsToDisplay = [
    { description: "Nome:", makeText: (production) => production.nome },
    { description: "Entidade:", makeText: (production) => production.entidade },
  ];

  return (
    <ProfessorActivityProfile
      chartsToRemove={["barChart"]}
      detailsToDisplay={detailsToDisplay}
      getCountPessoaPorAno={getCountPremioPessoaPorAno}
      menuFilter={menuFilter}
      productionType="Prêmios e Títulos"
      professorData={professorData}
      title="Prêmios e Títulos"
      productionDataKey="premios_titulos"
    />
  );
}

PerfilPremios.propTypes = {
  menuFilter: PropTypes.object.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
};

export default PerfilPremios;
