import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useParams } from "react-router-dom";
import { Conteudo } from "../styles";
import {
  getCountProdBibPessoa,
  getCountProdBibPessoaPorAno,
  getPessoa,
  getTiposProdBibPessoa,
} from "../../common/API";
import ProfessorActivityProfile from "../components/profile/ProfessorActivityProfile";
import { setFilters } from "../helpers";

function PerfilProdBiblio(props) {
  const { menuFilter, setMenuFilter } = props;
  const { id } = useParams();
  const [professorData, setProfessorData] = useState(null);

  useEffect(() => {
    getPessoa({ id }).then((responsePayload) => {
      setProfessorData(responsePayload);

      const department = responsePayload.departamento;

      setFilters(
        setMenuFilter,
        {
          docentes: true,
          perfil: true,
          bibliografica: true,
          [department]: true,
        },
        (filter, newState) => {
          newState[filter] = false;
        }
      );
    });
  }, []);

  if (!professorData) {
    return (
      <Conteudo
        style={{
          alignSelf: "center",
          textAlign: "center",
        }}
      >
        carregando
      </Conteudo>
    );
  }

  if (!professorData.producoes_bibliograficas) {
    return (
      <Conteudo style={{ justifyContent: "start" }}>
        Não foi possível encontrar essa pessoa
      </Conteudo>
    );
  }

  const detailsToDisplay = [
    { description: "Título:", makeText: (production) => production.titulo },
    {
      description: "Publicação:",
      makeText: (production) => production.publicacao,
    },
    { description: "Editora:", makeText: (production) => production.editora },
    { description: "Tipo:", makeText: (production) => production.tipo },
    { description: "Idioma:", makeText: (production) => production.idioma },
    {
      description: "DOI:",
      makeText: (production) =>
        production.doi ? (
          <a
            href={`http://dx.doi.org/${production.doi}`}
            target="_blank"
            rel="noreferrer"
          >
            {production.doi}
          </a>
        ) : null,
    },
    { description: "ISBN:", makeText: (production) => production.isbn },
    {
      description: "Autores:",
      makeText: (production) => production.autores.join(", "),
    },
    { description: "País:", makeText: (production) => production.pais },
  ];

  return (
    <ProfessorActivityProfile
      detailsToDisplay={detailsToDisplay}
      getCountPessoa={getCountProdBibPessoa}
      getCountPessoaPorAno={getCountProdBibPessoaPorAno}
      getTiposPessoa={getTiposProdBibPessoa}
      menuFilter={menuFilter}
      productionType="Produção Bibliográfica"
      professorData={professorData}
      title="Produção Bibliográfica"
      productionDataKey="producoes_bibliograficas"
    />
  );
}

PerfilProdBiblio.propTypes = {
  menuFilter: PropTypes.object.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
};

export default PerfilProdBiblio;
