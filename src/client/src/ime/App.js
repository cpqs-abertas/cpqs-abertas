import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./pages/Home";
import HeaderWrapper from "./components/wrappers/HeaderWrapper";
import Menu from "./components/Menu";
import FooterWrapper from "./components/wrappers/FooterWrapper";
import Docentes from "./pages/Docentes";
import DCC from "./pages/DCC";
import MAE from "./pages/MAE";
import MAT from "./pages/MAT";
import MAP from "./pages/MAP";
import PerfilDocente from "./pages/PerfilDocente";
import ProducaoBibliografica from "./pages/ProducaoBibliografica";
import ProducaoTecnica from "./pages/ProducaoTecnica";
import Orientacoes from "./pages/Orientacoes";
import Bancas from "./pages/Bancas";
import PremiosETitulos from "./pages/PremiosETitulos";
import PerfilProdBiblio from "./pages/PerfilProdBiblio";
import PerfilProdTec from "./pages/PerfilProdTec";
import PerfilOrientacoes from "./pages/PerfilOrientacoes";
import PerfilBancas from "./pages/PerfilBancas";
import PerfilPremios from "./pages/PerfilPremios";
import FAQ from "./components/faq/FAQ";
import { Body } from "./styles";

function App() {
  const [menuFilter, setMenuFilter] = useState({
    home: true,
    docentes: false,
    DCC: false,
    MAT: false,
    MAE: false,
    MAP: false,
    perfil: false,
    bibliografica: false,
    tecnica: false,
    orientacoes: false,
    bancas: false,
    premios: false,
    faq: false,
  });

  useEffect(() => {
    document.querySelector("title").innerText = "IME Aberto";
    document.querySelector('link[rel="shortcut icon"]').href =
      "/ime/favicon.ico";
  }, []);

  return (
    <Router>
      <Body>
        <HeaderWrapper />
        <Menu menuFilter={menuFilter} setMenuFilter={setMenuFilter} />

        {menuFilter.faq && <FAQ setMenuFilter={setMenuFilter} />}

        <Switch>
          <Route exact path="/">
            <Home setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/DCC">
            <DCC setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/MAT">
            <MAT setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/MAE">
            <MAE setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/MAP">
            <MAP setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/producao-bibliografica">
            <ProducaoBibliografica setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/producao-tecnica">
            <ProducaoTecnica setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/orientacoes">
            <Orientacoes setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/bancas">
            <Bancas setMenuFilter={setMenuFilter} />
          </Route>
          <Route path="/premios-e-titulos">
            <PremiosETitulos setMenuFilter={setMenuFilter} />
          </Route>

          <Route exact path="/pessoa/nomes">
            <Docentes setMenuFilter={setMenuFilter} department="all" />
          </Route>
          <Route path="/pessoa/nomes/DCC">
            <Docentes setMenuFilter={setMenuFilter} department="DCC" />
          </Route>
          <Route path="/pessoa/nomes/MAT">
            <Docentes setMenuFilter={setMenuFilter} department="MAT" />
          </Route>
          <Route path="/pessoa/nomes/MAE">
            <Docentes setMenuFilter={setMenuFilter} department="MAE" />
          </Route>
          <Route path="/pessoa/nomes/MAP">
            <Docentes setMenuFilter={setMenuFilter} department="MAP" />
          </Route>
          <Route path="/pessoa/perfil/:id">
            <PerfilDocente
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
          <Route path="/pessoa/producao-bibliografica/:id">
            <PerfilProdBiblio
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
          <Route path="/pessoa/producao-tecnica/:id">
            <PerfilProdTec
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
          <Route path="/pessoa/bancas/:id">
            <PerfilBancas
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
          <Route path="/pessoa/orientacoes/:id">
            <PerfilOrientacoes
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
          <Route path="/pessoa/premios-e-titulos/:id">
            <PerfilPremios
              menuFilter={menuFilter}
              setMenuFilter={setMenuFilter}
            />
          </Route>
        </Switch>

        <FooterWrapper />
      </Body>
    </Router>
  );
}

export default App;
