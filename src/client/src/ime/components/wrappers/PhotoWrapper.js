import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { getImg } from "../../../common/API";
import { departmentColors } from "../../styles";
import Photo from "../../../common/misc/Photo";
import FaderWrapper from "./FaderWrapper";
import lattesAvatar from "../../assets/images/black-lattes-avatar.png";

function PhotoWrapper(props) {
  const { id, menuFilter } = props;
  const [content, setContent] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const shouldUseDefaultImage = (imgElement) =>
    imgElement.naturalWidth === 95 && imgElement.naturalHeight === 99;

  useEffect(() => {
    getImg(id).then((image) => {
      const auxImg = document.createElement("img");
      auxImg.src = image;
      auxImg.addEventListener("load", () => {
        if (shouldUseDefaultImage(auxImg)) {
          setContent(lattesAvatar);
        } else {
          setContent(image);
        }

        setIsLoading(false);
      });
    });
  }, []);

  let borderColor;
  Object.keys(departmentColors).forEach((key) => {
    if (menuFilter[key]) {
      borderColor = departmentColors[key];
    }
  });

  return (
    <div
      style={{
        alignItems: "center",
        display: "flex",
        height: "150px",
        justifyContent: "center",
        margin: "20px",
        width: "150px",
      }}
    >
      {isLoading ? (
        <FaderWrapper />
      ) : (
        <Photo borderColor={borderColor} image={content} />
      )}
    </div>
  );
}

PhotoWrapper.propTypes = {
  id: PropTypes.string.isRequired,
  menuFilter: PropTypes.object.isRequired,
};

export default PhotoWrapper;
