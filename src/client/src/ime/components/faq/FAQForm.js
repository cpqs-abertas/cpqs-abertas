import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { instituteColor, supportColor } from "../../styles";

function FAQForm(props) {
  const { postQuestion } = props;
  const [isShowingEmailSentMessage, setIsShowingEmailSentMessage] =
    useState(false);
  const [isShowingErrorMessage, setIsShowingErrorMessage] = useState(false);
  const [hasEmailError, setHasEmailError] = useState(false);
  const [hasSubjectError, setHasSubjectError] = useState(false);
  const [hasMessageError, setHasMessageError] = useState(false);

  const handleFormSubmit = (event) => {
    event.preventDefault();
    const form = event.target;
    const params = {};
    for (let i = 0; i < form.elements.length; i++) {
      const element = form.elements[i];
      if (element.name) {
        params[element.name] = element.value;
      }
    }

    let hasError = false;
    const emailRegex =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!emailRegex.test(form.email.value)) {
      hasError = true;
      setHasEmailError(true);
      setTimeout(() => {
        setHasEmailError(false);
      }, 3000);
    }

    if (!form.subject.value) {
      hasError = true;
      setHasSubjectError(true);
      setTimeout(() => {
        setHasSubjectError(false);
      }, 3000);
    }

    if (!form.message.value) {
      hasError = true;
      setHasMessageError(true);
      setTimeout(() => {
        setHasMessageError(false);
      }, 3000);
    }

    if (hasError) {
      return;
    }

    postQuestion(params)
      .then(() => {
        setIsShowingEmailSentMessage(true);
        setTimeout(() => {
          setIsShowingEmailSentMessage(false);
        }, 3000);
      })
      .catch(() => {
        setIsShowingErrorMessage(true);
        setTimeout(() => {
          setIsShowingErrorMessage(false);
        }, 3000);
      });
  };

  let hasError = false;
  if (hasEmailError || hasSubjectError || hasMessageError) {
    hasError = true;
  }

  return (
    <form style={{ marginTop: "2rem" }} onSubmit={handleFormSubmit}>
      <div style={{ marginBottom: "0.25rem" }}>
        <FormInput name="name" id="name" placeholder="Nome completo" />
      </div>
      <div style={{ marginBottom: "0.25rem" }}>
        <FormInput
          hasError={hasEmailError}
          name="email"
          id="email"
          placeholder="E-mail"
        />
      </div>
      <div style={{ marginBottom: "0.25rem" }}>
        <FormInput
          hasError={hasSubjectError}
          name="subject"
          id="subject"
          placeholder="Assunto"
        />
      </div>
      <FormTextArea
        hasError={hasMessageError}
        name="message"
        id="message"
        placeholder="Descrição da dúvida"
        rows="7"
      />

      <div style={{ textAlign: "center" }}>
        <FormSubmitButton type="submit">Enviar</FormSubmitButton>
      </div>

      {isShowingEmailSentMessage && (
        <div style={{ marginTop: "1rem", textAlign: "center" }}>
          E-mail enviado com sucesso!
        </div>
      )}

      {isShowingErrorMessage && (
        <FormErrorArea>
          Erro no envio, tente novamente mais tarde!
        </FormErrorArea>
      )}

      {hasError && (
        <FormErrorArea>
          Preencha todos os campos e/ou verifique a formatação!
        </FormErrorArea>
      )}
    </form>
  );
}

FAQForm.propTypes = {
  postQuestion: PropTypes.func.isRequired,
};

const FormInput = styled.input`
  border-width: 1px;
  border-style: solid;
  border-color: ${(props) => (props.hasError ? "#FF0000" : supportColor)};
  border-radius: 4px;
  padding-left: 0.25rem;
  width: 100%;
`;

const FormTextArea = styled.textarea`
  border-width: 1px;
  border-style: solid;
  border-color: ${(props) => (props.hasError ? "#FF0000" : supportColor)};
  border-radius: 4px;
  font-family: Arial;
  padding-left: 0.25rem;
  resize: none;
  width: 100%;
`;

const FormSubmitButton = styled.button`
  background: ${supportColor};
  border: 0;
  border-radius: 4px;
  color: #ffffff;
  cursor: pointer;
  font-weight: bold;
  padding: 0.25rem 1rem;

  :active {
    background: ${instituteColor};
  }
`;

const FormErrorArea = styled.div`
  color: #ff0000;
  font-weight: bold;
  margin-top: 1rem;
  text-align: center;
`;

export default FAQForm;
