import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import FAQForm from "./FAQForm";
import FAQQuestionItem from "./FAQQuestionItem";
import questions from "../../../common/faq/questions";
import { CenteredColumn, supportColor } from "../../styles";
import { setFilters } from "../../helpers";
import { postQuestion } from "../../../common/API";

function FAQ(props) {
  const { setMenuFilter } = props;
  const [selectedQuestionIndex, setSelectedQuestionIndex] = useState(-1);

  const updateMenuEntry = () => {
    setFilters(setMenuFilter, { faq: false }, (filter, newState, prevState) => {
      newState[filter] = prevState[filter];
    });
  };

  const questionsPlusForm = [];
  questions.forEach((pair) => {
    questionsPlusForm.push(pair);
  });

  questionsPlusForm.push({
    text: "Minha dúvida não está aqui. O que fazer?",
    answer: (
      <>
        Caso você tenha alguma dúvida que não tenha sido respondida, não hesite
        em nos contatar! Basta preencher as informações abaixo que te mandaremos
        um e-mail esclarecendo quaisquer perguntas
        <FAQForm postQuestion={postQuestion} />
      </>
    ),
  });

  const questionCount = questionsPlusForm.length;
  const questionItems = questionsPlusForm.map((question, index) => (
    <FAQQuestionItem
      text={question.text}
      answer={question.answer}
      number={index}
      questionCount={questionCount}
      selectedQuestionNumber={selectedQuestionIndex}
      setSelectedQuestionNumber={setSelectedQuestionIndex}
    />
  ));

  return (
    <CenteredColumn style={{ zIndex: "10" }}>
      <ContentArea>
        <FAQArea>
          <FAQTitle>
            <span>FAQ</span>
            <ClosingButton
              type="button"
              onClick={() => {
                updateMenuEntry();
              }}
            >
              &times;
            </ClosingButton>
          </FAQTitle>
          <QuestionList>{questionItems}</QuestionList>
        </FAQArea>
      </ContentArea>
    </CenteredColumn>
  );
}

FAQ.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

const ContentArea = styled.div`
  background: rgba(255, 255, 255, 0.75);
  height: 100%;
  width: 100%;
`;

const FAQArea = styled.div`
  background: #ffffff;
  border: 2px solid ${supportColor};
  height: 100%;
  padding: 20px;
  width: 50%;
`;

const FAQTitle = styled.div`
  align-items: center;
  color: ${supportColor};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin: -4px 0 0 0;
  padding: 0;
  width: 100%;

  > span {
    font-size: 25px;
    font-weight: bold;
  }
`;

const ClosingButton = styled.button`
  background: transparent;
  border: 0;
  cursor: pointer;
  padding: 0;
  font-size: 2rem;
`;

const QuestionList = styled.ul`
  font-size: 15px;
  list-style-type: none;
  margin: 0.25rem 0 0 0;
  padding: 0;
`;

export default FAQ;
