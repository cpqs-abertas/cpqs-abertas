import React, { useEffect } from "react";
import PropTypes from "prop-types";
import {
  getDashboardCount,
  getDashboardCountByDep,
  getDashboard,
  getDashboardByDep,
  getDashboardKeywords,
  getDashboardMap,
  getDashboardNationalMap,
} from "../../../common/API";
import {
  Conteudo,
  DivCard,
  DivInfo,
  DivGraph,
  DivInfoText,
  DivDep,
  DivInfoProf,
  barColors,
  departmentColors,
  wordCloudColors,
  instituteColor,
  departmentArrows,
  BarChartWrapper,
  LineChartWrapper,
  NationalMapWrapper,
  WordCloudWrapper,
  WorldMapWrapper,
} from "../../styles";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import BarChartContainer from "../../../common/charts/BarChartContainer";
import LineChartContainer from "../../../common/charts/LineChartContainer";
import NationalMapContainer from "../../../common/maps/NationalMapContainer";
import WordCloudContainer from "../../../common/keywords/WordCloudContainer";
import WorldMapContainer from "../../../common/maps/WorldMapContainer";
import FaderWrapper from "../wrappers/FaderWrapper";
import { setFilters } from "../../helpers";
import CarouselWrapper from "../../../fearp/components/wrappers/CarouselWrapper";

function Department(props) {
  const { setMenuFilter, dep, depDescription, chefe, viceChefe, children } =
    props;

  useEffect(() => {
    setFilters(setMenuFilter, { [dep]: true }, (filter, newState) => {
      newState[filter] = false;
    });
  }, []);

  return (
    <Conteudo style={{ justifyContent: "start" }}>
      <DivCard style={{ borderColor: departmentColors[dep] }}>
        <DivInfo>
          <DivDep style={{ backgroundColor: departmentColors[dep] }}>
            {dep}
          </DivDep>
          <DivInfoProf>
            <span style={{ fontSize: 25 }}>{depDescription}</span>
            <span>Chefe</span>
            <span>{chefe}</span>
            <span>Vice-Chefe</span>
            <span>{viceChefe}</span>
          </DivInfoProf>
        </DivInfo>
        <DivGraph style={{ paddingLeft: "20px" }}>
          <CarouselWrapper arrowImage={departmentArrows[dep]}>
            <WordCloudWrapper>
              <WordCloudContainer
                colors={wordCloudColors[dep]}
                fetchData={() =>
                  getDashboardKeywords({ limit: 50, departamento: dep })
                }
                Loading={FaderWrapper}
              />
            </WordCloudWrapper>
            <WorldMapWrapper>
              <WorldMapContainer
                color={instituteColor}
                fetchData={() => getDashboardMap({ departamento: dep })}
              />
            </WorldMapWrapper>
            <NationalMapWrapper>
              <NationalMapContainer
                color={instituteColor}
                fetchData={() => getDashboardNationalMap({ departamento: dep })}
              />
            </NationalMapWrapper>
            <BarChartWrapper>
              <BarChartContainer
                labels={["IME", dep]}
                colors={barColors.IME}
                fetchData={async () => {
                  const IMECount = await getDashboard();
                  const depCount = await getDashboardByDep({
                    departamento: dep,
                  });
                  return {
                    countByYearByCategoryByLabel: {
                      IME: IMECount,
                      [dep]: depCount,
                    },
                    categories: [
                      "Produção Artística",
                      "Produção Técnica",
                      "Produção Bibliográfica",
                      "Orientação",
                      "Bancas",
                      "Prêmios e Títulos",
                    ],
                  };
                }}
              />
            </BarChartWrapper>
            <LineChartWrapper>
              <LineChartContainer
                labels={[dep, "IME"]}
                color={{
                  IME: departmentColors.IME,
                  [dep]: departmentColors[dep],
                }}
                fill={{ IMECount: "-1", [dep]: "origin" }}
                fetchData={async () => {
                  const IMECountByYear = await getDashboardCount({
                    ano_inicio: 1978,
                  });
                  const countByYearByDep = await getDashboardCountByDep({
                    ano_inicio: 1978,
                    departamento: dep,
                  });
                  return {
                    IME: IMECountByYear.IME,
                    [dep]: countByYearByDep[dep],
                  };
                }}
              />
            </LineChartWrapper>
          </CarouselWrapper>
        </DivGraph>
        <DivInfoText>{children}</DivInfoText>
      </DivCard>
    </Conteudo>
  );
}

Department.propTypes = {
  chefe: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  dep: PropTypes.string.isRequired,
  depDescription: PropTypes.string.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
  viceChefe: PropTypes.string.isRequired,
};

export default Department;
