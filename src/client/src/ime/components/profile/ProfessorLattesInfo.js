import React from "react";
import PropTypes from "prop-types";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { departmentColors, DivInfo, DivInfoProf } from "../../styles";
import PhotoWrapper from "../wrappers/PhotoWrapper";

function ProfessorLattesInfo(props) {
  const { lattesId, fullName, contactNumber, menuFilter, departament } = props;

  return (
    <DivInfo>
      <PhotoWrapper id={lattesId} menuFilter={menuFilter} />
      <DivInfoProf color={departmentColors[departament]}>
        <span style={{ fontSize: 25 }}>{fullName}</span>
        <span>Telefone</span>
        {contactNumber ? <span>{contactNumber}</span> : <span>-</span>}
        <span>Currículo Lattes</span>
        <a
          href={`http://lattes.cnpq.br/${lattesId}`}
          target="_blank"
          rel="noreferrer"
        >
          http://lattes.cnpq.br/
          {lattesId}
        </a>
      </DivInfoProf>
    </DivInfo>
  );
}

ProfessorLattesInfo.propTypes = {
  contactNumber: PropTypes.string.isRequired,
  fullName: PropTypes.string.isRequired,
  lattesId: PropTypes.string.isRequired,
  menuFilter: PropTypes.object.isRequired,
};

export default ProfessorLattesInfo;
