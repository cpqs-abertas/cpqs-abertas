import React from "react";
import PropTypes from "prop-types";
import {
  Conteudo,
  DivCard,
  DivGraph,
  barColors,
  departmentColors,
  departmentArrows,
  BarChartWrapper,
  LineChartWrapper,
} from "../../styles";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import ProfessorLattesInfo from "./ProfessorLattesInfo";
import ProfessorActivityRecord from "./ProfessorActivityRecord";
import BarChartContainer from "../../../common/charts/BarChartContainer";
import LineChartContainer from "../../../common/charts/LineChartContainer";
import FaderWrapper from "../wrappers/FaderWrapper";
import { getDashboardCountByDepTipo } from "../../../common/API";
import { createCountByYear } from "../../helpers";
import CarouselWrapper from "../../../fearp/components/wrappers/CarouselWrapper";

function ProfessorActivityProfile(props) {
  const {
    chartsToRemove,
    detailsToDisplay,
    getCountPessoa,
    getCountPessoaPorAno,
    getTiposPessoa,
    menuFilter,
    productionDataKey,
    productionType,
    professorData,
    title,
  } = props;

  const chartsMap = {
    barChart: true,
    lineChart: true,
  };

  chartsToRemove.forEach((chart) => {
    chartsMap[chart] = false;
  });

  const fetchBarChartData = async () => {
    const id = professorData.id_lattes;
    const categories = await getTiposPessoa({ id });
    const countByYearByCategory = await getCountPessoa({
      id,
      tipos: categories,
    });

    return {
      categories,
      countByYearByCategoryByLabel: { DOCENTE: countByYearByCategory },
    };
  };

  const fetchLineChartData = async () => {
    const id = professorData.id_lattes;
    const dep = professorData.departamento;
    const countByYearByDep = await getDashboardCountByDepTipo({
      ano_inicio: 1978,
      tipo: productionType,
      departamento: dep,
    });
    const types = await getTiposPessoa({ id });
    const countByYearByProduction = await getCountPessoaPorAno({
      id,
      tipos: types,
    });
    const professorCountByYear = createCountByYear(countByYearByProduction);

    return { DOCENTE: professorCountByYear, [dep]: countByYearByDep[dep] };
  };

  const charts = {
    barChart: (
      <BarChartWrapper>
        <BarChartContainer
          labels={["DOCENTE"]}
          colors={barColors[professorData.departamento]}
          type="single"
          fetchData={fetchBarChartData}
          Loading={FaderWrapper}
        />
      </BarChartWrapper>
    ),
    lineChart: (
      <LineChartWrapper>
        <LineChartContainer
          labels={["DOCENTE", professorData.departamento]}
          color={{
            DOCENTE: departmentColors.DOCENTE,
            [professorData.departamento]:
              departmentColors[professorData.departamento],
          }}
          fill={{
            DOCENTE: "origin",
            [professorData.departamento]: "-1",
          }}
          fetchData={fetchLineChartData}
          Loading={FaderWrapper}
        />
      </LineChartWrapper>
    ),
  };

  const carouselItems = [];
  Object.keys(chartsMap).forEach((chart) => {
    if (chartsMap[chart]) {
      carouselItems.push(charts[chart]);
    }
  });

  return (
    <Conteudo style={{ justifyContent: "start" }}>
      <DivCard
        style={{ borderColor: departmentColors[professorData.departamento] }}
      >
        <ProfessorLattesInfo
          lattesId={professorData.id_lattes}
          contactNumber={professorData.contato}
          fullName={professorData.nome_completo}
          menuFilter={menuFilter}
          departament={professorData.departamento}
        />

        <DivGraph style={{ paddingLeft: "20px" }}>
          <CarouselWrapper
            arrowImage={departmentArrows[professorData.departamento]}
          >
            {carouselItems}
          </CarouselWrapper>
        </DivGraph>

        <ProfessorActivityRecord
          lattesId={professorData.id_lattes}
          professorData={professorData[productionDataKey]}
          activityType={title}
          detailsToDisplay={detailsToDisplay}
          departament={professorData.departamento}
        />
      </DivCard>
    </Conteudo>
  );
}

ProfessorActivityProfile.propTypes = {
  chartsToRemove: PropTypes.array,
  detailsToDisplay: PropTypes.object.isRequired,
  getCountPessoa: PropTypes.func,
  getCountPessoaPorAno: PropTypes.func.isRequired,
  getTiposPessoa: PropTypes.func,
  menuFilter: PropTypes.object.isRequired,
  productionDataKey: PropTypes.string.isRequired,
  productionType: PropTypes.string.isRequired,
  professorData: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
};

ProfessorActivityProfile.defaultProps = {
  chartsToRemove: [],
  getCountPessoa: () => {},
  getTiposPessoa: () => {},
};

export default ProfessorActivityProfile;
