import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import CNPQLogo from "../assets/images/cnpq-logo.png";

function Footer(props) {
  const { children, font, lastDataUpdateDate, lastDataUpdateNoticeColor } =
    props;

  return (
    <Container style={{ fontFamily: font }}>
      <ContainerTop>
        <b
          style={{
            color: lastDataUpdateNoticeColor,
            display: "block",
            marginTop: "0.5rem",
          }}
        >
          Última atualização: {lastDataUpdateDate || "não há registro."}
        </b>
      </ContainerTop>

      <ContainerBottom>
        {children}
        <a href="http://www.cnpq.br/" target="_blank" rel="noopener noreferrer">
          <img style={{ height: 41 }} src={CNPQLogo} alt="Logo CNPQ" />
        </a>
      </ContainerBottom>
    </Container>
  );
}

Footer.propTypes = {
  children: PropTypes.node.isRequired,
  font: PropTypes.string,
  lastDataUpdateDate: PropTypes.string.isRequired,
  lastDataUpdateNoticeColor: PropTypes.string,
};

Footer.defaultProps = {
  font: "sans-serif",
  lastDataUpdateNoticeColor: "",
};

export default Footer;

const Container = styled.div`
  grid-column-start: 3;
  grid-row-start: 3;
  width: 100%;
`;

const ContainerTop = styled.div`
  text-align: right;
`;

const ContainerBottom = styled.div`
  display: flex;
  justify-content: space-between;
  overflow: hidden;
  padding-top: 30px;
  width: 100%;
`;
