import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Link } from "react-router-dom";

function Header(props) {
  const {
    title,
    titleColor,
    subtitle,
    subtitleColor,
    fontFamily,
    instituteLogo,
    logoWidth,
    logoHeight,
  } = props;

  return (
    <Container>
      <Text fontFamily={fontFamily}>
        <Link to="/">
          <Title color={titleColor}>{title}</Title>
        </Link>
        <Link to="/">
          <Subtitle color={subtitleColor}>{subtitle}</Subtitle>
        </Link>
      </Text>
      <Link style={{ alignItems: "center", display: "flex" }} to="/">
        <img
          width={logoWidth}
          height={logoHeight}
          src={instituteLogo}
          alt="logo"
        />
      </Link>
    </Container>
  );
}

Header.propTypes = {
  fontFamily: PropTypes.string,
  instituteLogo: PropTypes.string.isRequired,
  logoHeight: PropTypes.number.isRequired,
  logoWidth: PropTypes.number.isRequired,
  subtitle: PropTypes.string.isRequired,
  subtitleColor: PropTypes.string,
  title: PropTypes.string.isRequired,
  titleColor: PropTypes.string,
};

Header.defaultProps = {
  fontFamily: "sans-serif",
  subtitleColor: "#000000",
  titleColor: "#000000",
};

const Container = styled.div`
  display: flex;
  grid-column-start: 3;
  grid-row-start: 1;
  justify-content: space-between;
  overflow: hidden;
  width: 100%;
`;

const Text = styled.div`
  display: flex;
  font-family: ${(props) => props.fontFamily};

  > a {
    align-items: center;
    display: flex;
    height: 100%;
    text-decoration: none !important;
  }
`;

const Title = styled.div`
  color: ${(props) => props.color};
  font-size: 30px;
  font-weight: bold;
`;

const Subtitle = styled.span`
  color: ${(props) => props.color};
  font-size: 26px;
  margin-left: 9px;
`;

export default Header;
