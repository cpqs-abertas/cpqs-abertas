import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

function Photo(props) {
  const { borderColor, image } = props;
  const style = {
    backgroundImage: `url(${image})`,
    backgroundPosition: "center top",
    border: `8px solid ${borderColor}`,
  };

  return <DivPhoto style={style} />;
}

Photo.propTypes = {
  image: PropTypes.string.isRequired,
  borderColor: PropTypes.string.isRequired,
};

const DivPhoto = styled.div`
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  border-radius: 50%;
  height: 100%;
  width: 100%;
`;

export default Photo;
