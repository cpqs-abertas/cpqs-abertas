import axios from "axios";

const url = process.env.REACT_APP_BACKEND_URL;

const fetchApi = (base, params = {}) => {
  const request = url + base;

  return axios.get(request, { params }).then((res) => res.data);
};

const postApi = (base, params = {}) => {
  const request = url + base;

  return axios.post(request, { params }).then((res) => {
    if (res.status !== 200) {
      return null;
    }
    return res.data.result;
  });
};

const getDashboard = () => fetchApi("/dashboard/").then((data) => data);

const getDashboardAggregated = () =>
  fetchApi("/dashboard/aggregated-dashboard").then((data) => data);

const getProdArtCount = (params) =>
  fetchApi("/producao_artistica/aggregated-count", params).then((data) => data);

const getProdArtCountByType = (params) =>
  fetchApi("/producao_artistica/aggregated-count-by-type", params).then(
    (data) => data
  );

const getProdArtTypesCount = (params) =>
  fetchApi("/producao_artistica/types-count", params).then((data) => data);

const getProdArtKeywords = (params) =>
  fetchApi("/producao_artistica/aggregated-keywords", params).then(
    (data) => data.keywords
  );

const getProdArtMap = () =>
  fetchApi("/producao_artistica/aggregated-map").then((data) => data.countries);

const getProdArtNationalMap = (params) =>
  fetchApi("/producao_artistica/aggregated-national-map", params).then(
    (data) => data
  );

const getProdBibCount = (params) =>
  fetchApi("/producao_bibliografica/aggregated-count", params).then(
    (data) => data
  );

const getProdBibCountByType = (params) =>
  fetchApi("/producao_bibliografica/aggregated-count-by-type", params).then(
    (data) => data
  );

const getProdBibTypesCount = (params) =>
  fetchApi("/producao_bibliografica/types-count", params).then((data) => data);

const getProdBibKeywords = (params) =>
  fetchApi("/producao_bibliografica/aggregated-keywords", params).then(
    (data) => data.keywords
  );

const getProdBibMap = () =>
  fetchApi("/producao_bibliografica/aggregated-map").then(
    (data) => data.countries
  );

const getProdBibNationalMap = (params) =>
  fetchApi("/producao_bibliografica/aggregated-national-map", params).then(
    (data) => data
  );

const getProdTecCount = (params) =>
  fetchApi("/producao_tecnica/aggregated-count", params).then((data) => data);

const getProdTecCountByType = (params) =>
  fetchApi("/producao_tecnica/aggregated-count-by-type", params).then(
    (data) => data
  );

const getProdTecTypesCount = (params) =>
  fetchApi("/producao_tecnica/types-count", params).then((data) => data);

const getProdTecKeywords = (params) =>
  fetchApi("/producao_tecnica/aggregated-keywords", params).then(
    (data) => data.keywords
  );

const getProdTecMap = () =>
  fetchApi("/producao_tecnica/aggregated-map").then((data) => data.countries);

const getProdTecNationalMap = (params) =>
  fetchApi("/producao_tecnica/aggregated-national-map", params).then(
    (data) => data
  );

const getOrientacaoCount = (params) =>
  fetchApi("/orientacao/aggregated-count", params).then((data) => data);

const getOrientacaoCountByType = (params) =>
  fetchApi("/orientacao/aggregated-count-by-type", params).then((data) => data);

const getOrientacaoTypesCount = (params) =>
  fetchApi("/orientacao/types-count", params).then((data) => data);

const getOrientacaoKeywords = (params) =>
  fetchApi("/orientacao/aggregated-keywords", params).then(
    (data) => data.keywords
  );

const getOrientacaoMap = () =>
  fetchApi("/orientacao/aggregated-map").then((data) => data.countries);

const getBancasCount = (params) =>
  fetchApi("/bancas/aggregated-count", params).then((data) => data);

const getBancasCountByType = (params) =>
  fetchApi("/bancas/aggregated-count-by-type", params).then((data) => data);

const getBancasTypesCount = (params) =>
  fetchApi("/bancas/types-count", params).then((data) => data);

const getBancasKeywords = (params) =>
  fetchApi("/bancas/aggregated-keywords", params).then((data) => data.keywords);

const getBancasMap = () =>
  fetchApi("/bancas/aggregated-map").then((data) => data.countries);

const getPremiosCount = (params) =>
  fetchApi("/premios/aggregated-count", params).then((data) => data);

const getPessoa = (params) => fetchApi("/pessoa", params).then((data) => data);

const getAllPessoa = () => fetchApi("/pessoa/all").then((data) => data);

const getPessoaNames = () =>
  fetchApi("/pessoa/aggregated-names").then((data) => data.names);

const getCountPessoaPorDep = (params) =>
  fetchApi("/pessoa/countPorDep", params).then((data) => data);

const getKeywordsPessoa = (params) =>
  fetchApi("/pessoa/keywords", params).then((data) => data.keywords);

const getMapPessoa = (params) =>
  fetchApi("/pessoa/map", params).then((data) => data.countries);

const getMapPessoaNational = (params) =>
  fetchApi("/pessoa/map-national", params).then((data) => data);

const getTiposProdArtPessoa = (params) =>
  fetchApi("/pessoa/tiposProdArt", params).then(
    (data) => data.production_types
  );

const getCountProdArtPessoa = (params) =>
  fetchApi("/pessoa/count/prodArt", params).then((data) => data);

const getCountProdArtPessoaPorAno = (params) =>
  fetchApi("/pessoa/countPorAno/prodArt", params).then((data) => data);

const getTiposProdTecPessoa = (params) =>
  fetchApi("/pessoa/tiposProdTec", params).then(
    (data) => data.production_types
  );

const getCountProdTecPessoa = (params) =>
  fetchApi("/pessoa/count/prodTec", params).then((data) => data);

const getCountProdTecPessoaPorAno = (params) =>
  fetchApi("/pessoa/countPorAno/prodTec", params).then((data) => data);

const getTiposProdBibPessoa = (params) =>
  fetchApi("/pessoa/tiposProdBib", params).then(
    (data) => data.production_types
  );

const getCountProdBibPessoa = (params) =>
  fetchApi("/pessoa/count/prodBib", params).then((data) => data);

const getCountProdBibPessoaPorAno = (params) =>
  fetchApi("/pessoa/countPorAno/prodBib", params).then((data) => data);

const getTiposPartPessoa = (params) =>
  fetchApi("/pessoa/tiposPart", params).then((data) => data);

const getCountPartPessoa = (params) =>
  fetchApi("/pessoa/count/part", params).then((data) => data);

const getCountPartPessoaPorAno = (params) =>
  fetchApi("/pessoa/countPorAno/part", params).then((data) => data);

const getTiposBancaPessoa = (params) =>
  fetchApi("/pessoa/tiposBanca", params).then((data) => data.production_types);

const getCountBancaPessoa = (params) =>
  fetchApi("/pessoa/count/banca", params).then((data) => data);

const getCountBancaPessoaPorAno = (params) =>
  fetchApi("/pessoa/countPorAno/banca", params).then((data) => data);

const getTiposOriPessoa = (params) =>
  fetchApi("/pessoa/tiposOri", params).then((data) => data.production_types);

const getCountOriPessoa = (params) =>
  fetchApi("/pessoa/count/ori", params).then((data) => data);

const getCountOriPessoaPorAno = (params) =>
  fetchApi("/pessoa/countPorAno/ori", params).then((data) => data);

const getCountPremioPessoaPorAno = (params) =>
  fetchApi("/pessoa/countPorAno/premio", params).then((data) => data);

const getImg = async (id) =>
  fetchApi("/pessoa/picture", { id }).then(
    (data) =>
      `http://servicosweb.cnpq.br/wspessoa/servletrecuperafoto?tipo=1&id=${data.picture}`
  );

const getSearchResult = (params) =>
  fetchApi("/dashboard/search", params).then((data) => data);

const getDashboardCount = (params) =>
  fetchApi("/dashboard/count", params).then((data) => data);

const getDashboardAggregatedCount = (params) =>
  fetchApi("/dashboard/aggregated-count", params).then((data) => data);

const getDashboardCountByDep = (params) =>
  fetchApi("/dashboard/countByDep", params).then((data) => data);

const getDashboardByDep = (params) =>
  fetchApi("/dashboard/indexByDep", params).then((data) => data);

const getDashboardKeywords = (params) =>
  fetchApi("/dashboard/keywords", params).then((data) => data.keywords);

const getDashboardMap = (params) =>
  fetchApi("/dashboard/map", params).then((data) => data.countries);

const getDashboardAggregatedMap = (params) =>
  fetchApi("/dashboard/aggregated-map", params).then((data) => data.countries);

const getDashboardNationalMap = (params) =>
  fetchApi("/dashboard/national-map", params).then((data) => data);

const getDashboardAggregatedNationalMap = (params) =>
  fetchApi("/dashboard/aggregated-national-map", params).then((data) => data);

const getDashboardCountByDepTipo = (params) =>
  fetchApi("/dashboard/countByDepTipo", params).then((data) => data);

const getLastUpdateDate = (params) =>
  fetchApi("/dashboard/last-updated", params).then((data) => data.date);

const getCountAllPessoa = (params) =>
  fetchApi("/pessoa/countAll", params).then((data) => data);

const postQuestion = (params) =>
  postApi("/contato/faq", params).then((data) => data);

export {
  getDashboard,
  getDashboardAggregated,
  getProdArtCount,
  getProdArtCountByType,
  getProdArtTypesCount,
  getProdArtKeywords,
  getProdArtMap,
  getProdArtNationalMap,
  getProdBibCount,
  getProdBibCountByType,
  getProdBibTypesCount,
  getProdBibKeywords,
  getProdBibMap,
  getProdBibNationalMap,
  getProdTecCount,
  getProdTecCountByType,
  getProdTecTypesCount,
  getProdTecKeywords,
  getProdTecMap,
  getProdTecNationalMap,
  getOrientacaoCount,
  getOrientacaoCountByType,
  getOrientacaoTypesCount,
  getOrientacaoKeywords,
  getOrientacaoMap,
  getBancasCount,
  getBancasCountByType,
  getBancasTypesCount,
  getBancasKeywords,
  getBancasMap,
  getPremiosCount,
  getPessoa,
  getAllPessoa,
  getPessoaNames,
  getCountPessoaPorDep,
  getKeywordsPessoa,
  getMapPessoa,
  getMapPessoaNational,
  getTiposProdArtPessoa,
  getCountProdArtPessoa,
  getCountProdArtPessoaPorAno,
  getTiposProdTecPessoa,
  getCountProdTecPessoa,
  getCountProdTecPessoaPorAno,
  getTiposProdBibPessoa,
  getCountProdBibPessoa,
  getCountProdBibPessoaPorAno,
  getTiposPartPessoa,
  getCountPartPessoa,
  getCountPartPessoaPorAno,
  getTiposBancaPessoa,
  getCountBancaPessoa,
  getCountBancaPessoaPorAno,
  getTiposOriPessoa,
  getCountOriPessoa,
  getCountOriPessoaPorAno,
  getCountPremioPessoaPorAno,
  getImg,
  getSearchResult,
  getDashboardCount,
  getDashboardAggregatedCount,
  getDashboardCountByDep,
  getDashboardByDep,
  getDashboardKeywords,
  getDashboardMap,
  getDashboardAggregatedMap,
  getDashboardNationalMap,
  getDashboardAggregatedNationalMap,
  getDashboardCountByDepTipo,
  getLastUpdateDate,
  getCountAllPessoa,
  postQuestion,
};
