import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Line } from "react-chartjs-2";
import { Chart as ChartJS, registerables } from "chart.js";
import styled from "styled-components";
import Slider from "rc-slider";
import "rc-slider/assets/index.css";

// ChartJS.register(BarElement, CategoryScale, LinearScale);
ChartJS.register(...registerables);

const MIN_YEAR = 1978;
const MAX_YEAR = new Date().getFullYear();

function LineChart(props) {
  const {
    color,
    fill,
    initialEndYear,
    initialStartYear,
    labels,
    countByYearByLabel,
  } = props;
  const [lineData, setLineData] = useState(null);
  const [yearsRange, setYearsRange] = useState([
    initialStartYear,
    initialEndYear,
  ]);

  useEffect(() => {
    const data = {
      labels: [],
      datasets: [],
    };

    const [startYear, endYear] = yearsRange;
    for (let i = 0; i < endYear - startYear + 1; i++) {
      data.labels.push(startYear + i);
    }

    labels.forEach((label) => {
      if (countByYearByLabel[label]) {
        const dataset = {
          label,
          data: [],
          fill: fill[label],
          borderColor: color[label],
          backgroundColor: color[label],
        };

        data.labels.forEach((year) => {
          if (countByYearByLabel[label][year]) {
            dataset.data.push(countByYearByLabel[label][year]);
          } else {
            dataset.data.push(0);
          }
        });

        data.datasets.push(dataset);
      }
    });

    setLineData(data);
  }, [yearsRange]);

  const options = {
    legend: { display: true },
    maintainAspectRatio: true,
    responsive: false,
    elements: {
      line: {
        tension: 0,
      },
    },
  };

  return (
    lineData && (
      <>
        <SliderWrapper>
          <span>{MIN_YEAR}</span>
          <Slider
            range
            className="t-slider"
            min={MIN_YEAR}
            max={MAX_YEAR}
            defaultValue={[yearsRange[0], yearsRange[1]]}
            onChange={setYearsRange}
          />
          <span>{MAX_YEAR}</span>
        </SliderWrapper>
        <Years>
          <span>de</span>
          <span>
            <b>{yearsRange[0]}</b>
          </span>
          <span>a</span>
          <span>
            <b>{yearsRange[1]}</b>
          </span>
        </Years>

        <Line data={lineData} options={options} width={450} height={200} />
      </>
    )
  );
}

LineChart.propTypes = {
  color: PropTypes.object.isRequired,
  fill: PropTypes.object.isRequired,
  initialEndYear: PropTypes.number,
  initialStartYear: PropTypes.number,
  labels: PropTypes.array.isRequired,
  countByYearByLabel: PropTypes.object.isRequired,
};

LineChart.defaultProps = {
  initialEndYear: MAX_YEAR - 1,
  initialStartYear: 1990,
};

export default LineChart;

const SliderWrapper = styled.div`
  color: #000;
  display: flex;
  flex-direction: row;
  font-family: Sans-Serif;
  justify-content: center;
  padding-top: 8px;
  width: 450px;

  > span {
    :nth-child(1) {
      padding-right: 12px;
    }
    :nth-child(3) {
      padding-left: 12px;
    }
    font-size: 14px;
    margin-top: -3px;
    opacity: 0.5;
  }
`;

const Years = styled.div`
  display: flex;
  flex-direction: row;
  font-size: 20px;
  justify-content: center;
  padding: 8px 0px 8px;
  width: 450px;

  > span {
    :nth-child(2) {
      padding: 0px 8px;
    }

    :nth-child(4) {
      padding-left: 8px;
    }
  }
`;
