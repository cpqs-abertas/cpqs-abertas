import React from "react";
import PropTypes from "prop-types";
import { Bar } from "react-chartjs-2";

function BarChart(props) {
  const { categories, countByCategoryByLabel, colors, labels, type } = props;

  let barPercentage;
  if (type === "single") {
    barPercentage = 0.4;
  } else {
    barPercentage = 0.8;
  }

  const data = {
    labels,
    datasets: categories.map((category) => ({
      barPercentage: barPercentage,
      label: category,
      data: [],
      category,
    })),
  };

  data.datasets.forEach((dataset, index) => {
    dataset.backgroundColor = colors[index % categories.length];
    data.labels.forEach((label) => {
      const countByCategory = countByCategoryByLabel[label];
      if (countByCategory) {
        dataset.data.push(countByCategory[dataset.category]);
      }
    });
  });

  const options = {
    indexAxis: "y",
    animation: {
      duration: 1500,
    },
    scales: {
      x: {
        stacked: true,
        grid: { display: true },
      },
      y: {
        stacked: true,
        grid: { display: false },
      },
    },
    legend: {
      display: true,
      labels: {
        padding: 10,
        boxWidth: 20,
      },
    },
    maintainAspectRatio: true,
    responsive: false,
    tooltips: {
      mode: "nearest",
    },
  };

  return data && <Bar data={data} options={options} width={450} height={245} />;
}

BarChart.propTypes = {
  categories: PropTypes.array.isRequired,
  countByCategoryByLabel: PropTypes.object.isRequired,
  colors: PropTypes.array.isRequired,
  labels: PropTypes.array.isRequired,
  type: PropTypes.string,
};

BarChart.defaultProps = {
  type: "normal",
};

export default BarChart;
