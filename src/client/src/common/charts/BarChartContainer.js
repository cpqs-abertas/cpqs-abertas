import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import BarChart from "./BarChart";
import { dict } from "../dict";

const formatCategoriesThroughDict = (categories, countByCategoryByLabel) => {
  const formattedCategories = categories.map((category) => dict(category));
  const formattedCountByCategoryByLabel = {};

  Object.keys(countByCategoryByLabel).forEach((label) => {
    if (countByCategoryByLabel[label]) {
      formattedCountByCategoryByLabel[label] = {};
      Object.keys(countByCategoryByLabel[label]).forEach((category) => {
        const count = countByCategoryByLabel[label][category];
        formattedCountByCategoryByLabel[label][dict(category)] = count;
      });
    }
  });

  return {
    categories: formattedCategories,
    countByCategoryByLabel: formattedCountByCategoryByLabel,
  };
};

function BarChartContainer(props) {
  const { fetchData, formatData, labels, colors, type, Loading } = props;
  const [barChartData, setBarChartData] = useState(null);

  useEffect(() => {
    fetchData().then((data) => {
      const { categories, countByYearByCategoryByLabel } = data;
      const countByCategoryByLabel = formatData(countByYearByCategoryByLabel);
      setBarChartData(
        formatCategoriesThroughDict(categories, countByCategoryByLabel)
      );
    });
  }, []);

  return (
    <>
      {!barChartData ? (
        <Loading />
      ) : (
        <BarChart
          categories={barChartData.categories}
          countByCategoryByLabel={barChartData.countByCategoryByLabel}
          colors={colors}
          labels={labels}
          type={type}
        />
      )}
    </>
  );
}

BarChartContainer.propTypes = {
  colors: PropTypes.array.isRequired,
  fetchData: PropTypes.func.isRequired,
  formatData: PropTypes.func,
  labels: PropTypes.array.isRequired,
  type: PropTypes.string,
  Loading: PropTypes.func,
};

BarChartContainer.defaultProps = {
  formatData: (data) => data,
  type: "normal",
  Loading: () => <></>,
};

export default BarChartContainer;
