import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import WordCloud from "./WordCloud";

function WordCloudContainer(props) {
  const { fetchData, formatData, colors, Loading } = props;
  const [words, setWords] = useState(null);

  useEffect(() => {
    fetchData().then((data) => {
      setWords(
        formatData(data).map((obj) => ({
          text: Object.keys(obj)[0],
          value: Object.values(obj)[0],
        }))
      );
    });
  }, []);

  return (
    <>{!words ? <Loading /> : <WordCloud words={words} colors={colors} />}</>
  );
}

WordCloudContainer.propTypes = {
  colors: PropTypes.array.isRequired,
  fetchData: PropTypes.func.isRequired,
  formatData: PropTypes.func,
  Loading: PropTypes.func,
};

WordCloudContainer.defaultProps = {
  formatData: (data) => data,
  Loading: () => <></>,
};

export default WordCloudContainer;
