import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
  Conteudo,
  departmentColors,
  DivDetailBlock,
  DivInfoText,
  DivLink,
} from "../../styles";

function ProfessorActivityRecord(props) {
  const {
    lattesId,
    professorData,
    activityType,
    detailsToDisplay,
    departament,
  } = props;

  if (!professorData) {
    return (
      <Conteudo style={{ justifyContent: "start" }}>
        Não foi possível encontrar essa pessoa
      </Conteudo>
    );
  }

  const detailBlocks = [];
  const sortedData = professorData.sort(
    (a, b) => Number(b.ano) - Number(a.ano)
  );
  if (sortedData.length === 0) {
    detailBlocks.push(
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          margin: "20px 0px",
        }}
      >
        <span style={{ marginTop: -20 }}>
          <b>0 resultados</b>
        </span>
      </div>
    );
  }

  const productionsByYear = {};
  sortedData.forEach((production) => {
    if (productionsByYear[production.ano]) {
      productionsByYear[production.ano].push(production);
    } else {
      productionsByYear[production.ano] = [production];
    }
  });

  Object.keys(productionsByYear).forEach((year) => {
    productionsByYear[year].forEach((production) => {
      detailBlocks.push(
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            fontSize: 15,
            margin: "20px 0px",
          }}
        >
          {detailsToDisplay.map((detail) => (
            <DivDetailBlock color={departmentColors[departament]}>
              <b>{detail.description}</b>{" "}
              {detail.makeText(production) || "Não informado"}
            </DivDetailBlock>
          ))}
        </div>
      );
    });

    detailBlocks.push(<span id="ano">{year}</span>);
  });

  const closingXStyle = {
    color: "#000",
    cursor: "pointer",
    fontFamily: "cursive",
    fontSize: "20px",
    position: "absolute",
    right: "30px",
    textDecoration: "none",
  };

  return (
    <DivInfoText>
      <span
        style={{
          fontSize: 25,
          fontWeight: "bold",
          marginTop: -3.8,
        }}
      >
        {activityType}
        <Link to={`/pessoa/perfil/${lattesId}`} style={closingXStyle}>
          X
        </Link>
      </span>
      <DivLink>
        {detailBlocks.reverse().map((element) => (
          <div key={element}>{element}</div>
        ))}
      </DivLink>
    </DivInfoText>
  );
}

ProfessorActivityRecord.propTypes = {
  activityType: PropTypes.string.isRequired,
  departament: PropTypes.string.isRequired,
  detailsToDisplay: PropTypes.object.isRequired,
  lattesId: PropTypes.string.isRequired,
  professorData: PropTypes.array.isRequired,
};

export default ProfessorActivityRecord;
