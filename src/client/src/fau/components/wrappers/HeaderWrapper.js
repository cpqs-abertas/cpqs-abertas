import React from "react";
import Header from "../../../common/misc/Header";
import FAULogo from "../../assets/images/fau.png";

function HeaderWrapper() {
  return (
    <Header
      title="FAU Aberta"
      titleColor="#000000"
      subtitle="| Produção Intelectual da FAUUSP"
      subtitleColor="#000000"
      fontFamily="sans-serif"
      instituteLogo={FAULogo}
      logoWidth={138}
      logoHeight={60}
    />
  );
}

export default HeaderWrapper;
