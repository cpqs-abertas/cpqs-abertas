import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
  Conteudo,
  DivCard,
  DivGraph,
  DivInfoProducao,
  DivInfoText,
  DivTitle,
  DivTotal,
  barColors,
  departmentColors,
  wordCloudColors,
  BarChartWrapper,
  LineChartWrapper,
  NationalMapWrapper,
  WordCloudWrapper,
  WorldMapWrapper,
} from "../../styles";
import BarChartContainer from "../../../common/charts/BarChartContainer";
import LineChartContainer from "../../../common/charts/LineChartContainer";
import NationalMapContainer from "../../../common/maps/NationalMapContainer";
import WordCloudContainer from "../../../common/keywords/WordCloudContainer";
import WorldMapContainer from "../../../common/maps/WorldMapContainer";
import SpinnerWrapper from "../wrappers/SpinnerWrapper";
import { getDashboard } from "../../../common/API";
import {
  createCountByCategoryByLabel,
  createCountByYearByDep,
  createFAUCountByYear,
} from "../../helpers";

function Activity(props) {
  const {
    chartsToRemove,
    getCount,
    getCountByType,
    getKeywords,
    getMap,
    getNationalMap,
    infoText,
    type,
    updateMenuEntry,
  } = props;
  const [countByProductionType, setCountByProductionType] = useState(null);

  const chartsMap = {
    wordCloud: true,
    worldMap: true,
    nationalMap: true,
    barChart: true,
    FAULineChart: true,
    depsLineChart: true,
  };

  chartsToRemove.forEach((chart) => {
    chartsMap[chart] = false;
  });

  useEffect(() => {
    updateMenuEntry();

    getDashboard().then((responsePayload) => {
      setCountByProductionType(responsePayload);
    });
  }, []);

  if (!countByProductionType) {
    return (
      <Conteudo
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <SpinnerWrapper />
      </Conteudo>
    );
  }

  const charts = {
    wordCloud: (
      <WordCloudWrapper>
        <WordCloudContainer
          colors={wordCloudColors.default}
          fetchData={() => getKeywords({ limit: 50 })}
          Loading={SpinnerWrapper}
        />
      </WordCloudWrapper>
    ),
    worldMap: (
      <WorldMapWrapper>
        <WorldMapContainer
          fetchData={() => getMap()}
          Loading={SpinnerWrapper}
        />
      </WorldMapWrapper>
    ),
    nationalMap: (
      <NationalMapWrapper>
        <NationalMapContainer
          fetchData={() => getNationalMap()}
          Loading={SpinnerWrapper}
        />
      </NationalMapWrapper>
    ),
    barChart: (
      <BarChartWrapper>
        <BarChartContainer
          labels={["AUH", "AUT", "AUP"]}
          colors={barColors.FAU}
          formatData={createCountByCategoryByLabel}
          fetchData={async () => {
            const countByYearByProductionTypeByDep = await getCountByType();
            const categories = Object.keys(
              Object.values(countByYearByProductionTypeByDep)[0]
            );
            return {
              categories,
              countByYearByCategoryByLabel: countByYearByProductionTypeByDep,
            };
          }}
          Loading={SpinnerWrapper}
        />
      </BarChartWrapper>
    ),
    FAULineChart: (
      <LineChartWrapper>
        <LineChartContainer
          labels={["FAU"]}
          color={{ FAU: departmentColors.FAU }}
          fill={{ FAU: "origin" }}
          formatData={createFAUCountByYear}
          fetchData={() => getCount({ ano_inicio: 1978 })}
          Loading={SpinnerWrapper}
        />
      </LineChartWrapper>
    ),
    depsLineChart: (
      <LineChartWrapper>
        <LineChartContainer
          labels={["AUH", "AUT", "AUP"]}
          color={{
            AUH: departmentColors.AUH,
            AUP: departmentColors.AUP,
            AUT: departmentColors.AUT,
          }}
          fill={{ AUH: false, AUP: false, AUT: false }}
          formatData={createCountByYearByDep}
          fetchData={() => getCount({ ano_inicio: 1978 })}
          Loading={SpinnerWrapper}
        />
      </LineChartWrapper>
    ),
  };

  const carouselItems = [];
  Object.keys(chartsMap).forEach((chart) => {
    if (chartsMap[chart]) {
      carouselItems.push(charts[chart]);
    }
  });

  return (
    <Conteudo style={{ justifyContent: "start" }}>
      <DivCard style={{ border: "none" }}>
        <DivInfoProducao>
          <DivTitle>
            <span>{type}</span>
          </DivTitle>
          <DivTotal>
            <span style={{ fontSize: 100 }}>
              {Intl.NumberFormat().format(countByProductionType[type])}
            </span>
            <span>resultados</span>
          </DivTotal>
        </DivInfoProducao>
        <DivGraph>
          <Carousel
            height="100%"
            showArrows
            showThumbs={false}
            showIndicators={false}
            autoPlay
            showStatus={false}
            infiniteLoop
            interval={5000}
          >
            {carouselItems}
          </Carousel>
        </DivGraph>
        <DivInfoText style={{ paddingTop: 0 }}>
          <span
            style={{
              fontSize: 25,
              fontWeight: "bold",
              marginTop: -3.8,
            }}
          >
            Apresentação
          </span>
          <span>{infoText}</span>
        </DivInfoText>
      </DivCard>
    </Conteudo>
  );
}

Activity.propTypes = {
  chartsToRemove: PropTypes.array,
  getCount: PropTypes.func,
  getCountByType: PropTypes.func,
  getKeywords: PropTypes.func,
  getMap: PropTypes.func,
  getNationalMap: PropTypes.func,
  infoText: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  updateMenuEntry: PropTypes.func.isRequired,
};

Activity.defaultProps = {
  chartsToRemove: [],
  getCount: () => {},
  getCountByType: () => {},
  getKeywords: () => {},
  getMap: () => {},
  getNationalMap: () => {},
};

export default Activity;
