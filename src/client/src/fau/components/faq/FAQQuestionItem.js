import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

function FAQQuestionItem(props) {
  const {
    text,
    answer,
    number,
    questionCount,
    selectedQuestionNumber,
    setSelectedQuestionNumber,
  } = props;

  const handleQuestionItemClick = (event) => {
    let target = event.target;
    while (!target.getAttribute("data-number")) {
      target = target.parentNode;
    }

    const questionNumber = Number(target.getAttribute("data-number"));
    if (questionNumber === selectedQuestionNumber) {
      setSelectedQuestionNumber(-1);
      return;
    }

    setSelectedQuestionNumber(questionNumber);
  };

  return (
    <li
      key={text}
      style={{
        borderBottom: number < questionCount - 1 ? "1px solid black" : "",
        padding: "0.4rem 0",
      }}
    >
      <ItemButton
        data-number={number}
        type="button"
        onClick={handleQuestionItemClick}
      >
        {text}
        {selectedQuestionNumber === number && <>&nbsp;&#9660;</>}
        {selectedQuestionNumber !== number && <>&nbsp;&#9658;</>}
      </ItemButton>
      <div
        style={{
          display: selectedQuestionNumber === number ? "block" : "none",
          marginTop: "0.25rem",
        }}
      >
        {answer}
      </div>
    </li>
  );
}

FAQQuestionItem.propTypes = {
  answer: PropTypes.string.isRequired,
  number: PropTypes.number.isRequired,
  questionCount: PropTypes.number.isRequired,
  selectedQuestionNumber: PropTypes.number.isRequired,
  setSelectedQuestionNumber: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};

const ItemButton = styled.button`
  background: transparent;
  border: 0;
  cursor: pointer;
  font-size: 15px;
  font-weight: bold;
  padding: 0;
  text-align: left;
`;

export default FAQQuestionItem;
