const setFilters = (setState, filtersToIsActiveMap, handleFilter) => {
  setState((prevState) => {
    const newState = {};
    Object.keys(prevState).forEach((filter) => {
      handleFilter(filter, newState, prevState);
    });

    Object.keys(filtersToIsActiveMap).forEach((filter) => {
      newState[filter] = filtersToIsActiveMap[filter];
    });

    return newState;
  });
};

const createCountByCategoryByLabel = (countByYearByCategoryByLabel) => {
  /* Example
    - params:
      - countByYearByCategoryByLabel = {
        label1: {
          category1: {
            ...
            1990: 10,
            1991: 3,
            ...
          },
          ...
        },
        ...
      }
    - return value = {
        label1: {
          category1: 13,
          ...
        },
        ...
      }
  */

  const countByCategoryByLabel = {};
  Object.keys(countByYearByCategoryByLabel).forEach((label) => {
    countByCategoryByLabel[label] = {};
    Object.keys(countByYearByCategoryByLabel[label]).forEach((category) => {
      const countByYear = countByYearByCategoryByLabel[label][category];
      countByCategoryByLabel[label][category] = Object.values(
        countByYear
      ).reduce((total, current) => total + current);
    });
  });

  return countByCategoryByLabel;
};

const createCountByYear = (countByYearByProductionType) => {
  /*  Example:
      - params:
        - countByYearByProductionType = {
          productionType1: {
            ...
            1990: 10,
            1991: 3,
            ...
          },
          productionType2: {
            ...
            1990: 7,
            1991: 7,
            ...
          },
        }

      - return value = {
          ...
          1990: 17,
          1991: 10,
          ...
        }
  */

  const data = {};

  Object.values(countByYearByProductionType).forEach((countByYear) => {
    Object.keys(countByYear).forEach((year) => {
      data[year] = (data[year] || 0) + countByYear[year];
    });
  });

  return data;
};

const createCountByYearByDep = (productionTypeYearCountByDep) => {
  /*  Example:
      - params
        - productionTypeYearCountByDep = {
            department1: {
              productionType1: {
                1990: 10,
                1991: 3,
              },
              productionType2: {
                1990: 7,
                1991: 7,
              },
            },
            ...
          }

      - return value = {
          department1: {
            1990: 17,
            1991: 10,
          },
          ...
        }
  */

  const countByYearByDep = {};
  const departments = ["AUH", "AUT", "AUP"];
  departments.forEach((dep) => {
    if (productionTypeYearCountByDep[dep]) {
      countByYearByDep[dep] = {};
      Object.values(productionTypeYearCountByDep[dep]).forEach(
        (countByYear) => {
          Object.keys(countByYear).forEach((year) => {
            const count = countByYear[year];
            countByYearByDep[dep][year] =
              (countByYearByDep[dep][year] || 0) + count;
          });
        }
      );
    }
  });

  return countByYearByDep;
};

const createFAUCountByYear = (countByYearByProductionTypeByDep) => {
  /*  Example:
      - params
        - countByYearByProductionTypeByDep = {
            department1: {
              productionType1: {
                1990: 10,
                1991: 3,
              },
              productionType2: {
                1990: 7,
                1991: 7,
              },
            },
            department2: {
              productionType1: {
                1990: 4,
                1991: 5,
              },
              productionType2: {
                1990: 6,
                1991: 6,
              },
            },
          }

      - return value = {
          FAU: {
            1990: 27,
            1991: 21,
          },
        }
  */

  const FAUCountByYear = { FAU: {} };
  const departments = ["AUH", "AUT", "AUP"];
  departments.forEach((dep) => {
    if (countByYearByProductionTypeByDep[dep]) {
      Object.values(countByYearByProductionTypeByDep[dep]).forEach(
        (countByYear) => {
          Object.keys(countByYear).forEach((year) => {
            const count = countByYear[year];
            FAUCountByYear.FAU[year] = (FAUCountByYear.FAU[year] || 0) + count;
          });
        }
      );
    }
  });

  return FAUCountByYear;
};

export default setFilters;
export {
  setFilters,
  createCountByCategoryByLabel,
  createCountByYear,
  createCountByYearByDep,
  createFAUCountByYear,
};
