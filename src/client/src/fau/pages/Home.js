import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Carousel } from "react-responsive-carousel";
import {
  getDashboardAggregatedCount,
  getDashboardAggregated,
  getDashboardAggregatedMap,
  getDashboardAggregatedNationalMap,
} from "../../common/API";
import {
  Conteudo,
  DivCard,
  DivGraph,
  DivInfoText,
  DivTitle,
  DivInfoProducao,
  DivTotal,
  barColors,
  departmentColors,
  BarChartWrapper,
  LineChartWrapper,
  NationalMapWrapper,
  WorldMapWrapper,
} from "../styles";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import BarChartContainer from "../../common/charts/BarChartContainer";
import LineChartContainer from "../../common/charts/LineChartContainer";
import NationalMapContainer from "../../common/maps/NationalMapContainer";
import WorldMapContainer from "../../common/maps/WorldMapContainer";
import SpinnerWrapper from "../components/wrappers/SpinnerWrapper";
import { setFilters } from "../helpers";

function Home(props) {
  const { setMenuFilter } = props;
  const [countByProductionType, setCountByProductionType] = useState(null);

  useEffect(() => {
    getDashboardAggregated().then((responsePayload) => {
      setCountByProductionType(responsePayload);
    });

    setFilters(setMenuFilter, { home: true }, (filter, newState) => {
      newState[filter] = false;
    });
  }, []);

  if (!countByProductionType) {
    return (
      <Conteudo
        style={{
          alignItems: "center",
          display: "flex",
          justifyContent: "center",
        }}
      >
        <SpinnerWrapper />
      </Conteudo>
    );
  }

  return <HomeData countByProductionType={countByProductionType} />;
}

function HomeData(props) {
  const { countByProductionType } = props;
  const total = Object.values(countByProductionType).reduce(
    (accumulator, value) => accumulator + value,
    0
  );

  return (
    <Conteudo style={{ justifyContent: "start" }}>
      <DivCard style={{ border: "none" }}>
        <DivInfoProducao>
          <DivTitle>
            <span style={{ fontSize: 32 }}>Total FAUUSP</span>
          </DivTitle>
          <DivTotal>
            <span style={{ fontSize: 100 }}>
              {Intl.NumberFormat().format(total)}
            </span>
            <span>resultados</span>
          </DivTotal>
        </DivInfoProducao>
        <DivGraph>
          <Carousel
            showArrows
            showThumbs={false}
            showIndicators={false}
            showStatus={false}
            autoPlay={false}
            infiniteLoop
            interval={5000}
          >
            <LineChartWrapper>
              <LineChartContainer
                labels={["FAU"]}
                color={{ FAU: departmentColors.FAU }}
                fill={{ FAU: "origin" }}
                fetchData={() =>
                  getDashboardAggregatedCount({ ano_inicio: 1978 })
                }
                Loading={SpinnerWrapper}
              />
            </LineChartWrapper>
            <BarChartWrapper>
              <BarChartContainer
                labels={["FAU"]}
                type="single"
                colors={barColors.FAU}
                fetchData={async () => ({
                  countByYearByCategoryByLabel: { FAU: countByProductionType },
                  categories: [
                    "Produção Artística",
                    "Produção Técnica",
                    "Produção Bibliográfica",
                    "Orientação",
                    "Bancas",
                    "Prêmios e Títulos",
                  ],
                })}
                Loading={SpinnerWrapper}
              />
            </BarChartWrapper>
            <WorldMapWrapper>
              <WorldMapContainer
                fetchData={() => getDashboardAggregatedMap()}
                Loading={SpinnerWrapper}
              />
            </WorldMapWrapper>
            <NationalMapWrapper>
              <NationalMapContainer
                fetchData={() => getDashboardAggregatedNationalMap()}
                Loading={SpinnerWrapper}
              />
            </NationalMapWrapper>
          </Carousel>
        </DivGraph>
        <DivInfoText style={{ padding: "0px 0px 0px 5px" }}>
          <span style={{ fontSize: 13.7 }}>
            A <b>FAU Aberta</b> é uma iniciativa da{" "}
            <b>Comissão de Pesquisa da FAUUSP (CPq-FAUUSP)</b> com o intuito de
            dar visibilidade à produção intelectual da faculdade, difundindo sua
            especificidade e diversidade através de dados extraídos do currículo
            Lattes de docentes. Destinado à comunidade acadêmica em geral, a FAU
            Aberta resulta da cooperação entre a <b>FAU</b> e o{" "}
            <b>Instituto de Matemática e Estatística (IME-USP)</b>, representado
            por um grupo de alunos da disciplina Laboratório de Programação
            Extrema (MAC0342), coordenada pelo Prof. Dr. Alfredo Goldman Vel
            Lejbman, para desenvolver uma base de dados automatizada que
            alimentará a plataforma web de acesso aberto. O projeto envolveu a
            participação de docentes de ambas às unidades, discentes de
            graduação, bibliotecários e a Superintendência da Tecnologia e
            Informação da USP (STI-USP). É compromisso das universidades
            públicas ampliar o acesso às suas pesquisas acadêmicas e aos seus
            resultados (produções bibliográficas, técnicas e artísticas),
            permitindo quantificá-los e qualificá-los em termos de impacto
            social, impacto econômico, inovação tecnológica e desdobramentos em
            políticas públicas e de sustentabilidade. Esta segunda etapa do
            projeto, desenvolvida em 2020 e implementada em 2021, aprimora a
            primeira versão piloto desenvolvida em 2019. Atualmente, esse
            projeto em andamento, disponibiliza apenas a produção intelectual
            dos docentes da FAUUSP cadastrada até novembro de 2020, quando os
            dados foram coletados. Para a próxima etapa de desenvolvimento e
            automatização do sistema, seguiremos contando com o apoio do
            IME-USP, do STI-USP, STI-FAU e da Diretoria da FAUUSP.
          </span>
          <span style={{ marginBottom: 5, marginTop: 0 }}>
            <b>coordenação do projeto CPqs Abertas:</b> Alfredo Goldman vel
            Lejbman
          </span>
          <span style={{ marginBottom: 0 }}>
            <b>pesquisadores colaboradores:</b> Artur Rozestraten, Beatriz
            Bueno, Leandro Velloso, Amarílis Corrêa, Harley Macedo e Deidson
            Rafael Trindade
            <br />
            <b>etapa III, 2021 | desenvolvimento:</b> João Daniel, João Gabriel
            Lembo, Leonardo Pereira, Victor Lima | <b>design:</b> Gustavo
            Machado. <b>coordenação:</b> Luís Felipe Abbud
            <br />
            <b>etapa IV, 2022 | desenvolvimento:</b> João Daniel, Rafael
            Rodrigues, Mohamad Rkein | <b>design:</b> Gustavo Machado.{" "}
            <b>coordenação:</b> Luís Felipe Abbud
          </span>
        </DivInfoText>
      </DivCard>
    </Conteudo>
  );
}

Home.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

HomeData.propTypes = {
  countByProductionType: PropTypes.object.isRequired,
};

export default Home;
