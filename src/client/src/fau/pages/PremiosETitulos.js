import React from "react";
import PropTypes from "prop-types";
import Activity from "../components/activity/Activity";
import { getPremiosCount } from "../../common/API";
import { PremiosDescription } from "../staticTexts";
import { setFilters } from "../helpers";

function PremiosETitulos(props) {
  const { setMenuFilter } = props;

  const updateMenuEntry = () => {
    setFilters(setMenuFilter, { premios: true }, (filter, newState) => {
      newState[filter] = false;
    });
  };

  const chartsToRemove = ["wordCloud", "worldMap", "nationalMap", "barChart"];

  return (
    <Activity
      chartsToRemove={chartsToRemove}
      getCount={getPremiosCount}
      infoText={PremiosDescription}
      type="Prêmios e Títulos"
      updateMenuEntry={updateMenuEntry}
    />
  );
}

PremiosETitulos.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default PremiosETitulos;
