import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useParams } from "react-router-dom";
import { Conteudo } from "../styles";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {
  getCountProdArtPessoa,
  getCountProdArtPessoaPorAno,
  getPessoa,
  getTiposProdArtPessoa,
} from "../../common/API";
import SpinnerWrapper from "../components/wrappers/SpinnerWrapper";
import ProfessorActivityProfile from "../components/profile/ProfessorActivityProfile";
import { setFilters } from "../helpers";

function PerfilProdArt(props) {
  const { menuFilter, setMenuFilter } = props;
  const { id } = useParams();
  const [professorData, setProfessorData] = useState(null);

  useEffect(() => {
    getPessoa({ id }).then((responsePayload) => {
      setProfessorData(responsePayload);

      const department = responsePayload.departamento;

      setFilters(
        setMenuFilter,
        {
          docentes: true,
          perfil: true,
          artistica: true,
          [department]: true,
        },
        (filter, newState) => {
          newState[filter] = false;
        }
      );
    });
  }, []);

  if (!professorData) {
    return (
      <Conteudo
        style={{
          alignSelf: "center",
          textAlign: "center",
        }}
      >
        <SpinnerWrapper />
      </Conteudo>
    );
  }

  if (!professorData.producoes_artisticas) {
    return (
      <Conteudo style={{ justifyContent: "start" }}>
        Não foi possível encontrar essa pessoa
      </Conteudo>
    );
  }

  const detailsToDisplay = [
    { description: "Título:", makeText: (production) => production.titulo },
    {
      description: "DOI:",
      makeText: (production) =>
        production.doi ? (
          <a
            href={`http://dx.doi.org/${production.doi}`}
            target="_blank"
            rel="noreferrer"
          >
            {production.doi}
          </a>
        ) : null,
    },
    {
      description: "Autores:",
      makeText: (production) => production.autores.join(", "),
    },
    { description: "País:", makeText: (production) => production.pais },
  ];

  return (
    <ProfessorActivityProfile
      detailsToDisplay={detailsToDisplay}
      getCountPessoa={getCountProdArtPessoa}
      getCountPessoaPorAno={getCountProdArtPessoaPorAno}
      getTiposPessoa={getTiposProdArtPessoa}
      menuFilter={menuFilter}
      productionType="Produção Artística"
      professorData={professorData}
      title="Produção Artística"
      productionDataKey="producoes_artisticas"
    />
  );
}

PerfilProdArt.propTypes = {
  menuFilter: PropTypes.object.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
};

export default PerfilProdArt;
