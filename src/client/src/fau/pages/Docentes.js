import React, { useEffect } from "react";
import PropTypes from "prop-types";
import Faculty from "../components/faculty/Faculty";
import { setFilters } from "../helpers";

function Docentes(props) {
  const { setMenuFilter, department } = props;

  useEffect(() => {
    const filtersMap = { docentes: true };
    if (department !== "all") {
      filtersMap[department] = true;
    }

    setFilters(setMenuFilter, filtersMap, (filter, newState) => {
      newState[filter] = false;
    });
  }, [department]);

  return <Faculty department={department} />;
}

Docentes.propTypes = {
  department: PropTypes.string.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
};

export default Docentes;
