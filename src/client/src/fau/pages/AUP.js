import React from "react";
import PropTypes from "prop-types";
import Department from "../components/department/Department";

function AUP(props) {
  const { setMenuFilter } = props;

  return (
    <Department
      setMenuFilter={setMenuFilter}
      dep="AUP"
      depDescription="Departamento de Projeto"
      chefe="Prof. Dr. Luís Antônio Jorge"
      viceChefe="Prof. Dr. Fábio Mariz Gonçalves"
    >
      <span style={{ fontSize: 25, fontWeight: "bold" }}>Histórico</span>
      <span>
        O Departamento de Projeto – AUP foi fundado em 1962, sendo originado a
        partir da sequência de disciplinas de Composição, a partir de reunião
        dos colegiados João Baptista Vilanova Artigas, Roberto Cerqueira César,
        Abelardo Riedy de Souza e Hélio de Queiroz Duarte.
      </span>
      <span>
        Tem como objetivo central atuar com os demais departamentos da FAUUSP na
        formação de arquitetos, urbanistas e designers nos cursos de graduação,
        e de professores e pesquisadores na pós-graduação.
      </span>
      <span>
        Propõe-se, neste sentido, desenvolver a capacidade de compreender,
        interpretar criticamente e reelaborar os espaços, objetos e artefatos
        visuais que dão suporte ao processo social do país, de modo a fazer
        avançar não apenas a tecnologia, mas também os modos socioeconômicos de
        organização, na direção da universalização da qualidade de vida para
        todos seus habitantes.
      </span>
      <span>
        No âmbito da graduação, o centro das atividades é voltado para a prática
        do projeto, tanto na escala do planejamento urbano e regional, como da
        arquitetura do edifício, da paisagem e do ambiente, do objeto e da
        programação visual, tendo a atividade de desenvolvimento de trabalhos
        práticos – assim como a reflexão a eles associada – utilizando para tal
        os studios e salas de aula, assim como imersões no campo, nos
        territórios objeto de intervenção/reflexão.
      </span>
      <span>
        Na pós-graduação, desenvolvem-se estudos e pesquisas visando a
        compreensão das práticas projetuais assim como dos processos sociais,
        econômicos políticos e tecnológicos a ela atinentes.
      </span>
    </Department>
  );
}

AUP.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default AUP;
