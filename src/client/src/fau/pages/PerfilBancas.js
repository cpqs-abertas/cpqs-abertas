import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useParams } from "react-router-dom";
import { Conteudo } from "../styles";
import {
  getCountBancaPessoa,
  getCountBancaPessoaPorAno,
  getPessoa,
  getTiposBancaPessoa,
} from "../../common/API";
import SpinnerWrapper from "../components/wrappers/SpinnerWrapper";
import ProfessorActivityProfile from "../components/profile/ProfessorActivityProfile";
import { setFilters } from "../helpers";

function PerfilBancas(props) {
  const { menuFilter, setMenuFilter } = props;
  const { id } = useParams();
  const [professorData, setProfessorData] = useState(null);

  useEffect(() => {
    getPessoa({ id }).then((responsePayload) => {
      setProfessorData(responsePayload);

      const department = responsePayload.departamento;

      setFilters(
        setMenuFilter,
        {
          docentes: true,
          perfil: true,
          bancas: true,
          [department]: true,
        },
        (filter, newState) => {
          newState[filter] = false;
        }
      );
    });
  }, []);

  if (!professorData) {
    return (
      <Conteudo style={{ alignSelf: "center", textAlign: "center" }}>
        <SpinnerWrapper />
      </Conteudo>
    );
  }

  if (!professorData.bancas) {
    return (
      <Conteudo style={{ justifyContent: "start" }}>
        Não foi possível encontrar essa pessoa
      </Conteudo>
    );
  }

  const detailsToDisplay = [
    { description: "Título:", makeText: (production) => production.titulo },
    {
      description: "Instituição:",
      makeText: (production) => production.instituicao,
    },
    { description: "Aluno:", makeText: (production) => production.aluno },
    { description: "Tipo:", makeText: (production) => production.tipo },
    { description: "País:", makeText: (production) => production.pais },
  ];

  return (
    <ProfessorActivityProfile
      detailsToDisplay={detailsToDisplay}
      getCountPessoa={getCountBancaPessoa}
      getCountPessoaPorAno={getCountBancaPessoaPorAno}
      getTiposPessoa={getTiposBancaPessoa}
      menuFilter={menuFilter}
      productionType="Bancas"
      professorData={professorData}
      title="Bancas"
      productionDataKey="bancas"
    />
  );
}

PerfilBancas.propTypes = {
  menuFilter: PropTypes.object.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
};

export default PerfilBancas;
