import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { CenteredColumn, DivLinkDocentes } from "../../styles";
import { getPessoaNames } from "../../../common/API";
import FaderWrapper from "../wrappers/FaderWrapper";

function Faculty(props) {
  const { department } = props;
  const [professors, setProfessors] = useState(null);

  useEffect(() => {
    getPessoaNames().then((responsePayload) => {
      setProfessors(responsePayload);
    });
  }, []);

  if (!professors) {
    return (
      <CenteredColumn>
        <FaderWrapper />
      </CenteredColumn>
    );
  }

  return (
    <CenteredColumn style={{ justifyContent: "start" }}>
      <DivLinkDocentes>
        {professors.length === 0 && "Não há professores disponíveis"}
        {professors.map((professor) => {
          const { nome, departamento, id } = professor;

          const redirect = `/pessoa/perfil/${id}`;
          if (department === "all" || departamento === department) {
            return (
              <span key={id}>
                <Link to={redirect}>{nome}</Link>
              </span>
            );
          }

          return (
            <span key={id} style={{ color: "#bbb", fontSize: "14px" }}>
              {nome}
            </span>
          );
        })}
      </DivLinkDocentes>
    </CenteredColumn>
  );
}

Faculty.propTypes = {
  department: PropTypes.string.isRequired,
};

export default Faculty;
