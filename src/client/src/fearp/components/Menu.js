import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Link, useLocation } from "react-router-dom";

import {
  highlightsFontFamily,
  departmentColors,
  instituteColor,
  supportColor,
} from "../styles";
import { setFilters } from "../helpers";

const setUnselectedStyleForDepartmentItems = (
  departments,
  styles,
  indexToSkip
) => {
  for (let i = 0; i < departments.length; i++) {
    if (i !== indexToSkip) {
      styles[departments[i]] = "unselected";
    }
  }
};

function Menu(props) {
  const { menuFilter, setMenuFilter } = props;
  const { pathname } = useLocation();
  const lattesIdMatch = /\d+/.exec(pathname);
  let lattesId = "";
  if (lattesIdMatch) {
    [lattesId] = lattesIdMatch;
  }

  const styles = {
    RAD: {
      backgroundColor: departmentColors.RAD,
      borderColor: "transparent",
      color: "#FFFFFF",
    },
    RCC: {
      backgroundColor: departmentColors.RCC,
      borderColor: "transparent",
      color: "#FFFFFF",
    },
    REC: {
      backgroundColor: departmentColors.REC,
      borderColor: "transparent",
      color: "#FFFFFF",
    },
    bordered: {
      border: `2.5px solid ${supportColor}`,
    },
    selected: {
      backgroundColor: instituteColor,
      borderColor: "transparent",
      color: "#FFFFFF",
    },
    unselected: {},
  };

  let homeStyle = "unselected";
  let docentesStyle = "unselected";
  const departmentsStyles = {
    RAD: "unselected",
    RCC: "unselected",
    REC: "unselected",
  };

  const productionsStyles = {
    bibliografica: "unselected",
    artistica: "unselected",
    tecnica: "unselected",
    orientacoes: "unselected",
    bancas: "unselected",
    premios: "unselected",
  };

  const departments = Object.keys(departmentsStyles);
  const productionTypes = Object.keys(productionsStyles);

  if (menuFilter.home) {
    homeStyle = "selected";
  } else {
    if (menuFilter.docentes) {
      docentesStyle = "selected";
      departments.forEach((department) => {
        departmentsStyles[department] = "bordered";
      });
    }

    for (let i = 0; i < departments.length; i++) {
      const department = departments[i];
      if (menuFilter[department]) {
        departmentsStyles[department] = department;
        if (menuFilter.perfil) {
          setUnselectedStyleForDepartmentItems(
            departments,
            departmentsStyles,
            i
          );
        }

        break;
      }
    }

    if (menuFilter.perfil) {
      productionTypes.forEach((productionType) => {
        productionsStyles[productionType] = "bordered";
      });
    }

    const selectedProductionType = productionTypes.find(
      (productionType) => menuFilter[productionType]
    );

    if (selectedProductionType) {
      productionsStyles[selectedProductionType] = "selected";
    }
  }

  const departmentInfo = {
    RAD: "Administração - RAD",
    RCC: "Contabilidade - RCC",
    REC: "Economia - REC",
  };

  const productionTypeInfo = {
    bibliografica: {
      text: "Produção Bibliográfica",
      urlName: "producao-bibliografica",
    },
    artistica: { text: "Produção Artística", urlName: "producao-artistica" },
    tecnica: { text: "Produção Técnica", urlName: "producao-tecnica" },
    orientacoes: { text: "Orientações", urlName: "orientacoes" },
    bancas: { text: "Bancas", urlName: "bancas" },
    premios: { text: "Prêmios e Títulos", urlName: "premios-e-titulos" },
  };

  return (
    <Container id="links">
      <MenuLink to="/" style={styles[homeStyle]}>
        FEA-RP Aberta
      </MenuLink>
      <MenuLink to="/pessoa/nomes" style={styles[docentesStyle]}>
        Docentes
      </MenuLink>

      {Object.keys(departmentInfo).map((department) => (
        <MenuLink
          hoverBackgroundColor={departmentColors[department]}
          id={department}
          to={
            menuFilter.docentes
              ? `/pessoa/nomes/${department}`
              : `/${department}`
          }
          style={styles[departmentsStyles[department]]}
        >
          {departmentInfo[department]}
        </MenuLink>
      ))}

      {Object.keys(productionTypeInfo).map((productionType) => {
        const { text, urlName } = productionTypeInfo[productionType];
        return (
          <MenuLink
            to={
              menuFilter.perfil
                ? `/pessoa/${urlName}/${lattesId}`
                : `/${urlName}`
            }
            style={styles[productionsStyles[productionType]]}
          >
            {text}
          </MenuLink>
        );
      })}

      <MenuButton
        type="button"
        onClick={() => {
          setFilters(
            setMenuFilter,
            { faq: true },
            (filter, newState, prevState) => {
              newState[filter] = prevState[filter];
            }
          );
        }}
      >
        FAQ
      </MenuButton>
    </Container>
  );
}

Menu.propTypes = {
  menuFilter: PropTypes.object.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  grid-column-start: 2;
  grid-row-start: 2;
  justify-content: space-between;

  > a,
  > button {
    align-items: center;
    background-color: #ffffff;
    border-radius: 4px;
    border: 1px solid ${supportColor};
    color: ${supportColor};
    cursor: pointer;
    display: flex;
    font-family: ${highlightsFontFamily};
    font-weight: bold;
    font-size: 12px;
    height: 40px;
    justify-content: center;
    padding-top: 1%;
    text-align: center;
    text-decoration: none;
    width: 200px;
  }

  > a:not(:last-child),
  > button:not(:last-child) {
    margin-bottom: 15px;
  }
`;

const MenuLink = styled(Link)`
  :hover {
    background-color: ${(props) => props.hoverBackgroundColor || supportColor};
    border-color: transparent !important;
    color: #ffffff;
  }
`;

const MenuButton = styled.button`
  :hover {
    background-color: ${(props) => props.hoverBackgroundColor || supportColor};
    border-color: transparent !important;
    color: #ffffff;
  }
`;

export default Menu;
