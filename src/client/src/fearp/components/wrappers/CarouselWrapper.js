import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { Carousel } from "react-responsive-carousel";

function CarouselWrapper(props) {
  const { arrowImage, children } = props;

  return (
    <Carousel
      showArrows
      showThumbs={false}
      showIndicators={false}
      showStatus={false}
      autoPlay
      infiniteLoop
      interval={5000}
      renderArrowPrev={(clickHandler, hasPrev, label) => {
        if (!hasPrev) {
          return <></>;
        }

        return (
          <CarouselButton
            style={{ left: 0 }}
            aria-label={label}
            onClick={clickHandler}
          >
            <img
              style={{ transform: "rotate(180deg)" }}
              width="15"
              height="24"
              src={arrowImage}
              alt="slider-left"
            />
          </CarouselButton>
        );
      }}
      renderArrowNext={(clickHandler, hasNext, label) => {
        if (!hasNext) {
          return <></>;
        }

        return (
          <CarouselButton
            style={{ right: 0 }}
            aria-label={label}
            onClick={clickHandler}
          >
            <img width="15" height="24" src={arrowImage} alt="slider-right" />
          </CarouselButton>
        );
      }}
    >
      {children}
    </Carousel>
  );
}

CarouselWrapper.propTypes = {
  arrowImage: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

const CarouselButton = styled.button`
  background: none;
  border: 0;
  bottom: 0;
  color: #000000;
  cursor: pointer;
  font-size: 26px;
  margin: 0;
  padding: 0;
  position: absolute;
  opacity: 1;
  outline: 0;
  top: 0;
  transition: all 0.25s ease-in;
  z-index: 2;
`;

export default CarouselWrapper;
