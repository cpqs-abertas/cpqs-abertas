import React from "react";
import Header from "../../../common/misc/Header";
import { instituteColor } from "../../styles";
import FEARPLogo from "../../assets/images/logo.png";

function HeaderWrapper() {
  return (
    <Header
      title="FEA-RP Aberta |"
      titleColor={instituteColor}
      subtitle="Produção Intelectual da FEA-RP/USP"
      subtitleColor="#000000"
      fontFamily="ITC Avant Garde Gothic Std"
      instituteLogo={FEARPLogo}
      logoWidth={80}
      logoHeight={60}
    />
  );
}

export default HeaderWrapper;
