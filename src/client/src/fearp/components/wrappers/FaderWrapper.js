import React from "react";
import Logo from "../../assets/images/logo.png";
import Fader from "../../../common/misc/Fader";

function FaderWrapper() {
  return <Fader image={Logo} width={120} height={90} />;
}

export default FaderWrapper;
