import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { highlightsFontFamily, supportColor } from "../../styles";
import FAQArrow from "../../assets/images/FAQ-arrow.svg";

function FAQQuestionItem(props) {
  const {
    text,
    answer,
    number,
    questionCount,
    selectedQuestionNumber,
    setSelectedQuestionNumber,
  } = props;

  const handleQuestionItemClick = (event) => {
    let target = event.target;
    while (!target.getAttribute("data-number")) {
      target = target.parentNode;
    }

    const questionNumber = Number(target.getAttribute("data-number"));
    if (questionNumber === selectedQuestionNumber) {
      setSelectedQuestionNumber(-1);
      return;
    }

    setSelectedQuestionNumber(questionNumber);
  };

  return (
    <li
      key={text}
      style={{
        borderBottom: number < questionCount - 1 ? "1px solid black" : "",
        padding: "0.4rem 0",
      }}
    >
      <ItemButton
        data-number={number}
        type="button"
        onClick={handleQuestionItemClick}
      >
        <span style={{ color: supportColor }}>{text}</span>
        {selectedQuestionNumber === number && (
          <img
            style={{ marginLeft: "3px", transform: "rotate(90deg)" }}
            width="12"
            height="12"
            src={FAQArrow}
            alt="selected"
          />
        )}
        {selectedQuestionNumber !== number && (
          <img
            style={{ marginLeft: "3px" }}
            width="12"
            height="12"
            src={FAQArrow}
            alt="unselected"
          />
        )}
      </ItemButton>
      <div
        style={{
          display: selectedQuestionNumber === number ? "block" : "none",
          marginTop: "0.25rem",
        }}
      >
        {answer}
      </div>
    </li>
  );
}

FAQQuestionItem.propTypes = {
  answer: PropTypes.string.isRequired,
  number: PropTypes.number.isRequired,
  questionCount: PropTypes.number.isRequired,
  selectedQuestionNumber: PropTypes.number.isRequired,
  setSelectedQuestionNumber: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
};

const ItemButton = styled.button`
  background: transparent;
  border: 0;
  cursor: pointer;
  font-size: 15px;
  font-family: ${highlightsFontFamily} !important;
  font-weight: bold;
  padding: 0;
  text-align: left;
`;

export default FAQQuestionItem;
