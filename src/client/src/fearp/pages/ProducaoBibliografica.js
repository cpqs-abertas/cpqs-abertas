import React from "react";
import PropTypes from "prop-types";
import Activity from "../components/activity/Activity";
import {
  getProdBibCount,
  getProdBibCountByType,
  getProdBibKeywords,
  getProdBibMap,
  getProdBibNationalMap,
} from "../../common/API";
import { ProdBibDescription } from "../staticTexts";
import { setFilters } from "../helpers";

function ProducaoBibliografica(props) {
  const { setMenuFilter } = props;

  const updateMenuEntry = () => {
    setFilters(setMenuFilter, { bibliografica: true }, (filter, newState) => {
      newState[filter] = false;
    });
  };

  return (
    <Activity
      getCount={getProdBibCount}
      getCountByType={getProdBibCountByType}
      getKeywords={getProdBibKeywords}
      getMap={getProdBibMap}
      getNationalMap={getProdBibNationalMap}
      infoText={ProdBibDescription}
      type="Produção Bibliográfica"
      updateMenuEntry={updateMenuEntry}
    />
  );
}

ProducaoBibliografica.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default ProducaoBibliografica;
