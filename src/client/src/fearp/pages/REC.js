import React from "react";
import PropTypes from "prop-types";
import Department from "../components/department/Department";

function REC(props) {
  const { setMenuFilter } = props;

  return (
    <Department
      setMenuFilter={setMenuFilter}
      dep="REC"
      depDescription="Departamento de Economia"
      chefe="Prof. Dr. Milton Barossi Filho"
      viceChefe="Prof. Dr. Renato Leite Marcondes"
    >
      <span>
        O Departamento de Economia da Faculdade de Economia, Administração e
        Contabilidade de Ribeirão Preto (CREC-FEA-RP/USP) tem como missão a
        geração e a difusão de conhecimento em Economia e áreas afins, bem como
        a contribuição para a superação de problemas da sociedade (local,
        regional e nacional), tendo como base o conhecimento especializado de
        seus membros. No cumprimento de sua missão institucional, o Departamento
        de Economia visa a excelência na execução de suas atividades de ensino,
        pesquisa e extensão.
      </span>
      <span>
        O Departamento de Economia é parte constituinte da FEA-RP desde o início
        da faculdade, em 1992. Criado com 19 docentes, chegou a contar com 30
        docentes no período de 2013 a 2015, estando atualmente com 28
        professores. É responsável pelo curso de graduação em Ciências
        Econômicas, desde a criação da FEA-RP, e corresponsável, juntamente com
        o Departamento de Contabilidade, pelo curso de Finanças e Negócios, que,
        a partir de 2022, substitui o curso de Economia Empresarial e
        Controladoria (ECEC), em atividade desde 2006.
      </span>
      <span>
        O Departamento de Economia também tem participação nos cursos de
        graduação de Matemática Aplicada a Negócios, curso esse inicialmente
        criado conjuntamente pelos três Departamentos da FEA-RP e pelo
        Departamento de Computação e Matemática da FFCLRP. Esse curso passou a
        ser gerido exclusivamente pelo Departamento de Computação e Matemática
        em 2013, sendo o Departamento de Economia responsável por 32% das
        disciplinas ministradas neste curso. Os docentes do Departamento de
        Economia também ministram disciplinas nos cursos Administração Diurno e
        Noturno, Ciências Contábeis, Direito, Biblioteconomia e Ciência da
        Informação.
      </span>
      <span>
        Na Pós-Graduação os docentes do Departamento têm a gestão do Programa de
        Pós-Graduação em Economia – Área: Economia Aplicada da FEA-RP. O
        Departamento preza pelo aprofundamento das relações com outros
        Departamentos e setores da C&T do país, inclusive da própria USP, pois
        entende esta integração e interdisciplinaridade como favorável ao
        progresso do próprio Departamento. Neste sentido apoia e incentiva
        também participações e gestão em programas de Pós-Graduação externos ao
        Departamento e à Unidade, como o de Integração da América Latina do
        PROLAM-USP, além de participações em outros Programas da USP.
      </span>
    </Department>
  );
}

REC.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default REC;
