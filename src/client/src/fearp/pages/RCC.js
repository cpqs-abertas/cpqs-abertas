import React from "react";
import PropTypes from "prop-types";
import Department from "../components/department/Department";

function RCC(props) {
  const { setMenuFilter } = props;

  return (
    <Department
      setMenuFilter={setMenuFilter}
      dep="RCC"
      depDescription="Departamento de Contabilidade"
      chefe="Prof. Dr. Carlos Alberto Grespan Bonacim"
      viceChefe="Prof. Dr. Roni Cleber Bonizio"
    >
      <span>
        O Departamento de Contabilidade da FEA-RP/USP (RCC) tem como objetivo
        desenvolver pesquisas de alto nível e disseminar novos conhecimentos e
        tecnologias de ponta, necessários para que os alunos dos cursos de
        graduação em Ciências Contábeis (CC) e Economia Empresarial e
        Controladoria (ECEC), de pós-graduação (Mestrado e Doutorado) e de
        extensão (MBAs) estejam sempre inseridos em um ambiente onde são
        estudados e discutidos criticamente o que há de mais inovador nessa
        grande área de conhecimento que temos expertise: a Contabilidade.
      </span>
      <span>
        O RCC é gerido por um Conselho (CRCC) e pelo Chefe do Departamento, que
        contam com a assessoria das Comissões de Coordenação (CoC) do curso de
        Ciências Contábeis e do curso de Economia Empresarial e Controladoria e
        da Comissão Coordenadora do Programa de Pós-Graduação em Controladoria e
        Contabilidade (CCP). Esses colegiados são compostos por docentes, alunos
        de graduação e pós-graduação, que discutem e estabelecem as principais
        diretrizes acadêmicas e estratégicas que norteiam as atividades do
        Departamento, sempre tendo como objetivo principal o desenvolvimento e
        difusão de conhecimentos relevantes e úteis de Contabilidade.
      </span>
      <span>
        À Chefia do Departamento cabe convocar e presidir as reuniões do
        Conselho, representar o Departamento de Contabilidade na Congregação e
        no Conselho Técnico Administrativo (CTA) da Unidade, além de orientar as
        atividades do pessoal docente, técnico e administrativo do Departamento.
      </span>
      <span>
        MISSÃO DO DEPARTAMENTO: Desenvolver o conhecimento de Contabilidade e
        Controladoria e formar profissionais e pesquisadores capazes de
        contribuir de forma efetiva para o contínuo aperfeiçoamento da
        sociedade.
      </span>
      <span>
        VISÃO DO DEPARTAMENTO: Ser reconhecido como centro de excelência em
        ensino, pesquisa e extensão universitária, nas áreas de Contabilidade e
        Controladoria.
      </span>
    </Department>
  );
}

RCC.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default RCC;
