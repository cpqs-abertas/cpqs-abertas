import React from "react";
import PropTypes from "prop-types";
import Department from "../components/department/Department";

function RAD(props) {
  const { setMenuFilter } = props;

  return (
    <Department
      setMenuFilter={setMenuFilter}
      dep="RAD"
      depDescription="Departamento de Administração"
      chefe="Prof. Dr. João Luiz Passador"
      viceChefe="Profa. Dra. Silvia Inês Dallavalle de Pádua"
    >
      <span>
        O curso de administração foi criado como sendo noturno, de duração de 5
        anos, juntamente com os demais cursos da unidade. Posteriormente, foi
        criado o curso diurno, com duração de 4 anos, buscando otimizar as
        estruturas da unidade. Posteriormente, os dois cursos foram alterados e
        tornaram-se idênticos, no que se refere à estrutura curricular e
        duração.
      </span>
      <span>
        A FEARP distingue-se pela proximidade aos clusters de negócios em saúde
        e ao agronegócio,inclusive sendo uma das motivações para a abertura da
        unidade. Tal proximidade fomenta pesquisas em cooperativismo,
        tecnologias em negócios agroindustriais,inovação em gestão da cadeia
        agropecuária, processos hospitalares e relacionados aos negócios de
        saúde. Há envolvimento de professores em inúmeras organizações públicas
        e privadas deste campo do conhecimento, inclusive contando com uma
        incubadora relacionada ao desenvolvimento e inovação em tecnologias em
        saúde (Supera Parque de Inovação e Tecnologia, no campus da USP em
        Ribeirão Preto). Também se inter-relaciona, em ações conjuntas,
        assessorias, ou cursos compartilhados, com o Hospital das Clínicas da
        Faculdade de Medicina de Ribeirão Preto - USP (HC-FMRP/USP), o
        Hemocentro, além das faculdades de Enfermagem, Direito e dos cursos de
        Psicologia e Matemática Aplicada a Negócios (MAN).
      </span>
      <span>
        O Departamento de Administração conta com 35 docentes em regime RDIDP,
        com 326 alunos no curso diurno e 251 no noturno, além de 98 alunos na
        Pós-Graduação stricto sensu. A Unidade conta com o apoio da Fundação
        para Pesquisa e Desenvolvimento da Administração, Contabilidade e
        Economia (FUNDACE) e os docentes do departamento mantém mais de 500
        alunos em seus cursos de pós-graduação latu sensu.
      </span>
      <span>
        Os alunos possuem uma vasta gama de instituições para atuarem nas mais
        diversas atividades,destacando-se as entidades AIESEC, Centro Acadêmico,
        Clube de Mercado Financeiro, Enactus, iTeam, Júnior FEA-RP e Núcleo de
        Empreendedores.
      </span>
      <span>
        Destaca-se ainda, a forte presença do departamento nos processos de
        internacionalização dos estudantes, sendo que aproximadamente um terço
        dos alunos realizam algum tipo de intercâmbio.
      </span>
    </Department>
  );
}

RAD.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default RAD;
