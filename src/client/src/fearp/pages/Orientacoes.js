import React from "react";
import PropTypes from "prop-types";
import Activity from "../components/activity/Activity";
import {
  getOrientacaoCount,
  getOrientacaoCountByType,
  getOrientacaoKeywords,
  getOrientacaoMap,
} from "../../common/API";
import { OrientacaoDescription } from "../staticTexts";
import { setFilters } from "../helpers";

function Orientacoes(props) {
  const { setMenuFilter } = props;

  const updateMenuEntry = () => {
    setFilters(setMenuFilter, { orientacoes: true }, (filter, newState) => {
      newState[filter] = false;
    });
  };

  const chartsToRemove = ["nationalMap"];

  return (
    <Activity
      chartsToRemove={chartsToRemove}
      getCount={getOrientacaoCount}
      getCountByType={getOrientacaoCountByType}
      getKeywords={getOrientacaoKeywords}
      getMap={getOrientacaoMap}
      infoText={OrientacaoDescription}
      type="Orientação"
      updateMenuEntry={updateMenuEntry}
    />
  );
}

Orientacoes.propTypes = {
  setMenuFilter: PropTypes.func.isRequired,
};

export default Orientacoes;
