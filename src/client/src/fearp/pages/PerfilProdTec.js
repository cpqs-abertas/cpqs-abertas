import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useParams } from "react-router-dom";
import { Conteudo } from "../styles";
import {
  getCountProdTecPessoa,
  getCountProdTecPessoaPorAno,
  getPessoa,
  getTiposProdTecPessoa,
} from "../../common/API";
import FaderWrapper from "../components/wrappers/FaderWrapper";
import ProfessorActivityProfile from "../components/profile/ProfessorActivityProfile";
import { setFilters } from "../helpers";

function PerfilProdTec(props) {
  const { menuFilter, setMenuFilter } = props;
  const { id } = useParams();
  const [professorData, setProfessorData] = useState(null);

  useEffect(() => {
    getPessoa({ id }).then((responsePayload) => {
      setProfessorData(responsePayload);

      const department = responsePayload.departamento;

      setFilters(
        setMenuFilter,
        {
          docentes: true,
          perfil: true,
          tecnica: true,
          [department]: true,
        },
        (filter, newState) => {
          newState[filter] = false;
        }
      );
    });
  }, []);

  if (!professorData) {
    return (
      <Conteudo
        style={{
          alignSelf: "center",
          textAlign: "center",
        }}
      >
        <FaderWrapper />
      </Conteudo>
    );
  }

  if (!professorData.producoes_tecnicas) {
    return (
      <Conteudo style={{ justifyContent: "start" }}>
        Não foi possível encontrar essa pessoa
      </Conteudo>
    );
  }

  const detailsToDisplay = [
    { description: "Título:", makeText: (production) => production.titulo },
    { description: "Natureza:", makeText: (production) => production.natureza },
    { description: "Tipo:", makeText: (production) => production.tipo },
    { description: "Idioma:", makeText: (production) => production.idioma },
    {
      description: "Autores:",
      makeText: (production) => production.autores.join(", "),
    },
    { description: "País:", makeText: (production) => production.pais },
  ];

  return (
    <ProfessorActivityProfile
      detailsToDisplay={detailsToDisplay}
      getCountPessoa={getCountProdTecPessoa}
      getCountPessoaPorAno={getCountProdTecPessoaPorAno}
      getTiposPessoa={getTiposProdTecPessoa}
      menuFilter={menuFilter}
      productionType="Produção Técnica"
      professorData={professorData}
      title="Produção Técnica"
      productionDataKey="producoes_tecnicas"
    />
  );
}

PerfilProdTec.propTypes = {
  menuFilter: PropTypes.object.isRequired,
  setMenuFilter: PropTypes.func.isRequired,
};

export default PerfilProdTec;
