## Cliente do projeto


### Instalar dependências

Certifique-se que você possui o gerenciador de pacotes **npm**. Assim, para instalar as dependências do front-end, basta executar o comando:

`npm install`

### Formatar arquivos do projeto usando codestyle padronizado

Para formatar o projeto por completo, basta executar o comando:

`npm format`

### Executar projeto

Uma vez as dependências instaladas, execute o seguinte comando para iniciar uma versão de desenvolvimento:

`npm start`

O projeto rodará em **localhost:3000** e toda mudança feita no código refletirá instantaneamente em seu navegador (hot reload).



