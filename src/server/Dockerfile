FROM python:3.9.5 as base

ENV PYTHONUNBUFFERED 1

ARG USER_ID=1000 \
    GROUP_ID=1000

WORKDIR /server

RUN groupadd -g "$GROUP_ID" -f server && \
    useradd -g "$GROUP_ID" -u "$USER_ID" -m server && \
    chown "${USER_ID}:${GROUP_ID}" /server

USER "${USER_ID}:${GROUP_ID}"

EXPOSE 8000

################################################################################

FROM base as development

COPY --chown=server:server ./requirements.txt ./
RUN pip install -r requirements.txt
RUN python -m nltk.downloader stopwords
COPY --chown=server:server ./ ./

ENV PATH="${PATH}:/home/server/.local/bin"

CMD ["./wait-for.sh", "database:5432", "-t", "60", "--", \
     "python", "manage.py", "runserver", "0.0.0.0:8000"]

################################################################################

FROM development as production

ENV DJANGO_DEBUG=False

CMD ["./wait-for.sh", "database:5432", "-t", "60", "--", \
     "gunicorn", "cpqsabertas.wsgi", "-b", "0.0.0.0:8000"]
