from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /contato/faq
    url(r'^faq$', views.faq, name='contato-faq'),
]
