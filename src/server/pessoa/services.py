from models.models import Pessoa
import util.fau.route_operations as route_operations
import util.populate.code.parsing.db.connection as db_connection

aggregates = db_connection.get_mongo_db().pessoa

def get_names():
    query_set = Pessoa.objects.all()
    if not query_set:
        return route_operations.error('Não há pessoas registradas')
    result = []
    for pessoa in query_set.order_by('nome_completo'):
        idLattes = str(pessoa.id_lattes)
        while len(idLattes) < 16:
            idLattes = str(0) + idLattes

        result.append({
            "id": idLattes,
            "nome": pessoa.nome_completo,
            "departamento": pessoa.departamento
        })

    return result

def get_aggregated_names():
    return aggregates.find_one({"operation": "names"})["result"]