from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /pessoa/
    url(r'^$', views.index, name='pessoa-index'),

    # ex: /pessoa/names
    url(r'^names', views.names, name='pessoa-names'),
    # ex: /pessoa/aggregated-names
    url(r'^aggregated-names', views.aggregated_names, name='pessoa-aggregated_names'),

    # ex: /pessoa/countPorDep
    url(r'^countPorDep$', views.countByDep, name='pessoa-countByDep'),
    # ex: /pessoa/keywords
    url(r'^keywords$', views.keywords, name='pessoa-keywords'),
    # ex: /pessoa/map
    url(r'^map$', views.map, name='pessoa-map'),
    # ex: /pessoa/map-national
    url(r'^map-national$', views.map_national, name='pessoa-map-national'),
    # ex: /pessoa/tiposProdArt
    url(r'^tiposProdArt$', views.tiposProdArt, name='pessoa-tiposProdArt'),
    # ex: /pessoa/count/prodArt
    url(r'^count/prodArt$', views.countTiposProdArt, name='pessoa-countTiposProdArt'),
    # ex: /pessoa/countPorAno/prodArt
    url(r'^countPorAno/prodArt$', views.countProdArtByYear, name='pessoa-countProdArtByYear'),
    # ex: /pessoa/tiposProdTec
    url(r'^tiposProdTec$', views.tiposProdTec, name='pessoa-tiposProdTec'),
    # ex: /pessoa/count/prodTec
    url(r'^count/prodTec$', views.countTiposProdTec, name='pessoa-countTiposProdTec'),
    # ex: /pessoa/countPorAno/prodTec
    url(r'^countPorAno/prodTec$', views.countProdTecByYear, name='pessoa-countProdTecByYear'),
    # ex: /pessoa/tiposProdBib
    url(r'^tiposProdBib$', views.tiposProdBib, name='pessoa-tiposProdBib'),
    # ex: /pessoa/count/prodBib
    url(r'^count/prodBib$', views.countTiposProdBib, name='pessoa-countTiposProdBib'),
    # ex: /pessoa/countPorAno/prodArt
    url(r'^countPorAno/prodBib$', views.countProdBibByYear, name='pessoa-countProdBibByYear'),
    # ex: /pessoa/tiposPart
    url(r'^tiposPart$', views.tiposPart, name='pessoa-tiposPart'),
    # ex: /pessoa/count/part
    url(r'^count/part$', views.countTiposPart, name='pessoa-countTiposPart'),
    # ex: /pessoa/countPorAno/part
    url(r'^countPorAno/part$', views.countPartByYear, name='pessoa-countPartByYear'),
    # ex: /pessoa/tiposBanca
    url(r'^tiposBanca$', views.tiposBanca, name='pessoa-tiposBanca'),
    # ex: /pessoa/count/banca
    url(r'^count/banca$', views.countTiposBanca, name='pessoa-countTiposBanca'),
    # ex: /pessoa/countPorAno/banca
    url(r'^countPorAno/banca$', views.countBancaByYear, name='pessoa-countBancaByYear'),
    # ex: /pessoa/tiposOri
    url(r'^tiposOri$', views.tiposOri, name='pessoa-tiposOri'),
    # ex: /pessoa/count/ori
    url(r'^count/ori$', views.countTiposOri, name='pessoa-countTiposOri'),
    # ex: /pessoa/countPorAno/ori
    url(r'^countPorAno/ori$', views.countOriByYear, name='pessoa-countOriByYear'),
    # ex: /pessoa/countPorAno/premios
    url(r'^countPorAno/premio$', views.countPremioByYear, name='pessoa-countPremioByYear'),
    # ex: /pessoa/countAll
    url(r'^countAll$', views.countAll, name='pessoa-countAll'),
    # ex: /pessoa/picture
    url(r'^picture$', views.getPictureId, name='pessoa-picture')
]
