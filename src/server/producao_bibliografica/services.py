from models.models import Pessoa, ProducaoBibliografica
import util.fau.route_operations as route_operations
import util.populate.code.parsing.db.connection as db_connection

aggregates = db_connection.get_mongo_db().producao_bibliografica

def get_count(params):
    return route_operations.count(ProducaoBibliografica, params)


def get_keywords(params):
    return route_operations.keywords(ProducaoBibliografica, params)


def get_map(params):
    return route_operations.map(ProducaoBibliografica, params)


def get_national_map(params):
    return route_operations.national_map(ProducaoBibliografica, params)


def get_types_count(params):
    return route_operations.types_count(ProducaoBibliografica, params)


def get_types():
    types = ProducaoBibliografica.objects \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    result = []
    accepted = ['Livro', 'Capitulo', 'Artigo', 'Evento', 'Jornal', 'Prefacio_Posfacio']
    for t in types:
        if t['tipo'] in accepted:
            result.append(t['tipo'])
    return result


def get_aggregated_count():
    return aggregates.find_one({ "operation": "count" })["result"]


def get_aggregated_count_by_type():
    return aggregates.find_one({ "operation": "count_by_type" })["result"]


def get_aggregated_keywords():
    return aggregates.find_one({ "operation": "keywords" })["result"]


def get_aggregated_map():
    return aggregates.find_one({ "operation": "map" })["result"]


def get_aggregated_national_map():
    return aggregates.find_one({ "operation": "national_map" })["result"]


def get_aggregated_types_count():
    return aggregates.find_one({ "operation": "types_count" })["result"]
