import json
from django.test import TestCase, Client
from django.urls import reverse

from models.models import ProducaoBibliografica, RelPessoaProdBib, Pessoa


class ViewProducaoBibliograficaTests(TestCase):
    """ Test module for producao_bibliografica views """
    # fixture file
    fixtures = [
        'util/fixtures/docentes.json',
        'producao_bibliografica/fixtures/tests.json'
    ]

    # initialize the APIClient app
    client = Client()

    def test_count_returns_all_departments(self):
        response = self.client.get(reverse('producao_bibliografica-count'))
        prod_list = json.loads(response.content)

        expected_departments = [dep['departamento']
                                for dep in Pessoa.objects
                                .values('departamento').distinct()]

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(prod_list), len(expected_departments))

        for dep in prod_list:
            assert dep in expected_departments

    def test_count_returns_all_types(self):
        response = self.client.get(reverse('producao_bibliografica-count'))
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        expected_types = [typ['tipo']
                          for typ in ProducaoBibliografica.objects
                          .values('tipo').distinct()]

        for dep in prod_list:
            self.assertEqual(len(prod_list[dep]), len(expected_types))
            for type in prod_list[dep]:
                assert type in expected_types

                # 10 keys: one for each year
                self.assertEqual(True, len(prod_list[dep][type]) > 10)

    def test_count_with_specific_department(self):
        response = self.client.get(
            reverse('producao_bibliografica-count'),
            {'departamentos[]': 'AUH'})
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(prod_list), 1)
        self.assertEqual(list(prod_list.keys()), ['AUH'])

    def test_count_with_specific_type(self):
        types = ['tipo1', 'teste']
        response = self.client.get(
            reverse('producao_bibliografica-count'), {'tipos[]': types})
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        for dep in prod_list:
            type_list = list(prod_list[dep].keys())
            self.assertEqual(len(type_list), len(types))
            self.assertEqual(sorted(type_list), sorted(types))

    def test_count_returns_correctly_for_each_year(self):
        response = self.client.get(reverse('producao_bibliografica-count'))
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(prod_list['AUH']['tipo1']['2011'], 1)
        self.assertEqual(prod_list['AUT']['tipo2']['2015'], 1)
        self.assertEqual(prod_list['AUH']['tipo1']['2015'], 1)
        self.assertEqual(prod_list['AUT']['tipo2']['2019'], 1)
        self.assertEqual(prod_list['AUH']['tipo2']['2019'], 1)
        self.assertEqual(prod_list['AUP']['tipo2']['2019'], 1)

    def test_count_returns_correctly_for_specific_year_range(self):
        response = self.client.get(
            reverse('producao_bibliografica-count'),
            {'ano_inicio': 2017, 'ano_fim': 2019})
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        for dep in prod_list:
            for type in prod_list[dep]:
                self.assertEqual(len(prod_list[dep][type]), 3)

    def test_count_returns_400_with_invalid_range(self):
        response = self.client.get(
            reverse('producao_bibliografica-count'),
            {'ano_inicio': -1, 'ano_fim': 2019})

        self.assertEqual(response.status_code, 400)

    def test_count_returns_400_with_invalid_year(self):
        response = self.client.get(
            reverse('producao_bibliografica-count'),
            {'ano_inicio': 'batata', 'ano_fim': 0})

        self.assertEqual(response.status_code, 400)

    def test_count_with_empty_database(self):
        RelPessoaProdBib.objects.all().delete()
        ProducaoBibliografica.objects.all().delete()

        response = self.client.get(reverse('producao_bibliografica-count'))
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        for dep in prod_list:
            for type in prod_list[dep]:
                for year in prod_list[dep][type]:
                    self.assertEqual(prod_list[dep][type], 0)

    def test_keywords_with_default_limit(self):
        response = self.client.get(reverse('producao_bibliografica-keywords'))
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        expected_result = [
            {"titulo": 6},
            {"teste": 3},
            {"foo": 2},
            {"bar": 1}
        ]
        self.assertEqual(json_response['keywords'], expected_result)

    def test_keywords_with_specific_limit(self):
        response = self.client.get(
            reverse('producao_bibliografica-keywords'), {"limit": 2})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        expected_result = [
            {"titulo": 6},
            {"teste": 3}
        ]
        self.assertEqual(json_response['keywords'], expected_result)

    def test_keywords_with_empty_database(self):
        RelPessoaProdBib.objects.all().delete()
        ProducaoBibliografica.objects.all().delete()

        response = self.client.get(reverse('producao_bibliografica-keywords'))
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(json_response['keywords'], [])

    def test_keywords_with_negative_limit_returns_400(self):
        response = self.client.get(
            reverse('producao_bibliografica-keywords'), {"limit": -1})

        self.assertEqual(response.status_code, 400)

    def test_keywords_with_invalid_limit_returns_400(self):
        response = self.client.get(
            reverse('producao_bibliografica-keywords'), {"limit": "foo"})

        self.assertEqual(response.status_code, 400)

    def test_map_with_default_limit(self):
        response = self.client.get(reverse('producao_bibliografica-map'))
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        expected_result = [
            {'Brasil': {
                'lat': '-10.3333333', 'long': '-53.2', 'total': 3
            }},
            {'Espanha': {
                'lat': '39.3260685', 'long': '-4.8379791', 'total': 3
            }}
        ]
        self.assertCountEqual(json_response['countries'], expected_result)

    def test_map_with_specific_limit(self):
        response = self.client.get(
            reverse('producao_bibliografica-map'), {"limit": 2})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        expected_result = [
            {'Brasil': {
                'lat': '-10.3333333', 'long': '-53.2', 'total': 3
            }},
            {'Espanha': {
                'lat': '39.3260685', 'long': '-4.8379791', 'total': 3
            }}
        ]
        self.assertCountEqual(json_response['countries'], expected_result)

    def test_map_with_empty_database(self):
        RelPessoaProdBib.objects.all().delete()
        ProducaoBibliografica.objects.all().delete()

        response = self.client.get(reverse('producao_bibliografica-map'))
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(json_response['countries'], [])

    def test_map_with_negative_limit_returns_400(self):
        response = self.client.get(
            reverse('producao_bibliografica-map'), {"limit": -1})

        self.assertEqual(response.status_code, 400)

    def test_map_with_invalid_limit_returns_400(self):
        response = self.client.get(
            reverse('producao_bibliografica-map'), {"limit": "foo"})

        self.assertEqual(response.status_code, 400)

    def test_types_count_returns_all_departments(self):
        response = self.client.get(
            reverse('producao_bibliografica-types_count'))
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        expected_departments = [dep['departamento']
                                for dep in Pessoa.objects
                                .values('departamento').distinct()]
        returned_departments = list(prod_list.keys())

        self.assertEqual(len(returned_departments), len(expected_departments))
        self.assertEqual(sorted(returned_departments),
                         sorted(expected_departments))

    def test_types_count_returns_all_types(self):
        response = self.client.get(
            reverse('producao_bibliografica-types_count'))
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        expected_types = [typ['tipo']
                          for typ in ProducaoBibliografica.objects
                          .values('tipo').distinct()]

        for dep in prod_list:
            self.assertEqual(len(prod_list[dep]), len(expected_types))
            returned_types = []
            for value in prod_list[dep]:
                returned_types.extend(list(value.keys()))
            self.assertEqual(sorted(returned_types), sorted(expected_types))

    def test_types_count_with_specific_department(self):
        response = self.client.get(
            reverse('producao_bibliografica-types_count'),
            {'departamentos[]': 'AUH'})
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(prod_list), 1)
        self.assertEqual(list(prod_list.keys()), ['AUH'])

    def test_types_count_with_specific_type(self):
        types = ['tipo1', 'teste']
        response = self.client.get(
            reverse('producao_bibliografica-types_count'), {'tipos[]': types})
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        for dep in prod_list:
            returned_types = []
            for value in prod_list[dep]:
                returned_types.extend(list(value.keys()))
            self.assertEqual(len(returned_types), len(types))
            self.assertEqual(sorted(returned_types), sorted(types))

    def test_types_count_returns_correctly(self):
        response = self.client.get(
            reverse('producao_bibliografica-types_count'), {
                'departamentos[]': ['AUH'],
                'tipos[]': ['tipo1']
            })
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(prod_list['AUH']), 1)
        self.assertEqual(prod_list['AUH'][0]['tipo1'], 4)