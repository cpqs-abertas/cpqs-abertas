from django.apps import AppConfig


class ProducoesConfig(AppConfig):
    name = 'producoes'
