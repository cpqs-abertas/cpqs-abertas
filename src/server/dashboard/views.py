from django.db.models import Count
from django.http import JsonResponse
from models.models import ProducaoArtistica, ProducaoBibliografica, \
    ProducaoTecnica, Orientacao, Bancas, PremiosTitulos, Pessoa, ParserInfo
import datetime
import util.fau.route_operations as route_operations
from . import services

now = datetime.datetime.now()
actual_year = str(now.year)


def index(request):
    """Index for producoes route.

    Parameters: a GET request no parameters.

    Returns:
    JsonResponse with status code and the count of productions for each table.
   """

    return JsonResponse(services.get_dashboard())


def indexByDep(request):
    """Index by department for dashboard route.

    Parameters: a GET request with departamento parameter

    Returns:
    JsonResponse with status code and the count of productions for
    each table for the department.
   """
    department = request.GET.get('departamento', '')
    if not department:
        return route_operations.error('Departamento inválido')

    author_from_dep = Pessoa.objects \
        .filter(departamento__contains=department) \
        .values('id_lattes')

    dict = {
        'Produção Artística': [
            ProducaoArtistica.objects
            .filter(autores__in=author_from_dep)
            .distinct(), 'id'
        ],
        'Produção Técnica': [
            ProducaoTecnica.objects
            .filter(autores__in=author_from_dep)
            .distinct(), 'id'
        ],
        'Produção Bibliográfica': [
            ProducaoBibliografica.objects
            .filter(autores__in=author_from_dep)
            .distinct(), 'id'
        ],
        'Orientação': [
            Orientacao.objects
            .filter(pessoa__in=author_from_dep)
            .distinct(), 'id'
        ],
        'Bancas': [
            Bancas.objects
            .filter(participantes__in=author_from_dep)
            .distinct(), 'id'
        ],
        'Prêmios e Títulos': [
            PremiosTitulos.objects
            .filter(pessoa__in=author_from_dep)
            .distinct(), 'id'
        ]
    }

    result = {}
    for key, value in dict.items():
        result[key] = value[0].aggregate(Count(value[1]))[value[1]+'__count']

    return JsonResponse(result)


def search(request):
    """Search for dashboard route.

    Parameters: a GET request with 'key' parameter.

    Returns:
    JsonResponse with all registries that contains the given key
    in its title.
    """

    key = request.GET.get('key', '')

    if not key:
        return route_operations.error("Palavra chave para busca não informada")

    result = {
        "prod_art": [],
        "prod_bib": [],
        "prod_tec": [],
        "bancas": [],
        "orientacao": [],
        "premiosTitulos": []
    }

    prod_art = ProducaoArtistica.objects \
        .filter(titulo__icontains=key) \
        .order_by('-ano') \
        .distinct()
    for art in prod_art.all():
        obj = {
            "titulo": art.titulo,
            "tipo": art.tipo,
            "ano": art.ano,
            "autores": []
        }
        for autor in art.autores.all():
            idLattes = str(autor.id_lattes)
            while len(idLattes) < 16:
                idLattes = str(0) + idLattes

            obj["autores"].append({
                "nome": autor.nome_completo,
                "id_lattes": idLattes,
                "departamento": autor.departamento
            })
        result["prod_art"].append(obj)

    prod_bib = ProducaoBibliografica.objects \
        .filter(titulo__icontains=key) \
        .order_by('-ano') \
        .distinct()
    for bib in prod_bib.all():
        obj = {
            "titulo": bib.titulo,
            "tipo": bib.tipo,
            "ano": bib.ano,
            "autores": []
        }
        for autor in bib.autores.all():
            idLattes = str(autor.id_lattes)
            while len(idLattes) < 16:
                idLattes = str(0) + idLattes

            obj["autores"].append({
                "nome": autor.nome_completo,
                "id_lattes": idLattes,
                "departamento": autor.departamento
            })
        result["prod_bib"].append(obj)

    prod_tec = ProducaoTecnica.objects \
        .filter(titulo__icontains=key) \
        .order_by('-ano') \
        .distinct()
    for tec in prod_tec.all():
        obj = {
            "titulo": tec.titulo,
            "tipo": tec.tipo,
            "ano": tec.ano,
            "autores": []
        }
        for autor in tec.autores.all():
            idLattes = str(autor.id_lattes)
            while len(idLattes) < 16:
                idLattes = str(0) + idLattes

            obj["autores"].append({
                "nome": autor.nome_completo,
                "id_lattes": idLattes,
                "departamento": autor.departamento
            })
        result["prod_tec"].append(obj)

    bancas = Bancas.objects \
        .filter(titulo__icontains=key) \
        .order_by('-ano') \
        .distinct()
    for ban in bancas.all():
        obj = {
            "titulo": ban.titulo,
            "tipo": ban.tipo_pesquisa,
            "ano": ban.ano,
            "autores": []
        }
        for part in ban.participantes.all():
            idLattes = str(autor.id_lattes)
            while len(idLattes) < 16:
                idLattes = str(0) + idLattes

            obj["autores"].append({
                "nome": part.nome_completo,
                "id_lattes": idLattes,
                "departamento": part.departamento
            })
        result["bancas"].append(obj)

    orientacao = Orientacao.objects \
        .filter(titulo__icontains=key) \
        .order_by('-ano') \
        .distinct()
    for ori in orientacao.all():
        idLattes = str(ori.pessoa.id_lattes)
        while len(idLattes) < 16:
            idLattes = str(0) + idLattes
        obj = {
            "titulo": ori.titulo,
            "tipo": ori.tipo_orientacao,
            "ano": ori.ano,
            "autorId": idLattes,
            "autorNome": ori.pessoa.nome_completo,
            "autorDep": ori.pessoa.departamento
        }
        result["orientacao"].append(obj)

    premiosTitulos = PremiosTitulos.objects \
        .filter(nome__icontains=key) \
        .order_by('-ano') \
        .distinct()
    for pre in premiosTitulos.all():
        idLattes = str(pre.pessoa.id_lattes)
        while len(idLattes) < 16:
            idLattes = str(0) + idLattes
        obj = {
            "nome": pre.nome,
            "ano": pre.ano,
            "autorId": idLattes,
            "autorNome": pre.pessoa.nome_completo,
            "autorDep": pre.pessoa.departamento
        }
        result["premiosTitulos"].append(obj)

    return JsonResponse(result)


def count(request):
    """Count for dashboard route.

    Parameters: a GET request with optionals 'ano_inicio' and 'ano_fim'
    parameters.
    Returns:
    JsonResponse with status code and the count of all productions.
   """

    return JsonResponse(services.get_count(request.GET))


def countByDep(request):
    """Count by department for dashboard route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim'
    and 'departamento' parameters.
    Returns:
    JsonResponse with status code and the count of all productions
    for the given department.
   """

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    department = request.GET.get('departamento', '')
    if not department:
        return route_operations.error('Departamento inválido')
    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        return route_operations.error('Ano inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]
    result = {department: {year: 0 for year in labels}}

    author_from_dep = Pessoa.objects \
        .filter(departamento__contains=department) \
        .values('id_lattes')

    prodsArt = ProducaoArtistica.objects \
        .filter(autores__in=author_from_dep) \
        .distinct()
    prodsArt = prodsArt.values('id', 'ano')
    prodsTec = ProducaoTecnica.objects \
        .filter(autores__in=author_from_dep) \
        .distinct()
    prodsTec = prodsTec.values('id', 'ano')
    prodsBib = ProducaoBibliografica.objects \
        .filter(autores__in=author_from_dep) \
        .distinct()
    prodsBib = prodsBib.values('id', 'ano')
    prodsBancas = Bancas.objects \
        .filter(participantes__in=author_from_dep) \
        .distinct()
    prodsBancas = prodsBancas.values('id', 'ano')
    prodsOri = Orientacao.objects \
        .filter(pessoa__in=author_from_dep) \
        .distinct()
    prodsOri = prodsOri.values('id', 'ano')
    prodsPremios = PremiosTitulos.objects \
        .filter(pessoa__in=author_from_dep) \
        .distinct()
    prodsPremios = prodsPremios.values('id', 'ano')

    prods = list(prodsArt.union(prodsTec, prodsBib, prodsBancas, prodsOri,
                                prodsPremios, all=True))

    for prod in prods:
        if (prod['ano'] >= ini_year and prod['ano'] <= end_year):
            result[department][prod['ano']] += 1

    return JsonResponse(result)


def countByDepTipo(request):
    """Count by department and table for dashboard route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'departamento' and 'tipo' parameters.
    Returns:
    JsonResponse with status code and the count of all productions
    for the given department and given table.
   """

    ini_year = request.GET.get('ano_inicio', '1948')
    end_year = request.GET.get('ano_fim', actual_year)
    department = request.GET.get('departamento', '')
    if not department:
        return route_operations.error('Departamento inválido')
    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        return route_operations.error('Ano inválido')
    table = request.GET.get('tipo', '')
    if not table:
        return route_operations.error('Tipo inválido')

    ini_year, end_year = int(ini_year), int(end_year)
    labels = [y for y in range(int(ini_year), int(end_year)+1)]
    result = {department: {}}

    author_from_dep = Pessoa.objects \
        .filter(departamento__contains=department) \
        .values('id_lattes')

    if table == 'Produção Artística':
        prodsArt = ProducaoArtistica.objects \
            .filter(autores__in=author_from_dep) \
            .distinct()
        for label in labels:
            count = prodsArt.filter(ano=label).aggregate(Count('id'))
            result[department][label] = count['id__count']
    if table == 'Produção Técnica':
        prodsTec = ProducaoTecnica.objects \
            .filter(autores__in=author_from_dep) \
            .distinct()
        for label in labels:
            count = prodsTec.filter(ano=label).aggregate(Count('id'))
            result[department][label] = count['id__count']
    if table == 'Produção Bibliográfica':
        prodsBib = ProducaoBibliografica.objects \
            .filter(autores__in=author_from_dep) \
            .distinct()
        for label in labels:
            count = prodsBib.filter(ano=label).aggregate(Count('id'))
            result[department][label] = count['id__count']
    if table == 'Bancas':
        prodsBancas = Bancas.objects \
            .filter(participantes__in=author_from_dep) \
            .distinct()
        for label in labels:
            count = prodsBancas \
                .filter(ano=label) \
                .aggregate(Count('id'))
            result[department][label] = count['id__count']
    if table == 'Orientação':
        prodsOri = Orientacao.objects \
            .filter(pessoa__in=author_from_dep) \
            .distinct()
        for label in labels:
            count = prodsOri.filter(ano=label).aggregate(Count('id'))
            result[department][label] = count['id__count']
    if table == 'Prêmios e Títulos':
        prodsPremios = PremiosTitulos.objects \
            .filter(pessoa__in=author_from_dep) \
            .distinct()
        for label in labels:
            count = prodsPremios \
                .filter(ano=label) \
                .aggregate(Count('id'))
            result[department][label] = count['id__count']

    return JsonResponse(result)


def keywords(request):
    """Keywords for dashboard route.

    Parameters: a GET request with departamento and
    optional 'limit' parameter.

    Returns:
    JsonResponse with status code and a list of words with its frequencies.
    """

    try:
        limit = int(request.GET.get('limit', 20))
    except ValueError:
        return route_operations.error('Limite inválido.')
    if not limit > 0:
        return route_operations.error('Limite inválido.')
    department = request.GET.get('departamento', '')
    if not department:
        return route_operations.error('Departamento inválido')

    result = []

    keywordsProdArt = ProducaoArtistica.keywords_dep(limit, department)
    keywordsProdBib = ProducaoBibliografica.keywords_dep(limit, department)
    keywordsProdTec = ProducaoTecnica.keywords_dep(limit, department)
    keywordsOri = Orientacao.keywords_dep(limit, department)
    keywordsBancas = Bancas.keywords_dep(limit, department)

    keyList = [keywordsProdArt, keywordsProdBib, keywordsProdTec, keywordsOri,
               keywordsBancas]

    keywords = {}

    for keys in keyList:
        for keyword in keys:
            key = keyword[0]
            if key not in keywords:
                keywords[key] = keyword[1]
            else:
                keywords[key] += keyword[1]

    for keyword in keywords:
        result.append({keyword: keywords[keyword]})

    return JsonResponse({"keywords": result[:limit]})


def map(request):
    """List of countries for dashboard route.

    Parameters: a GET request with departamento and
    optionals 'limit' parameters.

    Returns:
    JsonResponse with status code and the list of countries
    and the count of all productions.
   """
    return JsonResponse({"countries": services.get_map(request.GET)})


def national_map(request):
    """National map for dashboard route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the list of states
    and the count of all productions.
   """
    return JsonResponse(services.get_national_map(request.GET))


def last_updated(request):
    """Time of last lattes update route.

    Parameters: a GET request.

    Returns:
    JsonResponse with status code and the time of the last update of the
    curriculums.
    """
    last_update_entry = ParserInfo.objects.latest('data_atualizacao')
    last_update_date = last_update_entry.data_atualizacao.date() \
        .strftime("%d/%m/%Y")

    return JsonResponse({'date': last_update_date})


def aggregated_dashboard(request):
    return JsonResponse(services.get_aggregated_dashboard())


def aggregated_count(request):
    return JsonResponse(services.get_aggregated_count())


def aggregated_map(request):
    return JsonResponse({"countries": services.get_aggregated_map()})


def aggregated_national_map(request):
    return JsonResponse(services.get_aggregated_national_map())