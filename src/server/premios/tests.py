import json
from django.test import TestCase, Client
from django.urls import reverse

from models.models import PremiosTitulos


class ViewPremiosTests(TestCase):
    """ Test module for premios views """
    # fixture file
    fixtures = ['util/fixtures/docentes.json', 'premios/fixtures/tests.json']

    # initialize the APIClient app
    client = Client()

    def test_count(self):
        response = self.client.get(reverse('premios-count'))
        premios_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(premios_list), 3)

        for item in premios_list:
            assert item in ['AUH', 'AUP', 'AUT']
            self.assertEqual(True, len(premios_list[item]['tipo_unico']) > 10)

    def test_count_year(self):
        response = self.client.get(reverse('premios-count'))
        premios_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(premios_list['AUH']['tipo_unico']['2014'], 1)
        self.assertEqual(premios_list['AUH']['tipo_unico']['2015'], 2)
        self.assertEqual(premios_list['AUH']['tipo_unico']['2017'], 1)
        self.assertEqual(premios_list['AUH']['tipo_unico']['2018'], 1)
        self.assertEqual(premios_list['AUP']['tipo_unico']['2015'], 1)

    def test_count_year_range(self):
        response = self.client.get(reverse('premios-count'),
                                   {'ano_inicio': 2017, 'ano_fim': 2019})
        premios_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(premios_list), 3)

        for item in premios_list:
            assert item in ['AUH', 'AUP', 'AUT']
            self.assertEqual(len(premios_list[item]['tipo_unico']), 3)

    def test_count_invalid_year(self):
        response = self.client.get(reverse('premios-count'),
                                   {'ano_inicio': -1, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 400)

    def test_count_invalid_year2(self):
        response = self.client.get(reverse('premios-count'),
                                   {'ano_inicio': 'batata', 'ano_fim': 0})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 400)

    def test_count_default_format(self):
        response1 = self.client.get(reverse('premios-count'))
        json_response1 = json.loads(response1.content)
        response2 = self.client.get(reverse('premios-count'),
                                    {'format': 'default'})
        json_response2 = json.loads(response2.content)
        self.assertEqual(json_response1, json_response2)

    def test_count_invalid_format(self):
        response = self.client.get(reverse('premios-count'),
                                   {'ano_inicio': 'zebra'})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 400)

    def test_count_with_empty_database(self):
        PremiosTitulos.objects.all().delete()

        response = self.client.get(reverse('premios-count'))
        premios_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(premios_list), 3)

        for item in premios_list:
            assert item in ['AUH', 'AUP', 'AUT']
            years = premios_list[item]['tipo_unico']
            for value in years:
                self.assertEqual(years[value], 0)
