from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /premios/count
    url(r'^count/$', views.count, name='premios-count'),
    # ex: /premios/aggregated-count
    url(r'^aggregated-count/$', views.aggregated_count, name='premios-aggregated_count'),
]
