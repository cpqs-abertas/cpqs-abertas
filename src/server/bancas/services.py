from models.models import Pessoa, Bancas
import util.fau.route_operations as route_operations
import util.populate.code.parsing.db.connection as db_connection

aggregates = db_connection.get_mongo_db().bancas

def get_count(params):
    return route_operations.count(Bancas, params)


def get_keywords(params):
    return route_operations.keywords(Bancas, params)


def get_map(params):
    return route_operations.map(Bancas, params)


def get_types_count(params):
    departments = params.getlist('departamentos[]')
    types = params.getlist('tipos[]')

    if not departments:
        departments = [dep['departamento']
                       for dep in Pessoa.objects.values('departamento')
                           .distinct()]
    if not types:
        types = [typ['tipo_pesquisa']
                 for typ in Bancas.objects.values('tipo_pesquisa')
                     .distinct()]

    result = {}
    for department in departments:
        result[department] = []
        for type in types:
            values = {}
            parts_from_dep = Pessoa.objects \
                .filter(departamento__contains=department) \
                .values('id_lattes')
            bancas_count = Bancas.objects \
                .filter(participantes__in=parts_from_dep) \
                .distinct() \
                .filter(tipo_pesquisa=type).count()

            values[type] = bancas_count
            result[department].append(values)

    return result


def get_types():
    types = Bancas.objects \
        .all() \
        .values('tipo_pesquisa') \
        .distinct() \
        .order_by('tipo_pesquisa')
    result = []
    accepted = ['Doutorado', 'Graduacao', 'Livre_Docencia', 'Mestrado',
                'Outra']
    for t in types:
        if t['tipo_pesquisa'] in accepted:
            result.append(t['tipo_pesquisa'])

    return result


def get_aggregated_count():
    return aggregates.find_one({ "operation": "count" })["result"]


def get_aggregated_count_by_type():
    return aggregates.find_one({ "operation": "count_by_type" })["result"]


def get_aggregated_keywords():
    return aggregates.find_one({ "operation": "keywords" })["result"]


def get_aggregated_map():
    return aggregates.find_one({ "operation": "map" })["result"]


def get_aggregated_national_map():
    return aggregates.find_one({ "operation": "national_map" })["result"]


def get_aggregated_types_count():
    return aggregates.find_one({ "operation": "types_count" })["result"]
