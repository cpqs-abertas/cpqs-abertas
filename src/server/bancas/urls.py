from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /bancas/count
    url(r'^count$', views.count, name='bancas-count'),
    # ex: /bancas/aggregated-count
    url(r'^aggregated-count$', views.aggregated_count, name='bancas-aggregated_count'),
    # ex: /bancas/aggregated-count-by-type
    url(r'^aggregated-count-by-type$', views.aggregated_count_by_type, name='bancas-aggregated_count_by_type'),

    # ex: /bancas/keywords
    url(r'^keywords$', views.keywords, name='bancas-keywords'),
    # ex: /bancas/aggregated-keywords
    url(r'^aggregated-keywords$', views.aggregated_keywords, name='bancas-aggregated_keywords'),

    # ex: /bancas/map
    url(r'^map$', views.map, name='bancas-map'),
    # ex: /bancas/aggregated-map
    url(r'^aggregated-map$', views.aggregated_map, name='bancas-aggregated_map'),

    # ex: /bancas/types-count
    url(r'^types-count$', views.types_count, name='bancas-types_count'),
]
