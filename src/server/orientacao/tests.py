import json
from django.test import TestCase, Client
from django.urls import reverse

from models.models import Orientacao, Pessoa


class ViewOrientacaoTests(TestCase):
    """ Test module for orientacao views """
    # fixture file
    fixtures = [
        'util/fixtures/docentes.json',
        'orientacao/fixtures/tests.json'
    ]

    # initialize the APIClient app
    client = Client()

    def test_count_returns_all_departments(self):
        response = self.client.get(reverse('orientacao-count'))
        prod_list = json.loads(response.content)

        expected_departments = [dep['departamento']
                                for dep in Pessoa.objects
                                .values('departamento').distinct()]

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(prod_list), len(expected_departments))

        for dep in prod_list:
            assert dep in expected_departments

    def test_count_returns_all_types(self):
        response = self.client.get(reverse('orientacao-count'))
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        expected_types = [typ['tipo_pesquisa']
                          for typ in Orientacao.objects
                          .values('tipo_pesquisa').distinct()]

        for dep in prod_list:
            self.assertEqual(len(prod_list[dep]), len(expected_types))
            for type in prod_list[dep]:
                assert type in expected_types

                # 10 keys: one for each year
                self.assertEqual(True, len(prod_list[dep][type]) > 1)

    def test_count_with_specific_department(self):
        response = self.client.get(
            reverse('orientacao-count'), {'departamentos[]': 'AUH'})
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(prod_list), 1)
        self.assertEqual(list(prod_list.keys()), ['AUH'])

    def test_count_with_specific_type(self):
        types = ['trabalho_de_conclusao_de_curso_graduacao', 'teste']
        response = self.client.get(
            reverse('orientacao-count'), {'tipos[]': types})
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        for dep in prod_list:
            type_list = list(prod_list[dep].keys())
            self.assertEqual(len(type_list), len(types))
            self.assertEqual(sorted(type_list), sorted(types))

    def test_count_returns_correctly_for_each_year(self):
        response = self.client.get(reverse('orientacao-count'))
        prod_list = json.loads(response.content)

        tcc = 'trabalho_de_conclusao_de_curso_graduacao'
        self.assertEqual(response.status_code, 200)
        self.assertEqual(prod_list['AUP'][tcc]['2018'], 1)
        self.assertEqual(prod_list['AUT']['mestrado']['2015'], 0)
        self.assertEqual(prod_list['AUH'][tcc]['2017'], 1)
        self.assertEqual(prod_list['AUT']['mestrado']['2017'], 0)
        self.assertEqual(prod_list['AUH']['mestrado']['2017'], 2)
        self.assertEqual(prod_list['AUP']['mestrado']['2017'], 0)

    def test_count_returns_correctly_for_specific_year_range(self):
        response = self.client.get(
            reverse('orientacao-count'), {'ano_inicio': 2017, 'ano_fim': 2019})
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        for dep in prod_list:
            for type in prod_list[dep]:
                self.assertEqual(len(prod_list[dep][type]), 3)

    def test_count_returns_400_with_invalid_range(self):
        response = self.client.get(
            reverse('orientacao-count'), {'ano_inicio': -1, 'ano_fim': 2019})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 400)

    def test_count_returns_400_with_invalid_year(self):
        response = self.client.get(
            reverse('orientacao-count'), {'ano_inicio': 'batata',
                                          'ano_fim': 0})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 400)

    def test_count_with_empty_database(self):
        Orientacao.objects.all().delete()

        response = self.client.get(reverse('orientacao-count'))
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        for dep in prod_list:
            for type in prod_list[dep]:
                for year in prod_list[dep][type]:
                    self.assertEqual(prod_list[dep][type], 0)

    def test_keywords_with_default_limit(self):
        response = self.client.get(reverse('orientacao-keywords'))
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        assert 'keywords' in json_response
        expected_result = [
            {"titulo": 6},
            {"teste": 3},
            {"foo": 2},
            {"bar": 1}
        ]
        self.assertEqual(json_response['keywords'], expected_result)

    def test_keywords_with_specific_limit(self):
        response = self.client.get(
            reverse('orientacao-keywords'), {"limit": 2})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        assert 'keywords' in json_response
        expected_result = [
            {"titulo": 6},
            {"teste": 3}
        ]
        self.assertEqual(json_response['keywords'], expected_result)

    def test_keywords_with_empty_database(self):
        Orientacao.objects.all().delete()

        response = self.client.get(reverse('orientacao-keywords'))
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        assert 'keywords' in json_response
        self.assertEqual(json_response['keywords'], [])

    def test_keywords_with_negative_limit_returns_400(self):
        response = self.client.get(
            reverse('orientacao-keywords'), {"limit": -1})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 400)

    def test_keywords_with_invalid_limit_returns_400(self):
        response = self.client.get(
            reverse('orientacao-keywords'), {"limit": "foo"})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 400)

    def test_map_with_default_limit(self):
        response = self.client.get(reverse('orientacao-map'))
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        assert 'countries' in json_response
        expected_result = [
            {'Argélia': {
                'lat': '28.0000272', 'long': '2.9999825', 'total': 1
            }},
            {'Brasil': {
                'lat': '-10.3333333', 'long': '-53.2', 'total': 4
            }},
            {'Espanha': {
                'lat': '39.3260685', 'long': '-4.8379791', 'total': 1
            }}
        ]
        self.assertCountEqual(json_response['countries'], expected_result)

    def test_map_with_specific_limit(self):
        response = self.client.get(
            reverse('orientacao-map'), {"limit": 2})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        assert 'countries' in json_response
        expected_result = [
            {'Argélia': {
                'lat': '28.0000272', 'long': '2.9999825', 'total': 1
            }},
            {'Brasil': {
                'lat': '-10.3333333', 'long': '-53.2', 'total': 4
            }}
        ]
        self.assertCountEqual(json_response['countries'], expected_result)

    def test_map_with_empty_database(self):
        Orientacao.objects.all().delete()

        response = self.client.get(reverse('orientacao-map'))
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        assert 'countries' in json_response
        self.assertEqual(json_response['countries'], [])

    def test_map_with_negative_limit_returns_400(self):
        response = self.client.get(
            reverse('orientacao-map'), {"limit": -1})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 400)

    def test_map_with_invalid_limit_returns_400(self):
        response = self.client.get(
            reverse('orientacao-map'), {"limit": "foo"})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 400)

    def test_types_count_returns_all_departments(self):
        response = self.client.get(
            reverse('orientacao-types_count'))
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        expected_departments = [dep['departamento']
                                for dep in Pessoa.objects
                                .values('departamento').distinct()]
        returned_departments = list(prod_list.keys())

        self.assertEqual(len(returned_departments), len(expected_departments))
        self.assertEqual(sorted(returned_departments),
                         sorted(expected_departments))

    def test_types_count_returns_all_types(self):
        response = self.client.get(
            reverse('orientacao-types_count'))
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        expected_types = [typ['tipo_pesquisa']
                          for typ in Orientacao.objects
                          .values('tipo_pesquisa').distinct()]

        for dep in prod_list:
            self.assertEqual(len(prod_list[dep]), len(expected_types))
            returned_types = []
            for value in prod_list[dep]:
                returned_types.extend(list(value.keys()))
            self.assertEqual(sorted(returned_types), sorted(expected_types))

    def test_types_count_with_specific_department(self):
        response = self.client.get(
            reverse('orientacao-types_count'), {'departamentos[]': 'AUH'})
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(prod_list), 1)
        self.assertEqual(list(prod_list.keys()), ['AUH'])

    def test_types_count_with_specific_type(self):
        types = ['trabalho_de_conclusao_de_curso_graduacao', 'teste']
        response = self.client.get(
            reverse('orientacao-types_count'), {'tipos[]': types})
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        for dep in prod_list:
            returned_types = []
            for value in prod_list[dep]:
                returned_types.extend(list(value.keys()))
            self.assertEqual(len(returned_types), len(types))
            self.assertEqual(sorted(returned_types), sorted(types))

    def test_types_count_returns_correctly(self):
        response = self.client.get(
            reverse('orientacao-types_count'), {
                'departamentos[]': ['AUH'],
                'tipos[]': ['trabalho_de_conclusao_de_curso_graduacao']
            })
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        tcc = 'trabalho_de_conclusao_de_curso_graduacao'
        self.assertEqual(len(prod_list['AUH']), 1)
        self.assertEqual(prod_list['AUH'][0][tcc], 1)
