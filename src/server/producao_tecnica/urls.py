from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /producao-tecnica/count
    url(r'^count$', views.count, name='producao_tecnica-count'),
    # ex: /producao-tecnica/aggregated-count
    url(r'^aggregated-count$', views.aggregated_count, name='producao_tecnica-aggregated_count'),
    # ex: /producao-tecnica/aggregated-count-by-type
    url(r'^aggregated-count-by-type$', views.aggregated_count_by_type, name='producao_tecnica-aggregated_count_by_type'),

    # ex: /producao-tecnica/keywords
    url(r'^keywords$', views.keywords, name='producao_tecnica-keywords'),
      # ex: /producao-tecnica/aggregated-keywords
    url(r'^aggregated-keywords$', views.aggregated_keywords, name='producao_tecnica-aggregated_keywords'),

    # ex: /producao-tecnica/map
    url(r'^map$', views.map, name='producao_tecnica-map'),
     # ex: /producao-tecnica/aggregated-map
    url(r'^aggregated-map$', views.aggregated_map, name='producao_tecnica-aggregated_map'),

    # ex: /producao-tecnica/national-map
    url(r'^national-map$', views.national_map, name='producao_tecnica-national_map'),
    # ex: /producao-tecnica/aggregated-national-map
    url(r'^aggregated-national-map$', views.aggregated_national_map, name='producao_tecnica-aggregated_national_map'),

    # ex: /producao-tecnica/types-count
    url(r'^types-count$', views.types_count, name='producao_tecnica-types_count'),
]
