from models.models import Pessoa, ProducaoTecnica
import util.fau.route_operations as route_operations
import util.populate.code.parsing.db.connection as db_connection

aggregates = db_connection.get_mongo_db().producao_tecnica

def get_count(params):
    return route_operations.count(ProducaoTecnica, params)


def get_keywords(params):
    return route_operations.keywords(ProducaoTecnica, params)


def get_map(params):
    return route_operations.map(ProducaoTecnica, params)


def get_national_map(params):
    return route_operations.national_map(ProducaoTecnica, params)


def get_types_count(params):
    return route_operations.types_count(ProducaoTecnica, params)


def get_types():
    types = ProducaoTecnica.objects \
        .all() \
        .values('tipo') \
        .distinct() \
        .order_by('tipo')
    result = []
    accepted = [
        'Trabalho_Tecnico', 'Organizacao_De_Evento',
        'Curso_De_Curta_Duracao_Ministrado', 'Programa_De_Radio_Ou_Tv',
        'Apresentacao_De_Trabalho'
    ]
    for t in types:
        if t['tipo'] in accepted:
            result.append(t['tipo'])
    return result


def get_aggregated_count():
    return aggregates.find_one({ "operation": "count" })["result"]


def get_aggregated_count_by_type():
    return aggregates.find_one({ "operation": "count_by_type" })["result"]


def get_aggregated_keywords():
    return aggregates.find_one({ "operation": "keywords" })["result"]


def get_aggregated_map():
    return aggregates.find_one({ "operation": "map" })["result"]


def get_aggregated_national_map():
    return aggregates.find_one({ "operation": "national_map" })["result"]


def get_aggregated_types_count():
    return aggregates.find_one({ "operation": "types_count" })["result"]
