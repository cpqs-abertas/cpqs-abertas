from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer

from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Bancas(models.Model):
    id = models.AutoField(primary_key=True)

    aluno = models.TextField()
    ano = models.IntegerField()
    curso = models.TextField(blank=True, null=True)
    doi = models.TextField(blank=True, null=True)
    idioma = models.TextField(blank=True, null=True)
    instituicao = models.TextField(blank=True, null=True)
    pais = models.TextField(blank=True, null=True)
    tipo_pesquisa = models.TextField(blank=True, null=True)
    titulo = models.TextField()

    participantes = models.ManyToManyField(
        'Pessoa', through='RelPessoaBancas', related_name='bancas')

    @staticmethod
    def keywords(limit):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        for banca in Bancas.objects.all():
            title = tokenizer.tokenize(banca.titulo.lower())
            for word in title:
                lower_word = word
                if (lower_word.isnumeric() or len(word) <= 2
                        or word in stopwords_pt or word in stopwords_en):
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_words

    @staticmethod
    def keywords_dep(limit, dep):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        author_from_dep = Pessoa.objects \
            .filter(departamento__contains=dep) \
            .values('id_lattes')
        for banca in Bancas.objects \
                .filter(participantes__in=author_from_dep) \
                .distinct():
            title = tokenizer.tokenize(banca.titulo.lower())
            for word in title:
                lower_word = word
                if lower_word.isnumeric() or len(word) <= 2 or word in \
                        stopwords_pt or word in stopwords_en:
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_words

    @staticmethod
    def database_fields_map():
        return {
            'professors_departments': 'participantes__departamento',
            'production_type': 'tipo_pesquisa',
            'production_involved_people': 'participantes'
        }

    class Meta:
        managed = True
        db_table = 'bancas'
        unique_together = (('aluno', 'ano', 'titulo'),)


class Colaboradores(models.Model):
    id = models.AutoField(primary_key=True)

    colaborador1_id = models.ForeignKey('Pessoa', models.DO_NOTHING,
                                        to_field='id_lattes',
                                        db_column='colaborador1_id',
                                        related_name='da_pessoa')
    colaborador2_id = models.ForeignKey('Pessoa', models.DO_NOTHING,
                                        to_field='id_lattes',
                                        db_column='colaborador2_id',
                                        related_name='para_pessoa')

    class Meta:
        managed = True
        db_table = 'colaboradores'
        unique_together = (('colaborador1_id', 'colaborador2_id'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING,
                                     blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Orientacao(models.Model):
    id = models.AutoField(primary_key=True)

    agencia_fomento = models.TextField(blank=True, null=True)
    aluno = models.TextField()
    ano = models.IntegerField()
    curso = models.TextField(blank=True, null=True)
    doi = models.TextField(blank=True, null=True)
    flag = models.TextField(blank=True, null=True)
    idioma = models.TextField(blank=True, null=True)
    instituicao = models.TextField(blank=True, null=True)
    natureza = models.TextField(blank=True, null=True)
    pais = models.TextField(blank=True, null=True)
    tipo_orientacao = models.TextField(blank=True, null=True)
    tipo_pesquisa = models.TextField(blank=True, null=True)
    titulo = models.TextField()

    pessoa = models.ForeignKey('Pessoa', models.DO_NOTHING,
                               to_field='id_lattes')

    @staticmethod
    def keywords(limit):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        for producao in Orientacao.objects.all():
            title = tokenizer.tokenize(producao.titulo.lower())
            for word in title:
                lower_word = word
                if (lower_word.isnumeric() or len(word) <= 2
                        or word in stopwords_pt or word in stopwords_en):
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    @staticmethod
    def keywords_dep(limit, dep):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        author_from_dep = Pessoa.objects \
            .filter(departamento__contains=dep) \
            .values('id_lattes')
        for producao in Orientacao.objects \
                .filter(pessoa__in=author_from_dep) \
                .distinct():
            title = tokenizer.tokenize(producao.titulo.lower())
            for word in title:
                lower_word = word
                if (lower_word.isnumeric() or len(word) <= 2
                        or word in stopwords_pt or word in stopwords_en):
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    @staticmethod
    def database_fields_map():
        return {
            'professors_departments': 'pessoa__departamento',
            'production_type': 'tipo_pesquisa',
            'production_involved_people': 'pessoa'
        }

    class Meta:
        managed = True
        db_table = 'orientacao'
        unique_together = (('pessoa', 'aluno', 'titulo', 'ano'),)


class Pessoa(models.Model):
    id = models.AutoField(primary_key=True)

    id_lattes = models.BigIntegerField(unique=True)
    cidade = models.TextField(blank=True, null=True)
    contato = models.TextField(blank=True, null=True)
    departamento = models.TextField()
    estado = models.TextField(blank=True, null=True)
    nacionalidade = models.TextField(blank=True, null=True)
    nome_completo = models.TextField()
    nome_citacao = models.TextField()
    resumo = models.TextField(blank=True, null=True)

    producoes_bibliograficas = models.ManyToManyField(
        'ProducaoBibliografica', through='RelPessoaProdBib',
        related_name='producoes_bibliograficas')
    producoes_tecnicas = models.ManyToManyField(
        'ProducaoTecnica', through='RelPessoaProdTec',
        related_name='producoes_tecnica')
    producoes_artisticas = models.ManyToManyField(
        'ProducaoArtistica', through='RelPessoaProdArt',
        related_name='producoes_artisticas')
    aparicoes_bancas = models.ManyToManyField(
        'Bancas', through='RelPessoaBancas',
        related_name='aparicoes_bancas')
    participacoes = models.ManyToManyField(
        'Participacao', through='RelPessoaPart',
        related_name='participacoes')

    @staticmethod
    def keywords(id, limit):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        query_set = Pessoa.objects.filter(id_lattes=id)
        if not query_set:
            return 'erro'
        for prod_art in query_set[0].producoes_artisticas.all():
            title = tokenizer.tokenize(prod_art.titulo.lower())
            for word in title:
                lower_word = word
                if (lower_word.isnumeric() or len(word) <= 2
                        or word in stopwords_pt or word in stopwords_en):
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        for prod_bib in query_set[0].producoes_bibliograficas.all():
            title = tokenizer.tokenize(prod_bib.titulo.lower())
            for word in title:
                lower_word = word
                if (lower_word.isnumeric() or len(word) <= 2
                        or word in stopwords_pt or word in stopwords_en):
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        for prod_tec in query_set[0].producoes_tecnicas.all():
            title = tokenizer.tokenize(prod_tec.titulo.lower())
            for word in title:
                lower_word = word
                if (lower_word.isnumeric() or len(word) <= 2
                        or word in stopwords_pt or word in stopwords_en):
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    class Meta:
        managed = True
        db_table = 'pessoa'


class PremiosTitulos(models.Model):
    id = models.AutoField(primary_key=True)

    ano = models.IntegerField()
    entidade = models.TextField(blank=True, null=True)
    nome = models.TextField()

    pessoa = models.ForeignKey(Pessoa, models.DO_NOTHING,
                               to_field='id_lattes')

    class Meta:
        managed = True
        db_table = 'premios_titulos'
        unique_together = (('nome', 'ano'),)


class ProducaoArtistica(models.Model):
    id = models.AutoField(primary_key=True)

    ano = models.IntegerField()
    doi = models.TextField(blank=True, null=True)
    estado = models.TextField(blank=True, null=True)
    exposicao = models.TextField(blank=True, null=True)
    flag = models.TextField(blank=True, null=True)
    idioma = models.TextField(blank=True, null=True)
    meio = models.TextField(blank=True, null=True)
    natureza = models.TextField(blank=True, null=True)
    nomes_autores = models.TextField(blank=True, null=True)
    pais = models.TextField(blank=True, null=True)
    premiacao = models.TextField(blank=True, null=True)
    tipo = models.TextField()
    titulo = models.TextField()

    autores = models.ManyToManyField(
        'Pessoa', through='RelPessoaProdArt', related_name='autores_artistica')

    @staticmethod
    def keywords(limit):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        for producao in ProducaoArtistica.objects.all():
            title = tokenizer.tokenize(producao.titulo.lower())
            for word in title:
                lower_word = word
                if (lower_word.isnumeric() or len(word) <= 2
                        or word in stopwords_pt or word in stopwords_en):
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    @staticmethod
    def keywords_dep(limit, dep):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        author_from_dep = Pessoa.objects \
            .filter(departamento__contains=dep) \
            .values('id_lattes')
        for producao in ProducaoArtistica.objects \
                .filter(autores__in=author_from_dep) \
                .distinct():
            title = tokenizer.tokenize(producao.titulo.lower())
            for word in title:
                lower_word = word
                if (lower_word.isnumeric() or len(word) <= 2
                        or word in stopwords_pt or word in stopwords_en):
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    @staticmethod
    def database_fields_map():
        return {
            'professors_departments': 'autores__departamento',
            'production_type': 'tipo',
            'production_involved_people': 'autores'
        }

    class Meta:
        managed = True
        db_table = 'producao_artistica'
        unique_together = (('tipo', 'titulo', 'ano'),)


class ProducaoBibliografica(models.Model):
    id = models.AutoField(primary_key=True)

    ano = models.IntegerField()
    doi = models.TextField(blank=True, null=True)
    edicao = models.TextField(blank=True, null=True)
    editora = models.TextField(blank=True, null=True)
    estado = models.TextField(blank=True, null=True)
    flag = models.TextField(blank=True, null=True)
    idioma = models.TextField(blank=True, null=True)
    isbn = models.TextField(blank=True, null=True)
    meio = models.TextField(blank=True, null=True)
    natureza = models.TextField(blank=True, null=True)
    nomes_autores = models.TextField(blank=True, null=True)
    pais = models.TextField(blank=True, null=True)
    publicacao = models.TextField(blank=True, null=True)
    tipo = models.TextField()
    titulo = models.TextField()
    volume = models.TextField(blank=True, null=True)

    autores = models.ManyToManyField(
        'Pessoa', through='RelPessoaProdBib',
        related_name='autores_bibliografica')

    @staticmethod
    def keywords(limit):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        for producao in ProducaoBibliografica.objects.all():
            title = tokenizer.tokenize(producao.titulo.lower())
            for word in title:
                lower_word = word
                if (lower_word.isnumeric() or len(word) <= 2
                        or word in stopwords_pt or word in stopwords_en):
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    @staticmethod
    def keywords_dep(limit, dep):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        author_from_dep = Pessoa.objects \
            .filter(departamento__contains=dep) \
            .values('id_lattes')
        for producao in ProducaoBibliografica.objects \
                .filter(autores__in=author_from_dep) \
                .distinct():
            title = tokenizer.tokenize(producao.titulo.lower())
            for word in title:
                lower_word = word
                if (lower_word.isnumeric() or len(word) <= 2
                        or word in stopwords_pt or word in stopwords_en):
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    @staticmethod
    def database_fields_map():
        return {
            'professors_departments': 'autores__departamento',
            'production_type': 'tipo',
            'production_involved_people': 'autores'
        }

    class Meta:
        managed = True
        db_table = 'producao_bibliografica'
        unique_together = (('tipo', 'titulo', 'ano'),)


class ProducaoTecnica(models.Model):
    id = models.AutoField(primary_key=True)

    ano = models.IntegerField()
    estado = models.TextField(blank=True, null=True)
    finalidade = models.TextField(blank=True, null=True)
    flag = models.TextField(blank=True, null=True)
    idioma = models.TextField(blank=True, null=True)
    meio = models.TextField(blank=True, null=True)
    natureza = models.TextField(blank=True, null=True)
    nomes_autores = models.TextField(blank=True, null=True)
    pags = models.TextField(blank=True, null=True)
    pais = models.TextField(blank=True, null=True)
    tipo = models.TextField()
    titulo = models.TextField()

    autores = models.ManyToManyField(
        'Pessoa', through='RelPessoaProdTec', related_name='autores_tecnica')

    @staticmethod
    def keywords(limit):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        for producao in ProducaoTecnica.objects.all():
            title = tokenizer.tokenize(producao.titulo.lower())
            for word in title:
                lower_word = word
                if (lower_word.isnumeric() or len(word) <= 2
                        or word in stopwords_pt or word in stopwords_en):
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    @staticmethod
    def keywords_dep(limit, dep):
        words = {}
        stopwords_pt = set(stopwords.words('portuguese'))
        stopwords_en = set(stopwords.words('english'))
        tokenizer = RegexpTokenizer(r'\w+')
        author_from_dep = Pessoa.objects \
            .filter(departamento__contains=dep) \
            .values('id_lattes')
        for producao in ProducaoTecnica.objects \
                .filter(autores__in=author_from_dep) \
                .distinct():
            title = tokenizer.tokenize(producao.titulo.lower())
            for word in title:
                lower_word = word
                if (lower_word.isnumeric() or len(word) <= 2
                        or word in stopwords_pt or word in stopwords_en):
                    continue
                if word in words:
                    words[word] += 1
                else:
                    words[word] = 1
        top_10_words = sorted(words.items(), key=lambda x: -x[1])[:limit]

        return top_10_words

    @staticmethod
    def database_fields_map():
        return {
            'professors_departments': 'autores__departamento',
            'production_type': 'tipo',
            'production_involved_people': 'autores'
        }

    class Meta:
        managed = True
        db_table = 'producao_tecnica'
        unique_together = (('tipo', 'titulo', 'ano'),)


class RelPessoaBancas(models.Model):
    id = models.AutoField(primary_key=True)

    pessoa = models.ForeignKey(Pessoa, models.DO_NOTHING,
                               to_field='id_lattes')
    bancas = models.ForeignKey(Bancas, models.DO_NOTHING)

    class Meta:
        managed = True
        db_table = 'rel_pessoa_bancas'
        unique_together = (('pessoa', 'bancas'),)


class RelPessoaProdArt(models.Model):
    id = models.AutoField(primary_key=True)

    pessoa = models.ForeignKey(Pessoa, models.DO_NOTHING,
                               to_field='id_lattes')
    producao = models.ForeignKey(ProducaoArtistica, models.DO_NOTHING)

    class Meta:
        managed = True
        db_table = 'rel_pessoa_prod_art'
        unique_together = (('pessoa', 'producao'),)


class RelPessoaProdBib(models.Model):
    id = models.AutoField(primary_key=True)

    pessoa = models.ForeignKey(Pessoa, models.DO_NOTHING,
                               to_field='id_lattes')
    producao = models.ForeignKey(ProducaoBibliografica,
                                 models.DO_NOTHING)

    class Meta:
        managed = True
        db_table = 'rel_pessoa_prod_bib'
        unique_together = (('pessoa', 'producao'),)


class RelPessoaProdTec(models.Model):
    id = models.AutoField(primary_key=True)

    pessoa = models.ForeignKey(Pessoa, models.DO_NOTHING,
                               to_field='id_lattes')
    producao = models.ForeignKey(ProducaoTecnica, models.DO_NOTHING)

    class Meta:
        managed = True
        db_table = 'rel_pessoa_prod_tec'
        unique_together = (('pessoa', 'producao'),)


class Participacao(models.Model):
    id = models.AutoField(primary_key=True)

    ano = models.IntegerField()
    doi = models.TextField(blank=True, null=True)
    flag = models.TextField(blank=True, null=True)
    idioma = models.TextField(blank=True, null=True)
    meio = models.TextField(blank=True, null=True)
    natureza = models.TextField(blank=True, null=True)
    nome_ev = models.TextField()
    nome_inst = models.TextField(blank=True, null=True)
    pais = models.TextField(blank=True, null=True)
    tipo = models.TextField()

    autores = models.ManyToManyField(
        'Pessoa', through='RelPessoaPart', related_name='autores_participacao')

    class Meta:
        managed = True
        db_table = 'participacao'
        unique_together = (('tipo', 'nome_ev', 'ano'),)


class RelPessoaPart(models.Model):
    id = models.AutoField(primary_key=True)

    pessoa = models.ForeignKey(Pessoa, models.DO_NOTHING,
                               to_field='id_lattes')
    participacao = models.ForeignKey(Participacao, models.DO_NOTHING)

    forma = models.TextField(blank=True, null=True)
    tipo = models.TextField(blank=True, null=True)
    titulo = models.TextField()

    class Meta:
        managed = True
        db_table = 'rel_pessoa_participacao'
        unique_together = (('pessoa', 'participacao', 'titulo'),)


class ParserInfo(models.Model):
    id = models.AutoField(primary_key=True)

    data_atualizacao = models.DateTimeField(auto_now_add=True)

    class Meta:
        managed = True
        db_table = 'parser_info'
