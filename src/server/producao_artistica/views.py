from django.http import JsonResponse
from . import services
from util.fau.RequestError import RequestError
from util.fau.route_operations import error

def count(request):
    """Count for producao_artistica route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'departamentos[]' and 'tipos[]' parameters.

    Returns:
    JsonResponse with status code and the count of artistic productions
    for each year for each departament.
    """
    try:
        return JsonResponse(services.get_count(request.GET))
    except RequestError as request_error:
        return error(request_error.error_message, request_error.return_code)


def keywords(request):
    """Keywords for producao_artistica route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and a list of words with its frequencies.
    """
    try:
        return JsonResponse({"keywords": services.get_keywords(request.GET)})
    except RequestError as request_error:
        return error(request_error.error_message, request_error.return_code)


def map(request):
    """Map for producao_artistica route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the list of countries
    and the specific artistic productions counter.
    """
    try:
        return JsonResponse({"countries": services.get_map(request.GET)})
    except RequestError as request_error:
        return error(request_error.error_message, request_error.return_code)


def national_map(request):
    """National map for producao_bibliografica route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the list of countries and the specific
    bibliographic productions counter.
    """
    try:
        return JsonResponse(services.get_national_map(request.GET))
    except RequestError as request_error:
        return error(request_error.error_message, request_error.return_code)


def types_count(request):
    """Count_tipos for producao_artistica route.

    Parameters: a GET request with 'tipos[]' and 'departamentos[]'.

    Returns:
    JsonResponse with status code and a list with the amount of each
    artistic production's type.
   """
    try:
        return JsonResponse(services.get_types_count(request.GET))
    except RequestError as request_error:
        return error(request_error.error_message, request_error.return_code)


def aggregated_count(request):
    return JsonResponse(services.get_aggregated_count())


def aggregated_count_by_type(request):
    return JsonResponse(services.get_aggregated_count_by_type())


def aggregated_keywords(request):
    return JsonResponse({"keywords": services.get_aggregated_keywords()})


def aggregated_map(request):
    return JsonResponse({"countries": services.get_aggregated_map()})


def aggregated_national_map(request):
    return JsonResponse(services.get_aggregated_national_map())


def aggregated_types_count(request):
    return JsonResponse(services.get_aggregated_types_count())
