from django.conf.urls import url
from . import views

urlpatterns = [
    # ex: /producao-artistica/count
    url(r'^count$', views.count, name='producao_artistica-count'),
    # ex: /producao-artistica/aggregated-count
    url(r'^aggregated-count$', views.aggregated_count, name='producao_artistica-aggregated_count'),
    # ex: /producao-artistica/aggregated-count-by-type
    url(r'^aggregated-count-by-type$', views.aggregated_count_by_type, name='producao_artistica-aggregated_count_by_type'),

    # ex: /producao-artistica/keywords
    url(r'^keywords$', views.keywords, name='producao_artistica-keywords'),
      # ex: /producao-artistica/aggregated-keywords
    url(r'^aggregated-keywords$', views.aggregated_keywords, name='producao_artistica-aggregated-keywords'),

    # ex: /producao-artistica/map
    url(r'^map$', views.map, name='producao_artistica-map'),
     # ex: /producao-artistica/aggregated-map
    url(r'^aggregated-map$', views.aggregated_map, name='producao_artistica-aggregated-map'),

    # ex: /producao-artistica/national-map
    url(r'^national-map$', views.national_map, name='producao_artistica-national_map'),
    # ex: /producao-artistica/aggregated-national-map
    url(r'^aggregated-national-map$', views.aggregated_national_map, name='producao_artistica-aggregated_national_map'),

    # ex: /producao-artistica/types-count
    url(r'^types-count$', views.types_count, name='producao_artistica-types_count'),
]
