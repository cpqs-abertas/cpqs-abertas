import json
from django.test import TestCase, Client
from django.urls import reverse

from models.models import ProducaoArtistica, RelPessoaProdArt


class ViewProducaoArtisticaTests(TestCase):
    """ Test module for producao_artistica views """
    # fixture file
    fixtures = [
        'util/fixtures/docentes.json',
        'producao_artistica/fixtures/tests.json'
    ]

    # initialize the APIClient app
    client = Client()

    def test_count_returns_correctly_for_each_department(self):
        response = self.client.get(reverse('producao_artistica-count'))
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(prod_list), 3)

        for item in prod_list:
            assert item in ['AUH', 'AUP', 'AUT']
            self.assertEqual(len(prod_list[item]), 2)

    def test_count_returns_correctly_for_each_year(self):
        response = self.client.get(reverse('producao_artistica-count'))
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(prod_list['AUH']['tipo1']['2011'], 1)
        self.assertEqual(prod_list['AUT']['tipo2']['2015'], 1)
        self.assertEqual(prod_list['AUH']['tipo1']['2015'], 1)

    def test_count_returns_correctly_for_specific_year_range(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'ano_inicio': 2017,
                                                  'ano_fim': 2019})
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(prod_list), 3)

        for item in prod_list:
            assert item in ['AUH', 'AUP', 'AUT']
            self.assertEqual(len(prod_list[item]), 2)

    def test_count_returns_400_with_invalid_range(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'ano_inicio': -1,
                                                  'ano_fim': 2019})

        self.assertEqual(response.status_code, 400)

    def test_count_returns_400_with_invalid_year(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'ano_inicio': 'batata',
                                                  'ano_fim': 0})

        self.assertEqual(response.status_code, 400)

    def test_count_returns_default_format(self):
        response1 = self.client.get(reverse('producao_artistica-count'))
        json_response1 = json.loads(response1.content)
        response2 = self.client.get(
            reverse('producao_artistica-count'), {'format': 'default'})
        json_response2 = json.loads(response2.content)
        self.assertEqual(json_response1, json_response2)

    def test_count_returns_400_with_invalid_format(self):
        response = self.client.get(
            reverse('producao_artistica-count'), {'ano_inicio': 'zebra'})

        self.assertEqual(response.status_code, 400)

    def test_count_with_empty_database(self):
        RelPessoaProdArt.objects.all().delete()
        ProducaoArtistica.objects.all().delete()

        response = self.client.get(reverse('producao_artistica-count'))
        prod_list = json.loads(response.content)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(prod_list), 3)

        for item in prod_list:
            assert item in ['AUH', 'AUP', 'AUT']
            years = prod_list[item]
            for value in years:
                self.assertEqual(years[value], 0)

    def test_keywords_with_default_limit(self):
        response = self.client.get(reverse('producao_artistica-keywords'))
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        expected_result = [
            {"titulo": 6},
            {"teste": 3},
            {"foo": 2},
            {"bar": 1}
        ]
        self.assertEqual(json_response['keywords'], expected_result)

    def test_keywords_with_specific_limit(self):
        response = self.client.get(reverse('producao_artistica-keywords'),
                                   {"limit": 2})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        expected_result = [
            {"titulo": 6},
            {"teste": 3}
        ]
        self.assertEqual(json_response['keywords'], expected_result)

    def test_keywords_with_empty_database(self):
        RelPessoaProdArt.objects.all().delete()
        ProducaoArtistica.objects.all().delete()

        response = self.client.get(reverse('producao_artistica-keywords'))
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(json_response['keywords'], [])

    def test_keywords_with_negative_limit_returns_400(self):
        response = self.client.get(reverse('producao_artistica-keywords'),
                                   {"limit": -1})

        self.assertEqual(response.status_code, 400)

    def test_keywords_with_invalid_limit_returns_400(self):
        response = self.client.get(
            reverse('producao_artistica-keywords'), {"limit": "foo"})

        self.assertEqual(response.status_code, 400)

    def test_map_with_default_limit(self):
        response = self.client.get(reverse('producao_artistica-map'))
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        expected_result = [
            {'Brasil': {
                'lat': '-10.3333333', 'long': '-53.2', 'total': 3
            }},
            {'Espanha': {
                'lat': '39.3260685', 'long': '-4.8379791', 'total': 3
            }}
        ]
        self.assertCountEqual(json_response['countries'], expected_result)

    def test_map_with_specific_limit(self):
        response = self.client.get(
            reverse('producao_artistica-map'), {"limit": 2})
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        expected_result = [
            {'Brasil': {
                'lat': '-10.3333333', 'long': '-53.2', 'total': 3
            }},
            {'Espanha': {
                'lat': '39.3260685', 'long': '-4.8379791', 'total': 3
            }}
        ]
        self.assertCountEqual(json_response['countries'], expected_result)

    def test_map_with_empty_database(self):
        RelPessoaProdArt.objects.all().delete()
        ProducaoArtistica.objects.all().delete()

        response = self.client.get(reverse('producao_artistica-map'))
        json_response = json.loads(response.content)

        self.assertEqual(response.status_code, 200)

        self.assertEqual(json_response['countries'], [])

    def test_map_with_negative_limit_returns_400(self):
        response = self.client.get(
            reverse('producao_artistica-map'), {"limit": -1})

        self.assertEqual(response.status_code, 400)

    def test_map_with_invalid_limit_returns_400(self):
        response = self.client.get(
            reverse('producao_artistica-map'), {"limit": "foo"})

        self.assertEqual(response.status_code, 400)
