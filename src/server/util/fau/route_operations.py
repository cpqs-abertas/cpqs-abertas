import datetime
import json
from django.db.models import Count, Q
from django.http import JsonResponse
from models.models import Pessoa
from ..populate.code.parsing.util import normalize_string
from .RequestError import RequestError

INITIAL_YEAR = '1948'
now = datetime.datetime.now()
current_year = str(now.year)


def error(msg, status):
    return JsonResponse({ 'error': msg }, status=status)


def count(production_model, params):
    """Count route.

    Parameters: a GET request with optionals 'ano_inicio', 'ano_fim',
    'departamentos[]' and 'tipos[]' parameters.

    Returns:
    JsonResponse with status code and the count of productions
    for each type, department and year.
    """
    ini_year = params.get('ano_inicio', INITIAL_YEAR)
    end_year = params.get('ano_fim', current_year)
    departments = params.getlist('departamentos[]')
    types = params.getlist('tipos[]')

    if (not ini_year.isdigit() or not end_year.isdigit()
            or int(ini_year) < 1948):
        raise RequestError('Ano inválido', 400)

    ini_year, end_year = int(ini_year), int(end_year)

    labels = [y for y in range(int(ini_year), int(end_year)+1)]

    has_request_types = bool(types)
    has_request_departments = bool(departments)

    production_model_fields_map = production_model.database_fields_map()
    professors_department = production_model_fields_map['professors_departments']
    production_type = production_model_fields_map['production_type']
    production_involved_people = production_model_fields_map['production_involved_people']

    if not types:
        types = [typ[production_type]
                 for typ in production_model.objects.values(production_type)
                 .distinct()]

    if not departments:
        departments = [dep['departamento']
                       for dep in Pessoa.objects.values('departamento')
                       .distinct()]

    result = {
        dep:
            {
                type:
                    {year: 0 for year in labels}
                for type in types
            }
        for dep in departments
    }

    prods = production_model.objects
    if has_request_departments:
        authors = Pessoa.objects.values('id_lattes')
        authors_from_deps = authors.filter(departamento__in=departments)
        involved_people_filter_query = Q(**{"%s__in" % production_involved_people: authors_from_deps})
        prods = prods.filter(involved_people_filter_query)

    if has_request_types:
        type_filter_query = Q(**{"%s__in" % production_type: types})
        prods = prods.filter(type_filter_query)

    prods = prods \
        .values(professors_department, 'ano', production_type) \
        .annotate(year_total=Count('id', distinct=True)) \
        .order_by(professors_department, production_type, 'ano')

    prods = list(prods)
    department = ""
    i = 0
    while i < len(prods):
        prod = prods[i]
        if prod[professors_department] != department:
            department = prod[professors_department]
            type = ""
            while (i < len(prods) and
                    prods[i][professors_department] == department):
                prod = prods[i]
                if prod[production_type] != type:
                    type = prod[production_type]
                    while (
                        i < len(prods) and
                        prods[i][production_type] == type and
                        prods[i][professors_department] == department
                    ):
                        prod = prods[i]
                        if ini_year <= prod['ano'] <= end_year:
                            result[department][type][prod['ano']] = \
                                prod['year_total']
                        i += 1

    return result

def keywords(production_model, params):
    """Keywords for model route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the keywords with your specific
    frequency.
   """
    try:
        limit = int(params.get('limit', 20))
    except ValueError:
        raise RequestError('Limite inválido', 400)

    if not limit > 0:
        raise RequestError('Limite inválido', 400)

    keywords = production_model.keywords(limit)
    result = []

    for keyword in keywords:
        result.append({keyword[0]: keyword[1]})

    return result

def map(production_model, params):
    """Map for producao_artistica route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the list of countries
    and the specific artistic productions counter.
   """
    try:
        limit = int(params.get('limit', 20))
    except ValueError:
        raise RequestError('Limite inválido.', 400)

    if not limit > 0:
        raise RequestError('Limite inválido.', 400)

    countries = production_model.objects \
        .all() \
        .values('pais') \
        .annotate(total=Count('pais')) \
        .order_by('-total')[:limit]
    result = []

    with open('util/countries.json', 'r') as f:
        country_dict = json.loads(f.read())

    for country in countries:
        key = normalize_string(country['pais'])
        try:
            geolocation = country_dict[key]
            result.append({geolocation['name']: {'lat': geolocation['lat'],
                                                 'long': geolocation['long'],
                                                 'total': country['total']}})
        except Exception:
            pass

    return result

def national_map(production_model, params):
    """National map for producao_bibliografica route.

    Parameters: a GET request with optionals 'limit'.

    Returns:
    JsonResponse with status code and the list of countries and the specific
    bibliographic productions counter.
   """
    try:
        limit = int(params.get('limit', 20))
    except ValueError:
        raise RequestError('Limite inválido', 400)

    if not limit > 0:
        raise RequestError('Limite inválido', 400)

    states_count = production_model.objects \
        .all() \
        .values('estado') \
        .annotate(total=Count('estado'))

    states = {
        None: 0,
        "": 0,
        "Acre": 0,
        "Alagoas": 0,
        "Amapá": 0,
        "Amazonas": 0,
        "Bahia": 0,
        "Ceará": 0,
        "Distrito Federal": 0,
        "Espírito Santo": 0,
        "Goiás": 0,
        "Maranhão": 0,
        "Mato Grosso": 0,
        "Mato Grosso do Sul": 0,
        "Minas Gerais": 0,
        "Pará": 0,
        "Paraíba": 0,
        "Paraná": 0,
        "Pernambuco": 0,
        "Piauí": 0,
        "Rio de Janeiro": 0,
        "Rio Grande do Norte": 0,
        "Rio Grande do Sul": 0,
        "Rondônia": 0,
        "Roraima": 0,
        "Santa Catarina": 0,
        "São Paulo": 0,
        "Sergipe": 0,
        "Tocantins": 0
    }

    for state in states_count:
        states[state["estado"]] = state["total"]

    if "" in states:
        del states[""]
    if None in states:
        del states[None]

    states_total = sum([total for total in states.values()])
    result = {"estados": states, "total": states_total}

    return result

def types_count(production_model, params):
    departments = params.getlist('departamentos[]')
    types = params.getlist('tipos[]')

    if not departments:
        departments = [dep['departamento']
                       for dep in Pessoa.objects.values('departamento')
                           .distinct()]
    if not types:
        types = [typ['tipo']
                 for typ in production_model.objects.values('tipo')
                     .distinct()]

    result = {}
    for department in departments:
        result[department] = []
        for type in types:
            values = {}
            author_from_dep = Pessoa.objects \
                .filter(departamento__contains=department) \
                .values('id_lattes')
            prods_count = production_model.objects \
                .filter(autores__in=author_from_dep) \
                .distinct() \
                .filter(tipo=type) \
                .count()
            values[type] = prods_count
            result[department].append(values)

    return result
