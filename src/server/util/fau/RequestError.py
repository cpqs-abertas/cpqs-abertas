class RequestError(Exception):
    def __init__(self, error_message, return_code):
        super().__init__(error_message)
        self.error_message = error_message
        self.return_code = return_code

    def __str__(self):
        return self.error_message
