class Parser():
	def has_correct_extension(self):
		raise NotImplementedError()
	
	def is_well_formed(self):
		raise NotImplementedError()

	def insert_professor_information(self):
		raise NotImplementedError()

	def insert_lattes_information(self):
		raise NotImplementedError()
