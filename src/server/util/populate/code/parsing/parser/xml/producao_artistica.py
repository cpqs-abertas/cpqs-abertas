from html import unescape
from bs4 import BeautifulSoup

from ... import util


def parse_producao_artistica(root, num_identificador):
    producoes_array = []

    for producoes in root.findall("OUTRA-PRODUCAO/PRODUCAO-ARTISTICA-CULTURAL"):
        for producao in producoes:
            tipo = producao.tag
            dados_producao = producao.find("DADOS-BASICOS-DO-{}".format(tipo))
            if dados_producao is None:
                dados_producao = producao.find("DADOS-BASICOS-DA-{}".format(tipo))
            if dados_producao is None:
                dados_producao = producao.find("DADOS-BASICOS-DE-{}".format(tipo))
            if dados_producao is None:
                print("Exception in parse_producao_artistica: {} with {}".format(num_identificador, producao.tag))

            producoes_dict = {}
            producoes_dict['id'] = ""
            producoes_dict['tipo'] = producao.tag.replace("-", "_").lower().title()
            producoes_dict['natureza'] = dados_producao.attrib['NATUREZA']
            producoes_dict['titulo'] = BeautifulSoup(unescape(dados_producao.attrib['TITULO']), 'lxml').text
            producoes_dict['pais'] = dados_producao.attrib['PAIS']
            producoes_dict['ano'] = dados_producao.attrib['ANO']
            try:
                producoes_dict['meio'] = dados_producao.attrib['MEIO-DE-DIVULGACAO']
            except:
                producoes_dict['meio'] = ""
            try:
                producoes_dict['flag'] = dados_producao.attrib['FLAG-RELEVANCIA']
            except:
                producoes_dict['flag'] = ""
            try:
                producoes_dict['doi'] = dados_producao.attrib['DOI']
            except:
                producoes_dict['doi'] = ""
            try:
                producoes_dict['idioma'] = dados_producao.attrib['IDIOMA']
            except:
                producoes_dict['idioma'] = ""

            detalhe_producao = producao.find("DETALHAMENTO-DE-{}".format(tipo))
            if detalhe_producao is None:
                detalhe_producao = producao.find("DETALHAMENTO-DO-{}".format(tipo))

            if detalhe_producao is None:
                detalhe_producao = producao.find("DETALHAMENTO-DA-{}".format(tipo))

            try:
                producoes_dict['premiacao'] = detalhe_producao.attrib['PREMIACAO']
            except:
                producoes_dict['premiacao'] = ""
            try:
                producoes_dict['exposicao'] = detalhe_producao.attrib['EXPOSICAO']
            except:
                producoes_dict['exposicao'] = ""

            autores = []
            for autor in producao.findall("AUTORES"):
                autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
            producoes_dict['autores'] = autores

            util.check_country_exists(producoes_dict['pais'], producoes_dict['titulo'], autores)
        
            producoes_dict['estado'] = util.get_estado_from_cidade(detalhe_producao,
                ['CIDADE', 'CIDADE-DO-EVENTO'], producoes_dict['titulo'], autores)

            producoes_array.append(producoes_dict)

    return producoes_array
