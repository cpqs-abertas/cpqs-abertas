from html import unescape
from bs4 import BeautifulSoup

from ... import util


def parse_producao_tecnica(root, num_identificador):
    producoes_array = []

    for producoes in root.findall("PRODUCAO-TECNICA"):
        for producao in producoes:
            if producao.tag == "DEMAIS-TIPOS-DE-PRODUCAO-TECNICA":
                continue
            tipo = producao.tag
            artigo = "O"
            dados_producao = producao.find("DADOS-BASICOS-DO-{}".format(tipo))
            if dados_producao is None:
                artigo = "A"
                dados_producao = producao.find("DADOS-BASICOS-DA-{}".format(tipo))
            if dados_producao is None:
                artigo = "E"
                dados_producao = producao.find("DADOS-BASICOS-DE-{}".format(tipo))
            if dados_producao is None:
                print("Exception in parse_producao_tecnica: {} with {}".format(num_identificador, producao.tag))

            producoes_dict = {}
            producoes_dict['id'] = ""
            producoes_dict['autores'] = []
            producoes_dict['tipo'] = producao.tag.replace("-", "_").lower().title()

            try:
                producoes_dict['natureza'] = dados_producao.attrib['NATUREZA']
            except:
                producoes_dict['natureza'] = ""

            producoes_dict['titulo'] = ""
            try:
                producoes_dict['titulo'] = BeautifulSoup(unescape(dados_producao.attrib['TITULO-D{}-{}'.format(artigo, tipo)]), 'lxml').text
            except:
                pass
            if producoes_dict['titulo'] == "":
                try:
                    producoes_dict['titulo'] = dados_producao.attrib['TITULO-DO-PROCESSO']
                except:
                    pass
            if producoes_dict['titulo'] == "":
                try:
                    producoes_dict['titulo'] = dados_producao.attrib['TIPO-PRODUTO']
                except:
                    pass
            if producoes_dict['titulo'] == "":
                try:
                    producoes_dict['titulo'] = dados_producao.attrib['TITULO']
                except:
                    pass
            if producoes_dict['titulo'] == "":
                print("Exception of titulo in producao_tecnica: {} with {}".format(tipo, dados_producao.attrib))

            try:
                producoes_dict['ano'] = dados_producao.attrib['ANO']
            except:
                try:
                    producoes_dict['ano'] = dados_producao.attrib['ANO-DESENVOLVIMENTO']
                except:
                    print("Exception of ano in producao_tecnica: {} with {}".format(tipo, dados_producao.attrib))
            
            producoes_dict['pais'] = dados_producao.attrib['PAIS']
            try:
                producoes_dict['idioma'] = dados_producao.attrib['IDIOMA']
            except:
                producoes_dict['idioma'] = ""
            try:
                producoes_dict['meio'] = dados_producao.attrib['MEIO-DE-DIVULGACAO']
            except:
                producoes_dict['meio'] = ""
            try:
                producoes_dict['flag'] = dados_producao.attrib['FLAG-RELEVANCIA']
            except:
                producoes_dict['flag'] = ""
            
            detalhe_producao = producao.find("DETALHAMENTO-DE-{}".format(tipo))
            if detalhe_producao is None:
                detalhe_producao = producao.find("DETALHAMENTO-DO-{}".format(tipo))

            if detalhe_producao is None:
                detalhe_producao = producao.find("DETALHAMENTO-DA-{}".format(tipo))

            try:
                producoes_dict['finalidade'] = detalhe_producao.attrib['FINALIDADE']
            except:
                producoes_dict['finalidade'] = ""
            try:
                producoes_dict['pags'] = detalhe_producao.attrib['NUMERO-DE-PAGINAS']
            except:
                producoes_dict['pags'] = ""

            autores = []
            producoes_dict['autores'] = autores
            for autor in producao.findall("AUTORES"):
                autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
            producoes_dict['autores'] = autores

            util.check_country_exists(producoes_dict['pais'], producoes_dict['titulo'], autores)

            producoes_dict['estado'] = util.get_estado_from_cidade(detalhe_producao,
                ['CIDADE-DO-TRABALHO'], producoes_dict['titulo'], autores)

            producoes_array.append(producoes_dict)

    for producoes in root.findall("PRODUCAO-TECNICA/DEMAIS-TIPOS-DE-PRODUCAO-TECNICA"):
        for producao in producoes:
            tipo = producao.tag
            artigo = "O"
            dados_producao = producao.find("DADOS-BASICOS-DO-{}".format(tipo))
            if dados_producao is None:
                artigo = "A"
                dados_producao = producao.find("DADOS-BASICOS-DA-{}".format(tipo))
            if dados_producao is None:
                artigo = "E"
                dados_producao = producao.find("DADOS-BASICOS-DE-{}".format(tipo))
            if dados_producao is None:
                if tipo == "CURSO-DE-CURTA-DURACAO-MINISTRADO":
                    dados_producao = producao.find("DADOS-BASICOS-DE-CURSOS-CURTA-DURACAO-MINISTRADO")
                elif tipo == "DESENVOLVIMENTO-DE-MATERIAL-DIDATICO-OU-INSTRUCIONAL":
                    dados_producao = producao.find("DADOS-BASICOS-DO-MATERIAL-DIDATICO-OU-INSTRUCIONAL")

            if dados_producao is None:
                print("Exception in parse_producao_tecnica: {} with {}".format(num_identificador, producao.tag))

            producoes_dict = {}
            producoes_dict['id'] = ""
            producoes_dict['tipo'] = producao.tag.replace("-", "_").lower().title()
            producoes_dict['pais'] = dados_producao.attrib['PAIS']
            producoes_dict['ano'] = dados_producao.attrib['ANO']
            producoes_dict['titulo'] = BeautifulSoup(unescape(dados_producao.attrib['TITULO']), 'lxml').text
            producoes_dict['pais'] = dados_producao.attrib['PAIS']
            try:
                producoes_dict['idioma'] = dados_producao.attrib['IDIOMA']
            except:
                producoes_dict['idioma'] = ""
            producoes_dict['meio'] = ""
            try:
                producoes_dict['flag'] = dados_producao.attrib['FLAG-RELEVANCIA']
            except:
                producoes_dict['flag'] = ""

            autores = []
            producoes_dict['autores'] = autores
            for autor in producao.findall("AUTORES"):
                autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
            producoes_dict['autores'] = autores

            util.check_country_exists(producoes_dict['pais'], producoes_dict['titulo'], autores)

            try:
                producoes_dict['natureza'] = dados_producao.attrib['NATUREZA']
            except:
                producoes_dict['natureza'] = ""            
            
            producoes_dict['finalidade'] = ""
            producoes_dict['pags'] = ""

            detalhe_tipo = tipo
            if tipo == "CURSO-DE-CURTA-DURACAO-MINISTRADO":
                detalhe_tipo = "CURSOS-CURTA-DURACAO-MINISTRADO"
            elif tipo == "DESENVOLVIMENTO-DE-MATERIAL-DIDATICO-OU-INSTRUCIONAL":
                detalhe_tipo = "MATERIAL-DIDATICO-OU-INSTRUCIONAL"

            detalhe_producao = producao.find("DETALHAMENTO-DE-{}".format(detalhe_tipo))
            if detalhe_producao is None:
                detalhe_producao = producao.find("DETALHAMENTO-DO-{}".format(detalhe_tipo))

            if detalhe_producao is None:
                detalhe_producao = producao.find("DETALHAMENTO-DA-{}".format(detalhe_tipo))

            producoes_dict['estado'] = util.get_estado_from_cidade(detalhe_producao,
                ['CIDADE', 'CIDADE-DA-APRESENTAÇÃO'], producoes_dict['titulo'], autores)
            
            producoes_array.append(producoes_dict)

    return producoes_array
