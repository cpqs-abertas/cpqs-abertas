from html import unescape
from bs4 import BeautifulSoup

from ... import util


def parse_orientacao(root, num_identificador):
    orientacoes_array = []

    # MESTRADO
    for orientacao in root.findall("OUTRA-PRODUCAO/ORIENTACOES-CONCLUIDAS/ORIENTACOES-CONCLUIDAS-PARA-MESTRADO"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DE-ORIENTACOES-CONCLUIDAS-PARA-MESTRADO")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DE-ORIENTACOES-CONCLUIDAS-PARA-MESTRADO")

        orientacoes_dict = {}
        orientacoes_dict['id'] = ""
        orientacoes_dict['titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO']), 'lxml').text
        orientacoes_dict['pessoa_id'] = num_identificador
        orientacoes_dict['aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTADO']
        orientacoes_dict['pais'] = dados_orientacao.attrib['PAIS']
        util.check_country_exists(orientacoes_dict['pais'], orientacoes_dict['titulo'],
            [orientacoes_dict['aluno']])

        orientacoes_dict['ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['instituicao'] = detalhe_orientacao.attrib['NOME-DA-INSTITUICAO']
        try:
            orientacoes_dict['agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['agencia_fomento'] = ""
        orientacoes_dict['tipo_orientacao'] = dados_orientacao.attrib['TIPO'].title()
        orientacoes_dict['tipo_pesquisa'] = "mestrado"

        try:
            orientacoes_dict['natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['natureza'] = ""
        try:
            orientacoes_dict['idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['idioma'] = ""
        try:
            orientacoes_dict['flag'] = dados_orientacao.attrib['FLAG-RELEVANCIA']
        except:
            orientacoes_dict['flag'] = ""
        try:
            orientacoes_dict['doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['doi'] = ""
        try:
            orientacoes_dict['curso'] = detalhe_orientacao.attrib['NOME-DO-CURSO']
        except:
            orientacoes_dict['curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # DOUTORADO
    for orientacao in root.findall("OUTRA-PRODUCAO/ORIENTACOES-CONCLUIDAS/ORIENTACOES-CONCLUIDAS-PARA-DOUTORADO"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DE-ORIENTACOES-CONCLUIDAS-PARA-DOUTORADO")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DE-ORIENTACOES-CONCLUIDAS-PARA-DOUTORADO")

        orientacoes_dict = {}
        orientacoes_dict['id'] = ""
        orientacoes_dict['titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO']), 'lxml').text
        orientacoes_dict['pessoa_id'] = num_identificador
        orientacoes_dict['aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTADO']
        orientacoes_dict['pais'] = dados_orientacao.attrib['PAIS']
        util.check_country_exists(orientacoes_dict['pais'], orientacoes_dict['titulo'],
            [orientacoes_dict['aluno']])

        orientacoes_dict['ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['instituicao'] = detalhe_orientacao.attrib['NOME-DA-INSTITUICAO']
        try:
            orientacoes_dict['agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['agencia_fomento'] = ""
        try:
            orientacoes_dict['tipo_orientacao'] = detalhe_orientacao.attrib['TIPO-DE-ORIENTACAO'].title()
        except:
            orientacoes_dict['tipo_orientacao'] = ""
        orientacoes_dict['tipo_pesquisa'] = "doutorado"

        try:
            orientacoes_dict['natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['natureza'] = ""
        try:
            orientacoes_dict['idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['idioma'] = ""
        try:
            orientacoes_dict['flag'] = dados_orientacao.attrib['FLAG-RELEVANCIA']
        except:
            orientacoes_dict['flag'] = ""
        try:
            orientacoes_dict['doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['doi'] = ""
        try:
            orientacoes_dict['curso'] = detalhe_orientacao.attrib['NOME-DO-CURSO']
        except:
            orientacoes_dict['curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # OUTRAS
    for orientacao in root.findall("OUTRA-PRODUCAO/ORIENTACOES-CONCLUIDAS/OUTRAS-ORIENTACOES-CONCLUIDAS"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DE-OUTRAS-ORIENTACOES-CONCLUIDAS")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DE-OUTRAS-ORIENTACOES-CONCLUIDAS")

        orientacoes_dict = {}
        orientacoes_dict['id'] = ""
        orientacoes_dict['pessoa_id'] = num_identificador
        orientacoes_dict['titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO']), 'lxml').text
        orientacoes_dict['aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTADO']
        orientacoes_dict['pais'] = dados_orientacao.attrib['PAIS']
        util.check_country_exists(orientacoes_dict['pais'], orientacoes_dict['titulo'],
            [orientacoes_dict['aluno']])

        orientacoes_dict['ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['instituicao'] = detalhe_orientacao.attrib['NOME-DA-INSTITUICAO']
        try:
            orientacoes_dict['agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['agencia_fomento'] = ""
        try:
            orientacoes_dict['tipo_orientacao'] = detalhe_orientacao.attrib['TIPO-DE-ORIENTACAO'].title()
        except:
            orientacoes_dict['tipo_orientacao'] = ""
        orientacoes_dict['tipo_pesquisa'] = "outras"

        try:
            orientacoes_dict['natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['natureza'] = ""
        try:
            orientacoes_dict['idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['idioma'] = ""
        try:
            orientacoes_dict['flag'] = dados_orientacao.attrib['FLAG-RELEVANCIA']
        except:
            orientacoes_dict['flag'] = ""
        try:
            orientacoes_dict['doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['doi'] = ""
        try:
            orientacoes_dict['curso'] = detalhe_orientacao.attrib['NOME-DO-CURSO']
        except:
            orientacoes_dict['curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # POS DOUTORADO
    for orientacao in root.findall("OUTRA-PRODUCAO/ORIENTACOES-CONCLUIDAS/ORIENTACOES-CONCLUIDAS-PARA-POS-DOUTORADO"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DE-ORIENTACOES-CONCLUIDAS-PARA-POS-DOUTORADO")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DE-ORIENTACOES-CONCLUIDAS-PARA-POS-DOUTORADO")

        orientacoes_dict = {}
        orientacoes_dict['id'] = ""
        orientacoes_dict['pessoa_id'] = num_identificador
        orientacoes_dict['titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO']), 'lxml').text
        orientacoes_dict['aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTADO']
        orientacoes_dict['pais'] = dados_orientacao.attrib['PAIS']
        util.check_country_exists(orientacoes_dict['pais'], orientacoes_dict['titulo'],
            [orientacoes_dict['aluno']])

        orientacoes_dict['ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['instituicao'] = detalhe_orientacao.attrib['NOME-DA-INSTITUICAO']
        try:
            orientacoes_dict['agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['agencia_fomento'] = ""
        try:
            orientacoes_dict['tipo_orientacao'] = detalhe_orientacao.attrib['TIPO-DE-ORIENTACAO'].title()
        except:
            orientacoes_dict['tipo_orientacao'] = ""
        orientacoes_dict['tipo_pesquisa'] = "pos_doutorado"

        try:
            orientacoes_dict['natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['natureza'] = ""
        try:
            orientacoes_dict['idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['idioma'] = ""
        try:
            orientacoes_dict['flag'] = dados_orientacao.attrib['FLAG-RELEVANCIA']
        except:
            orientacoes_dict['flag'] = ""
        try:
            orientacoes_dict['doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['doi'] = ""
        try:
            orientacoes_dict['curso'] = detalhe_orientacao.attrib['NOME-DO-CURSO']
        except:
            orientacoes_dict['curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # MESTRADO ANDAMENTO
    for orientacao in root.findall("DADOS-COMPLEMENTARES/ORIENTACOES-EM-ANDAMENTO/ORIENTACAO-EM-ANDAMENTO-DE-MESTRADO"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DA-ORIENTACAO-EM-ANDAMENTO-DE-MESTRADO")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DA-ORIENTACAO-EM-ANDAMENTO-DE-MESTRADO")

        orientacoes_dict = {}
        orientacoes_dict['id'] = ""
        orientacoes_dict['pessoa_id'] = num_identificador
        orientacoes_dict['titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO-DO-TRABALHO']), 'lxml').text
        orientacoes_dict['aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTANDO']
        orientacoes_dict['pais'] = dados_orientacao.attrib['PAIS']
        util.check_country_exists(orientacoes_dict['pais'], orientacoes_dict['titulo'],
            [orientacoes_dict['aluno']])

        orientacoes_dict['ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['instituicao'] = detalhe_orientacao.attrib['NOME-INSTITUICAO']
        try:
            orientacoes_dict['agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['agencia_fomento'] = ""
        try:
            orientacoes_dict['tipo_orientacao'] = detalhe_orientacao.attrib['TIPO-DE-ORIENTACAO'].title()
        except:
            orientacoes_dict['tipo_orientacao'] = ""
        orientacoes_dict['tipo_pesquisa'] = "mestrado_andamento"

        try:
            orientacoes_dict['natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['natureza'] = ""
        try:
            orientacoes_dict['idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['idioma'] = ""
        orientacoes_dict['flag'] = ""
        try:
            orientacoes_dict['doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['doi'] = ""
        try:
            orientacoes_dict['curso'] = detalhe_orientacao.attrib['NOME-CURSO']
        except:
            orientacoes_dict['curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # DOUTORADO ANDAMENTO
    for orientacao in root.findall("DADOS-COMPLEMENTARES/ORIENTACOES-EM-ANDAMENTO/ORIENTACAO-EM-ANDAMENTO-DE-DOUTORADO"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DA-ORIENTACAO-EM-ANDAMENTO-DE-DOUTORADO")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DA-ORIENTACAO-EM-ANDAMENTO-DE-DOUTORADO")

        orientacoes_dict = {}
        orientacoes_dict['id'] = ""
        orientacoes_dict['pessoa_id'] = num_identificador
        orientacoes_dict['titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO-DO-TRABALHO']), 'lxml').text
        orientacoes_dict['aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTANDO']
        orientacoes_dict['pais'] = dados_orientacao.attrib['PAIS']
        util.check_country_exists(orientacoes_dict['pais'], orientacoes_dict['titulo'],
            [orientacoes_dict['aluno']])

        orientacoes_dict['ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['instituicao'] = detalhe_orientacao.attrib['NOME-INSTITUICAO']
        try:
            orientacoes_dict['agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['agencia_fomento'] = ""
        try:
            orientacoes_dict['tipo_orientacao'] = detalhe_orientacao.attrib['TIPO-DE-ORIENTACAO'].title()
        except:
            orientacoes_dict['tipo_orientacao'] = ""
        orientacoes_dict['tipo_pesquisa'] = "doutorado_andamento"

        try:
            orientacoes_dict['natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['natureza'] = ""
        try:
            orientacoes_dict['idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['idioma'] = ""
        orientacoes_dict['flag'] = ""
        try:
            orientacoes_dict['doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['doi'] = ""
        try:
            orientacoes_dict['curso'] = detalhe_orientacao.attrib['NOME-CURSO']
        except:
            orientacoes_dict['curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # INICIACAO CIENTIFICA ANDAMENTO
    for orientacao in root.findall("DADOS-COMPLEMENTARES/ORIENTACOES-EM-ANDAMENTO/ORIENTACAO-EM-ANDAMENTO-DE-INICIACAO-CIENTIFICA"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DA-ORIENTACAO-EM-ANDAMENTO-DE-INICIACAO-CIENTIFICA")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DA-ORIENTACAO-EM-ANDAMENTO-DE-INICIACAO-CIENTIFICA")

        orientacoes_dict = {}
        orientacoes_dict['id'] = ""
        orientacoes_dict['pessoa_id'] = num_identificador
        orientacoes_dict['titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO-DO-TRABALHO']), 'lxml').text
        orientacoes_dict['aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTANDO']
        orientacoes_dict['pais'] = dados_orientacao.attrib['PAIS']
        util.check_country_exists(orientacoes_dict['pais'], orientacoes_dict['titulo'],
            [orientacoes_dict['aluno']])

        orientacoes_dict['ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['instituicao'] = detalhe_orientacao.attrib['NOME-INSTITUICAO']
        try:
            orientacoes_dict['agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['agencia_fomento'] = ""
        orientacoes_dict['tipo_orientacao'] = ""
        orientacoes_dict['tipo_pesquisa'] = "iniciacao_cientifica_andamento"

        try:
            orientacoes_dict['natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['natureza'] = ""
        try:
            orientacoes_dict['idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['idioma'] = ""
        try:
            orientacoes_dict['flag'] = ""
        except:
            orientacoes_dict['flag'] = ""
        try:
            orientacoes_dict['doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['doi'] = ""
        try:
            orientacoes_dict['curso'] = detalhe_orientacao.attrib['NOME-CURSO']
        except:
            orientacoes_dict['curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    # POS DOUTORADO ANDAMENTO
    for orientacao in root.findall("DADOS-COMPLEMENTARES/ORIENTACOES-EM-ANDAMENTO/ORIENTACAO-EM-ANDAMENTO-DE-POS-DOUTORADO"):
        dados_orientacao = orientacao.find("DADOS-BASICOS-DA-ORIENTACAO-EM-ANDAMENTO-DE-POS-DOUTORADO")
        detalhe_orientacao = orientacao.find("DETALHAMENTO-DA-ORIENTACAO-EM-ANDAMENTO-DE-POS-DOUTORADO")

        orientacoes_dict = {}
        orientacoes_dict['id'] = ""
        orientacoes_dict['pessoa_id'] = num_identificador
        orientacoes_dict['titulo'] = BeautifulSoup(unescape(dados_orientacao.attrib['TITULO-DO-TRABALHO']), 'lxml').text
        orientacoes_dict['aluno'] = detalhe_orientacao.attrib['NOME-DO-ORIENTANDO']
        orientacoes_dict['pais'] = dados_orientacao.attrib['PAIS']
        util.check_country_exists(orientacoes_dict['pais'], orientacoes_dict['titulo'],
            [orientacoes_dict['aluno']])

        orientacoes_dict['ano'] = dados_orientacao.attrib['ANO']
        orientacoes_dict['instituicao'] = detalhe_orientacao.attrib['NOME-INSTITUICAO']
        try:
            orientacoes_dict['agencia_fomento'] = detalhe_orientacao.attrib['NOME-DA-AGENCIA']
        except:
            orientacoes_dict['agencia_fomento'] = ""
        try:
            orientacoes_dict['tipo_orientacao'] = detalhe_orientacao.attrib['TIPO-DE-ORIENTACAO'].title()
        except:
            orientacoes_dict['tipo_orientacao'] = ""
        orientacoes_dict['tipo_pesquisa'] = "pos_doutorado_andamento"

        try:
            orientacoes_dict['natureza'] = dados_orientacao.attrib['NATUREZA']
        except:
            orientacoes_dict['natureza'] = ""
        try:
            orientacoes_dict['idioma'] = dados_orientacao.attrib['IDIOMA']
        except:
            orientacoes_dict['idioma'] = ""
        try:
            orientacoes_dict['flag'] = ""
        except:
            orientacoes_dict['flag'] = ""
        try:
            orientacoes_dict['doi'] = dados_orientacao.attrib['DOI']
        except:
            orientacoes_dict['doi'] = ""
        try:
            orientacoes_dict['curso'] = detalhe_orientacao.attrib['NOME-CURSO']
        except:
            orientacoes_dict['curso'] = ""

        orientacoes_array.append(orientacoes_dict)

    return orientacoes_array
