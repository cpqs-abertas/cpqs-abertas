from html import unescape
from bs4 import BeautifulSoup

from ... import util


def parse_producao_bibliografica(root, num_identificador):
    producoes_array = []

    # TRABALHOS-EM-EVENTOS
    for producao in root.findall("PRODUCAO-BIBLIOGRAFICA/TRABALHOS-EM-EVENTOS/TRABALHO-EM-EVENTOS"):
        dados_artigo = producao.find("DADOS-BASICOS-DO-TRABALHO")
        detalhe_artigo = producao.find("DETALHAMENTO-DO-TRABALHO")

        producao_dict = {}
        producao_dict['id'] = ""
        producao_dict['tipo'] = "evento".title()
        producao_dict['ano'] = dados_artigo.attrib['ANO-DO-TRABALHO']
        producao_dict['natureza'] = dados_artigo.attrib['NATUREZA']
        producao_dict['pais'] = dados_artigo.attrib['PAIS-DO-EVENTO']

        producao_dict['titulo'] = BeautifulSoup(unescape(dados_artigo.attrib['TITULO-DO-TRABALHO']), 'lxml').text
        autores = []
        for autor in producao.findall("AUTORES"):
            autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
        producao_dict['autores'] = autores

        util.check_country_exists(producao_dict['pais'], producao_dict['titulo'], autores)

        producao_dict['volume'] = detalhe_artigo.attrib['VOLUME']
        paginas = 1
        try:
            i = int(detalhe_artigo.attrib['PAGINA-INICIAL'])
            f = int(detalhe_artigo.attrib['PAGINA-FINAL'])
            paginas = f - i
        except:
            paginas = 1
        producao_dict['paginas'] = paginas
        producao_dict['doi'] = dados_artigo.attrib['DOI']
        producao_dict['publicacao'] = BeautifulSoup(unescape(detalhe_artigo.attrib['TITULO-DOS-ANAIS-OU-PROCEEDINGS']), 'lxml').text
        producao_dict['edicao'] = ""

        try:
            producao_dict['volume'] = detalhe_artigo.attrib['VOLUME']
        except:
            producao_dict['volume'] = ""
        try:
            producao_dict['editora'] = detalhe_artigo.attrib['NOME-DA-EDITORA']
        except:
            producao_dict['editora'] = ""
       
        producao_dict['estado'] = util.get_estado_from_cidade(detalhe_artigo,
          ['CIDADE-DO-EVENTO'], producao_dict['titulo'], producao_dict['autores'])
        
        try:
            producao_dict['idioma'] = dados_artigo.attrib['IDIOMA']
        except:
            producao_dict['idioma'] = ""
        try:
            producao_dict['meio'] = dados_artigo.attrib['MEIO-DE-DIVULGACAO']
        except:
            producao_dict['meio'] = ""
        try:
            producao_dict['flag'] = dados_artigo.attrib['FLAG-RELEVANCIA']
        except:
            producao_dict['flag'] = ""
        producao_dict['isbn'] = ""

        producoes_array.append(producao_dict)

    # ARTIGOS-PUBLICADOS
    for producao in root.findall("PRODUCAO-BIBLIOGRAFICA/ARTIGOS-PUBLICADOS/ARTIGO-PUBLICADO"):

        dados_artigo = producao.find("DADOS-BASICOS-DO-ARTIGO")
        detalhe_artigo = producao.find("DETALHAMENTO-DO-ARTIGO")

        producao_dict = {}
        producao_dict['id'] = ""
        producao_dict['tipo'] = "artigo".title()
        producao_dict['pais'] = dados_artigo.attrib['PAIS-DE-PUBLICACAO']

        producao_dict['natureza'] = dados_artigo.attrib['NATUREZA']
        producao_dict['ano'] = dados_artigo.attrib['ANO-DO-ARTIGO']
        producao_dict['titulo'] = BeautifulSoup(unescape(dados_artigo.attrib['TITULO-DO-ARTIGO']), 'lxml').text
        autores = []
        for autor in producao.findall("AUTORES"):
            autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
        producao_dict['autores'] = autores

        util.check_country_exists(producao_dict['pais'], producao_dict['titulo'], autores)

        producao_dict['volume'] = detalhe_artigo.attrib['VOLUME']
        paginas = 1
        try:
            i = int(detalhe_artigo.attrib['PAGINA-INICIAL'])
            f = int(detalhe_artigo.attrib['PAGINA-FINAL'])
            paginas = f - i
        except:
            paginas = 1
        producao_dict['paginas'] = paginas
        producao_dict['doi'] = dados_artigo.attrib['DOI']
        producao_dict['publicacao'] = BeautifulSoup(unescape(detalhe_artigo.attrib['TITULO-DO-PERIODICO-OU-REVISTA']), 'lxml').text
        producao_dict['volume'] = detalhe_artigo.attrib['VOLUME']
        producao_dict['editora'] = ""
        producao_dict['edicao'] = ""


        producao_dict['estado'] = util.get_estado_from_cidade(detalhe_artigo,
          ['CIDADE-DA-EDITORA'], producao_dict['titulo'], producao_dict['autores'])

        try:
            producao_dict['idioma'] = dados_artigo.attrib['IDIOMA']
        except:
            producao_dict['idioma'] = ""
        try:
            producao_dict['meio'] = dados_artigo.attrib['MEIO-DE-DIVULGACAO']
        except:
            producao_dict['meio'] = ""
        try:
            producao_dict['flag'] = dados_artigo.attrib['FLAG-RELEVANCIA']
        except:
            producao_dict['flag'] = ""
        try:
            producao_dict['isbn'] = detalhe_artigo.attrib['ISSN']
        except:
            producao_dict['isbn'] = ""

        producoes_array.append(producao_dict)

    # Livros
    for producao in root.findall("PRODUCAO-BIBLIOGRAFICA/LIVROS-E-CAPITULOS/LIVROS-PUBLICADOS-OU-ORGANIZADOS/LIVRO-PUBLICADO-OU-ORGANIZADO"):

        dados_livro = producao.find("DADOS-BASICOS-DO-LIVRO")
        detalhe_livro = producao.find("DETALHAMENTO-DO-LIVRO")

        producao_dict = {}
        producao_dict['id'] = ""
        producao_dict['tipo'] = "livro".title()
        producao_dict['ano'] = dados_livro.attrib['ANO']
        producao_dict['natureza'] = dados_livro.attrib['NATUREZA']
        producao_dict['pais'] = dados_livro.attrib['PAIS-DE-PUBLICACAO']

        producao_dict['titulo'] = BeautifulSoup(unescape(dados_livro.attrib['TITULO-DO-LIVRO']), 'lxml').text
        autores = []
        for autor in producao.findall("AUTORES"):
            autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
        producao_dict['autores'] = autores

        util.check_country_exists(producao_dict['pais'], producao_dict['titulo'], autores)

        producao_dict['volume'] = detalhe_livro.attrib['NUMERO-DE-VOLUMES']
        producao_dict['paginas'] = detalhe_livro.attrib['NUMERO-DE-PAGINAS']
        producao_dict['edicao'] = detalhe_livro.attrib['NUMERO-DA-EDICAO-REVISAO']
        producao_dict['doi'] = dados_livro.attrib['DOI']
        producao_dict['publicacao'] = "Livro"


        producao_dict['estado'] = util.get_estado_from_cidade(detalhe_livro,
          ['CIDADE-DA-EDITORA'], producao_dict['titulo'], producao_dict['autores'])

        try:
            producao_dict['editora'] = detalhe_livro.attrib['NOME-DA-EDITORA']
        except:
            producao_dict['editora'] = ""
        try:
            producao_dict['idioma'] = dados_livro.attrib['IDIOMA']
        except:
            producao_dict['idioma'] = ""
        try:
            producao_dict['meio'] = dados_livro.attrib['MEIO-DE-DIVULGACAO']
        except:
            producao_dict['meio'] = ""
        try:
            producao_dict['flag'] = dados_livro.attrib['FLAG-RELEVANCIA']
        except:
            producao_dict['flag'] = ""
        try:
            producao_dict['isbn'] = detalhe_livro.attrib['ISBN']
        except:
            producao_dict['isbn'] = ""

        producoes_array.append(producao_dict)

    # Capitulos
    for producao in root.findall("PRODUCAO-BIBLIOGRAFICA/LIVROS-E-CAPITULOS/CAPITULOS-DE-LIVROS-PUBLICADOS/CAPITULO-DE-LIVRO-PUBLICADO"):

        dados_capitulo = producao.find("DADOS-BASICOS-DO-CAPITULO")
        detalhe_capitulo = producao.find("DETALHAMENTO-DO-CAPITULO")

        producao_dict = {}
        producao_dict['id'] = ""
        producao_dict['tipo'] = "capitulo".title()
        producao_dict['ano'] = dados_capitulo.attrib['ANO']
        producao_dict['natureza'] = ""
        producao_dict['pais'] = dados_capitulo.attrib['PAIS-DE-PUBLICACAO']

        producao_dict['titulo'] = BeautifulSoup(unescape(dados_capitulo.attrib['TITULO-DO-CAPITULO-DO-LIVRO']), 'lxml').text
        autores = []
        for autor in producao.findall("AUTORES"):
            autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
        producao_dict['autores'] = autores

        util.check_country_exists(producao_dict['pais'], producao_dict['titulo'], autores)

        producao_dict['volume'] = detalhe_capitulo.attrib['NUMERO-DE-VOLUMES']
        paginas = 1
        try:
            i = int(detalhe_capitulo.attrib['PAGINA-INICIAL'])
            f = int(detalhe_capitulo.attrib['PAGINA-FINAL'])
            paginas = f - i
        except:
            paginas = 1
        producao_dict['doi'] = dados_capitulo.attrib['DOI']
        producao_dict['publicacao'] = BeautifulSoup(unescape(detalhe_capitulo.attrib['TITULO-DO-LIVRO']), 'lxml').text
        producao_dict['paginas'] = paginas
        producao_dict['edicao'] = detalhe_capitulo.attrib['NUMERO-DA-EDICAO-REVISAO']
        producao_dict['editora'] = detalhe_capitulo.attrib['NOME-DA-EDITORA']


        producao_dict['estado'] = util.get_estado_from_cidade(detalhe_capitulo,
          ['CIDADE-DA-EDITORA'], producao_dict['titulo'], producao_dict['autores'])

        try:
            producao_dict['idioma'] = dados_capitulo.attrib['IDIOMA']
        except:
            producao_dict['idioma'] = ""
        try:
            producao_dict['meio'] = dados_capitulo.attrib['MEIO-DE-DIVULGACAO']
        except:
            producao_dict['meio'] = ""
        try:
            producao_dict['flag'] = dados_capitulo.attrib['FLAG-RELEVANCIA']
        except:
            producao_dict['flag'] = ""
        try:    
            producao_dict['isbn'] = detalhe_capitulo.attrib['ISBN']
        except:
            producao_dict['isbn'] = ""

        producoes_array.append(producao_dict)

    # TEXTOS-EM-JORNAIS-OU-REVISTAS
    for producao in root.findall("PRODUCAO-BIBLIOGRAFICA/TEXTOS-EM-JORNAIS-OU-REVISTAS/TEXTO-EM-JORNAL-OU-REVISTA"):

        dados_jornal = producao.find("DADOS-BASICOS-DO-TEXTO")
        detalhe_jornal = producao.find("DETALHAMENTO-DO-TEXTO")

        producao_dict = {}
        producao_dict['id'] = ""
        producao_dict['tipo'] = "jornal".title()
        producao_dict['natureza'] = dados_jornal.attrib['NATUREZA']
        producao_dict['pais'] = dados_jornal.attrib['PAIS-DE-PUBLICACAO']

        producao_dict['ano'] = dados_jornal.attrib['ANO-DO-TEXTO']
        producao_dict['titulo'] = BeautifulSoup(unescape(dados_jornal.attrib['TITULO-DO-TEXTO']), 'lxml').text
        autores = []
        for autor in producao.findall("AUTORES"):
            autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
        producao_dict['autores'] = autores

        util.check_country_exists(producao_dict['pais'], producao_dict['titulo'], autores)

        producao_dict['volume'] = detalhe_jornal.attrib['VOLUME']
        paginas = 1
        try:
            i = int(detalhe_jornal.attrib['PAGINA-INICIAL'])
            f = int(detalhe_jornal.attrib['PAGINA-FINAL'])
            paginas = f - i
        except:
            paginas = 1
        producao_dict['paginas'] = paginas
        try:
            producao_dict['data'] = parse_data(detalhe_jornal.attrib['DATA-DE-PUBLICACAO'])
        except:
            producao_dict['data'] = None
        producao_dict['publicacao'] = detalhe_jornal.attrib['TITULO-DO-JORNAL-OU-REVISTA']
        producao_dict['edicao'] = detalhe_jornal.attrib['TITULO-DO-JORNAL-OU-REVISTA'] + ", " + detalhe_jornal.attrib['TITULO-DO-JORNAL-OU-REVISTA']
        producao_dict['editora'] = detalhe_jornal.attrib['TITULO-DO-JORNAL-OU-REVISTA']


        producao_dict['estado'] = util.get_estado_from_cidade(detalhe_jornal,
          ['LOCAL-DO-EVENTO'], producao_dict['titulo'], producao_dict['autores'])

        try:
            producao_dict['volume'] = detalhe_jornal.attrib['VOLUME']
        except:
            producao_dict['volume'] = ""

        try:
            producao_dict['idioma'] = dados_jornal.attrib['IDIOMA']
        except:
            producao_dict['idioma'] = ""
        try:
            producao_dict['meio'] = dados_jornal.attrib['MEIO-DE-DIVULGACAO']
        except:
            producao_dict['meio'] = ""

        try:
            producao_dict['flag'] = dados_jornal.attrib['FLAG-RELEVANCIA']
        except:
            producao_dict['flag'] = ""
        try:
            producao_dict['doi'] = dados_jornal.attrib['DOI']
        except:
            producao_dict['doi'] = ""
        try:
            producao_dict['isbn'] = detalhe_jornal.attrib['ISSN']
        except:
            producao_dict['isbn'] = ""

        producoes_array.append(producao_dict)

    # DEMAIS-TIPOS-DE-PRODUCAO-BIBLIOGRAFICA
    for producoes in root.findall("PRODUCAO-BIBLIOGRAFICA/DEMAIS-TIPOS-DE-PRODUCAO-BIBLIOGRAFICA"):
        for producao in producoes:

            tipo = None
            dados_producao = None
            detalhe_producao = None

            if producao.tag == "OUTRA-PRODUCAO-BIBLIOGRAFICA":
                tipo = "OUTRA-PRODUCAO-BIBLIOGRAFICA"
                dados_producao = producao.find("DADOS-BASICOS-DE-OUTRA-PRODUCAO")
                detalhe_producao = producao.find("DETALHAMENTO-DE-OUTRA-PRODUCAO")
            else:
                tipo = producao.tag
                dados_producao = producao.find("DADOS-BASICOS-DO-{}".format(tipo))
                detalhe_producao = producao.find("DETALHAMENTO-DO-{}".format(tipo))
                if dados_producao is None:
                    dados_producao = producao.find("DADOS-BASICOS-DA-{}".format(tipo))
                    detalhe_producao = producao.find("DETALHAMENTO-DA-{}".format(tipo))
                if dados_producao is None:
                    dados_producao = producao.find("DADOS-BASICOS-DE-{}".format(tipo))
                    detalhe_producao = producao.find("DETALHAMENTO-DE-{}".format(tipo))
                if dados_producao is None:
                    print("Exception in producao_bibliografica: {} with tag {}".format(num_identificador, tipo))


            producao_dict = {}
            producao_dict['id'] = ""
            producao_dict['tipo'] = tipo.replace("-", "_").lower().title()
            producao_dict['natureza'] = dados_producao.attrib['NATUREZA'].replace("-", "_").lower()
            producao_dict['pais'] = dados_producao.attrib['PAIS-DE-PUBLICACAO']


            producao_dict['ano'] = dados_producao.attrib['ANO']
            producao_dict['titulo'] = BeautifulSoup(unescape(dados_producao.attrib['TITULO']), 'lxml').text
            producao_dict['doi'] = dados_producao.attrib['DOI']

            autores = []
            for autor in producao.findall("AUTORES"):
                autores.append(autor.attrib['NOME-COMPLETO-DO-AUTOR'])
            producao_dict['autores'] = autores

            util.check_country_exists(producao_dict['pais'], producao_dict['titulo'], autores)

            producao_dict['estado'] = util.get_estado_from_cidade(detalhe_producao,
              ['CIDADE-DA-EDITORA'], producao_dict['titulo'], producao_dict['autores'])

            try:
                producao_dict['edicao'] = detalhe_producao.attrib['NUMERO-DA-EDICAO-REVISAO']
            except:
                producao_dict['edicao'] = ""

            try:
                producao_dict['volume'] = detalhe_producao.attrib['VOLUME']
            except:
                producao_dict['volume'] = ""

            try:
                producao_dict['editora'] = detalhe_producao.attrib['EDITORA']
            except:
                producao_dict['editora'] = ""

            if producao_dict['editora'] == "":
                try:
                    producao_dict['editora'] = detalhe_producao.attrib['EDITORA-DA-TRADUCAO']
                except:
                    producao_dict['editora'] = ""

            if producao_dict['editora'] == "":
                try:
                    producao_dict['editora'] = detalhe_producao.attrib['EDITORA-DO-PREFACIO-POSFACIO']
                except:
                    producao_dict['editora'] = ""

            try:
                producao_dict['publicacao'] = detalhe_producao.attrib['TITULO-DA-PUBLICACAO']
            except:
                producao_dict['publicacao'] = ""

            if producao_dict['publicacao'] == "":
                try:
                    producao_dict['publicacao'] = detalhe_producao.attrib['TITULO-DA-OBRA-ORIGINAL']
                except:
                    producao_dict['publicacao'] = ""
            
            try:
                producao_dict['idioma'] = dados_producao.attrib['IDIOMA']
            except:
                producao_dict['idioma'] = ""

            try:
                producao_dict['meio'] = dados_producao.attrib['MEIO-DE-DIVULGACAO']
            except:
                producao_dict['meio'] = ""
            try:    
                producao_dict['flag'] = dados_producao.attrib['FLAG-RELEVANCIA']
            except:
                producao_dict['flag'] = ""
            try:
                producao_dict['isbn'] = detalhe_producao.attrib['ISSN-ISBN']
            except:
                producao_dict['isbn'] = ""
            producoes_array.append(producao_dict)

    return producoes_array
