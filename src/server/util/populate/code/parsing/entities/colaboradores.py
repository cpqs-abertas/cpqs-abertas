import psycopg2

from .pessoa import select_id_pessoa_by_name


# Makes an insert in colaboradores table
def insert_colaboradores(attr, conn=None, cur=None):
    """
    Inserts in colaboradores.

    The insert_colaboradores function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the colaboradores table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    try:
        cur.execute("SELECT id FROM colaboradores WHERE colaborador1_id = %s and colaborador2_id = %s", [attr["colaborador1_id"], attr["colaborador2_id"]])
        get_id = cur.fetchall()
        id = None
        for row in get_id:
            id = int(row[0])

        if id is None:        
            cur.execute('INSERT INTO colaboradores (colaborador1_id, colaborador2_id) VALUES (%s, %s)', [attr["colaborador1_id"], attr["colaborador2_id"]])
            conn.commit()
            cur.execute("SELECT id FROM colaboradores WHERE colaborador1_id = %s and colaborador2_id = %s", [attr["colaborador1_id"], attr["colaborador2_id"]])
            get_id = cur.fetchall()
            for row in get_id:
                id = row[0]

        return id
    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()

    cur.close()
    conn.close()

def insert_colaboradores_from_list(lattes_id, authors, conn=None, cur=None):
    for author in authors:
        id = select_id_pessoa_by_name(author, conn, cur)
        if id is not None and int(lattes_id) != id:
            dict = {'colaborador1_id': lattes_id, 'colaborador2_id': id}
            insert_colaboradores(dict, conn, cur)
