import psycopg2


# Makes an insert in producao_tecnica table
def insert_producao_tecnica(attr, conn=None, cur=None):
    """
    Inserts in producao_tecnica.

    The insert_producao_tecnica function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the producao_tecnica table
    :type ev: dict of (<class 'text'>, <class 'text'>, <class 'int'>, <class 'text'>, <class 'text'>, <class 'text'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    try:
        cur.execute("SELECT id FROM producao_tecnica WHERE tipo = %s and titulo = %s and ano = %s", [attr["tipo"], attr["titulo"], attr["ano"]])
        get_id = cur.fetchall()
        id = None
        for row in get_id:
            id = int(row[0])

        if id is None:
            cur.execute('INSERT INTO producao_tecnica (tipo, titulo, ano, nomes_autores, natureza, pais, estado, idioma, meio, flag, finalidade, pags) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)', [attr["tipo"], attr["titulo"], attr["ano"], attr["autores"], attr["natureza"], attr["pais"], attr["estado"], attr["idioma"], attr["meio"], attr["flag"], attr["finalidade"], attr["pags"]])
            conn.commit()
            cur.execute("SELECT id FROM producao_tecnica WHERE tipo = %s and titulo = %s and ano = %s", [attr["tipo"], attr["titulo"], attr["ano"]])
            get_id = cur.fetchall()
            for row in get_id:
                id = row[0]

        return id
    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()

    cur.close()
    conn.close()


# Makes an insert in rel_pessoa_prod_tec table
def insert_rel_pessoa_prod_tec(attr, conn=None, cur=None):
    """
    Inserts in rel_pessoa_prod_tec.

    The insert_rel_pessoa_prod_tec function receives a record dictionary and creates
    a new dictionary with only the attributes we need in order to insert
    it into the database.

    :param ev: Event dictionary such as the attributes of the rel_pessoa_prod_tec table
    :type ev: dict of (<class 'bigint'>, <class 'bigint'>)
    :return None: Nothing is returned
    :rtype: None

    .. warning::
        The function doesn't check for the validity of the dictionary received.
    """
    try:
        cur.execute("SELECT id FROM rel_pessoa_prod_tec WHERE pessoa_id = %s and producao_id = %s", [attr["pessoa_id"], attr["producao_id"]])
        get_id = cur.fetchall()
        id = None
        for row in get_id:
            id = int(row[0])

        if id is None:
            cur.execute('INSERT INTO rel_pessoa_prod_tec (pessoa_id, producao_id) VALUES (%s, %s)', [attr["pessoa_id"], attr["producao_id"]])
            conn.commit()
            cur.execute("SELECT id FROM rel_pessoa_prod_tec WHERE pessoa_id = %s and producao_id = %s", [attr["pessoa_id"], attr["producao_id"]])
            get_id = cur.fetchall()
            for row in get_id:
                id = row[0]

        return id
    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()

    cur.close()
    conn.close()
