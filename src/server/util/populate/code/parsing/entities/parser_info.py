import psycopg2


# Makes an insert in parser_info table
def insert_parser_info(attr, conn = None, cur = None):
    """
    Inserts into parser_info.
    """

    try:
        cur.execute('INSERT INTO parser_info (data_atualizacao) VALUES (%s)', [attr["date"]])
        conn.commit()

    except (Exception, psycopg2.Error) as error:
        print ("Error while inserting", error)
        conn.rollback()
    cur.close()
    conn.close()
