import os
import psycopg2
import pymongo

from dotenv import load_dotenv

# Dictionary of database properties
load_dotenv()

INSTITUTE = os.environ.get('INSTITUTE', 'undefined')

db_dict = {
    'database': f'{INSTITUTE}_db',
    'user': os.environ.get('DB_USER', 'user'),
    'password': os.environ.get('DB_PASSWORD', 'password'),
    'host': os.environ.get('DB_HOST', 'database'),
    'port': int(os.environ.get('DB_PORT', 5432))
}

aggregate_db_dict = {
    'database': f'{INSTITUTE}_aggregate',
    'host': os.environ.get('AGGREGATE_DB_HOST', 'mongo'),
    'port': int(os.environ.get('AGGREGATE_DB_PORT', 27017))
}

# Connects to the db
def get_db(json=False):
    port = db_dict['port']
    user = db_dict['user']
    database = db_dict['database']
    password = db_dict['password']
    host = db_dict['host']
    if json:
        database = 'json_data'
    try:
        connection = psycopg2.connect(user = user,
                                      password = password,
                                      host = host,
                                      port = port,
                                      database = database)
        cursor = connection.cursor()
        return connection, cursor
    except (Exception, psycopg2.Error) as error :
        print ("Error while connecting to PostgreSQL", error)

# Makes a simple query
def make_query(query):
    conn, cur = get_db()
    cur.execute(query)
    return cur.fetchall()

def get_mongo_db():
    mongo_client = pymongo.MongoClient(aggregate_db_dict['host'], aggregate_db_dict['port'])
    return mongo_client[aggregate_db_dict['database']]

def reset_and_get_mongo_db():
    mongo_client = pymongo.MongoClient(aggregate_db_dict['host'], aggregate_db_dict['port'])
    mongo_client.drop_database(aggregate_db_dict['database'])
    db = mongo_client[aggregate_db_dict['database']]
    return db
