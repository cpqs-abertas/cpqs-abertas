import pymongo
import os
from django.http import QueryDict
from json import dumps, loads

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cpqsabertas.settings')

import sys
sys.path.append("/server")

import django
django.setup()

import dashboard.services as dashboard
import pessoa.services as pessoa
import bancas.services as bancas
import orientacao.services as orientacao
import premios.services as premios_titulos
import producao_artistica.services as producao_artistica
import producao_bibliografica.services as producao_bibliografica
import producao_tecnica.services as producao_tecnica

from parsing.db.connection import aggregate_db_dict

my_client = pymongo.MongoClient(aggregate_db_dict['host'], 27017)
my_client.drop_database(aggregate_db_dict['database'])
db = my_client[aggregate_db_dict['database']]

count_route_params = QueryDict('ano_inicio=1978')
keywords_route_params = QueryDict('limit=50')
no_params = QueryDict('')

def add_bancas_aggregates():
    collection = db["bancas"]
    aggregates = []

    aggregates.append(loads(dumps({"operation": "count", "result": bancas.get_count(count_route_params)})))
    aggregates.append(
        loads(dumps({"operation": "keywords", "result": bancas.get_keywords(keywords_route_params)})))
    aggregates.append(loads(dumps({"operation": "map", "result": bancas.get_map(no_params)})))

    types_param = QueryDict(mutable=True)
    types_param.setlistdefault('tipos[]', bancas.get_types())
    aggregates.append(loads(dumps({"operation": "count_by_type", "result": bancas.get_count(types_param)})))

    collection.insert_many(aggregates)


def add_dashboard_aggregates():
    collection = db["dashboard"]
    aggregates = []

    aggregates.append(loads(dumps({"operation": "dashboard", "result": dashboard.get_dashboard()})))
    aggregates.append(loads(dumps({"operation": "count", "result": dashboard.get_count(count_route_params)})))
    aggregates.append(loads(dumps({"operation": "map", "result": dashboard.get_map(no_params)})))
    aggregates.append(loads(dumps({"operation": "national_map", "result": dashboard.get_national_map(no_params)})))

    collection.insert_many(aggregates)


def add_orientacao_aggregates():
    collection = db["orientacao"]
    aggregates = []

    aggregates.append(loads(dumps({"operation": "count", "result": orientacao.get_count(count_route_params)})))
    aggregates.append(
        loads(dumps({"operation": "keywords", "result": orientacao.get_keywords(keywords_route_params)})))
    aggregates.append(loads(dumps({"operation": "map", "result": orientacao.get_map(no_params)})))

    types_param = QueryDict(mutable=True)
    types_param.setlistdefault('tipos[]', orientacao.get_types())
    aggregates.append(loads(dumps({"operation": "count_by_type", "result": orientacao.get_count(types_param)})))

    collection.insert_many(aggregates)


def add_premios_aggregates():
    collection = db["premios_titulos"]
    aggregates = []

    aggregates.append(loads(dumps({"operation": "count", "result": premios_titulos.get_count(count_route_params)})))

    collection.insert_many(aggregates)


def add_producao_artistica_aggregates():
    collection = db["producao_artistica"]
    aggregates = []

    aggregates.append(loads(dumps({ "operation": "count", "result": producao_artistica.get_count(count_route_params) })))
    aggregates.append(loads(dumps({ "operation": "keywords", "result": producao_artistica.get_keywords(keywords_route_params) })))
    aggregates.append(loads(dumps({ "operation": "map", "result": producao_artistica.get_map(no_params) })))
    aggregates.append(loads(dumps({ "operation": "national_map", "result": producao_artistica.get_national_map(no_params) })))

    types_param = QueryDict(mutable=True)
    types_param.setlistdefault('tipos[]', producao_artistica.get_types())
    aggregates.append(loads(dumps({ "operation": "count_by_type", "result": producao_artistica.get_count(types_param) })))

    collection.insert_many(aggregates)


def add_producao_bibliografica_aggregates():
    collection = db["producao_bibliografica"]
    aggregates = []

    aggregates.append(loads(dumps({"operation": "count", "result": producao_bibliografica.get_count(count_route_params)})))
    aggregates.append(
        loads(dumps({"operation": "keywords", "result": producao_bibliografica.get_keywords(keywords_route_params)})))
    aggregates.append(loads(dumps({"operation": "map", "result": producao_bibliografica.get_map(no_params)})))
    aggregates.append(
        loads(dumps({"operation": "national_map", "result": producao_bibliografica.get_national_map(no_params)})))

    types_param = QueryDict(mutable=True)
    types_param.setlistdefault('tipos[]', producao_bibliografica.get_types())
    aggregates.append(loads(dumps({"operation": "count_by_type", "result": producao_bibliografica.get_count(types_param)})))

    collection.insert_many(aggregates)


def add_producao_tecnica_aggregates():
    collection = db["producao_tecnica"]
    aggregates = []

    aggregates.append(loads(dumps({"operation": "count", "result": producao_tecnica.get_count(count_route_params)})))
    aggregates.append(
        loads(dumps({"operation": "keywords", "result": producao_tecnica.get_keywords(keywords_route_params)})))
    aggregates.append(loads(dumps({"operation": "map", "result": producao_tecnica.get_map(no_params)})))
    aggregates.append(
        loads(dumps({"operation": "national_map", "result": producao_tecnica.get_national_map(no_params)})))

    types_param = QueryDict(mutable=True)
    types_param.setlistdefault('tipos[]', producao_tecnica.get_types())
    aggregates.append(loads(dumps({"operation": "count_by_type", "result": producao_tecnica.get_count(types_param)})))

    collection.insert_many(aggregates)


def add_pessoa_aggregates():
    collection = db["pessoa"]
    aggregates = []

    aggregates.append(loads(dumps({"operation": "names", "result": pessoa.get_names()})))

    collection.insert_many(aggregates)


def create_aggregates():
    add_bancas_aggregates()
    add_dashboard_aggregates()
    add_orientacao_aggregates()
    add_premios_aggregates()
    add_producao_artistica_aggregates()
    add_producao_bibliografica_aggregates()
    add_producao_tecnica_aggregates()
    add_pessoa_aggregates()

create_aggregates()
