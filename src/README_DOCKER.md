# Sobre o Docker

## Resumo:

Docker é usado para criar containers, que são VMs "leves", pois não possuem um SO como uma VM de verdade teria. Temos dois arquivos importantes para que isso funcionar:

* `Dockerfile`: Usado para configurar e criar novas imagens.
* `docker-compose.yml`: É um "makefile" para o docker usado para criar diversas imagens a partir de Dockerfiles e configurar como elas rodam na máquina.

## Lista de Comandos:

* `docker-compose build (serviço)`: (Re)cria as imagens dos serviços. 

* `docker-compose up (serviço)`: Deve ser usado na mesma pasta que um `docker-compose.yml`. Faz um "make" no arquivo, criando os containers a partir das imagens dos `Dockerfiles` (criando elas caso não existam) e rodando eles.

* `docker-compose down`: Deve ser usado na mesma pasta que um `docker-compose.yml`. Para e destroi todos os containers relacionados ao docker-compose do local.

* `docker ps`: Mostra todos os containers que estão rodando no momento.

* `docker ps -a`: Mostra todos os containers que existem (mas não necessariamente estão rodando).

* `docker exec -it nome comando`: Executa o comando `comando` no container de nome `nome`. É como se você estivesse digitando um comando no terminal de dentro do container.

## Padrão das pastas
Cada pasta nesse diretorio representa um container, que segue o seguinte padrão:

* A pasta `docker_files` contém arquivos que são usados pelo Dockerfile durante a criação da imagem. Caso um deles seja modificado, a imagem deve ser recriada.

* Pastas `volume_` são inseridas no container como um volume, então modificações nos arquivos dentro delas também afetarão o container.

# Usando o Docker

## Requerimentos:

* [Docker](https://docs.docker.com/install/)
* [Docker-Compose](https://docs.docker.com/compose/install/)

## Rodando o docker da FAU Aberta

1. Entre na pasta faudjango (a mesma com o `docker-compose.yml`).
2. Rode o seguinte comando: `docker-compose up`.

## Containers

* `fauserver_db` tem o postgres com o banco de dados.
* `fauserver_django` tem o servidor do django (backend).
* `fauserver_react` tem o servidor do react (frontend).

## Usando o psql no container

1. Enquanto o container estiver rodando, abra um novo terminal.
2. Rode o seguinte comando: `docker exec -it fauserver_db psql -U fau_aberta_user -d fau_aberta_db`.

# IMPORTANTE

1. Containers são efêmeros. Isso é, uma vez que você deu `docker-compose down`, tudo que uma vez existia dentro do container some. Inclusive mudanças no banco de dados do `fauserver_db`.
2. Mudanças podem ser mantidas com `volumes`. Um volume é uma pasta do seu computador que você monta dentro do container (como um pendrive, por exemplo). Por exemplo, a pasta `django/volume_django` é um volume de `fauserver_django`, portanto podemos modificar os arquivos do django simplesmente modificando ela.


